import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
    selector: "tu-relationshipmanager",
    templateUrl: "./rm.component.html"
})
export class TURelationshipManagerComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.Get_UserAccountDetails(true);
            }
        });
        this.AssignedStores_Setup();
        this.Form_UpdateUser_Load();
        this.StoresList_Setup();
        this.RMTerminals_Setup();
    }
    Open_AddStore() {
        // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AddAccount.AddRm]);
        this._HelperService.OpenModal("ModalAssignStore");
    }
    public AssignedStores_Config: OList;
    AssignedStores_Setup() {
        this.AssignedStores_Config = {
            Id:null,
            Sort:null,
            Task: this._HelperService.AppConfig.Api.Core.GetRMStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
            Title: "Assigned Store Locations",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'RmKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, "="),
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'CreateDate desc',
            TableFields: [

                {
                    DisplayName: "Location",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Contact",
                    SystemName: "ContactNumber",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Address",
                    SystemName: "Address",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Merchant",
                    SystemName: "MerchantDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Terminals",
                    SystemName: "TerminalsCount",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: false,
                    Sort: false,
                    ResourceId: null
                },
                {
                    DisplayName: "Assigned On",
                    SystemName: "StartDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null
                }
            ]
        };
        this.AssignedStores_Config = this._DataHelperService.List_Initialize(
            this.AssignedStores_Config
        );
        this.AssignedStores_GetData();
    }
    AssignedStores_ToggleOption(event: any, Type: any) {
        this.AssignedStores_Config = this._DataHelperService.List_Operations(
            this.AssignedStores_Config,
            event,
            Type
        );
        if (this.AssignedStores_Config.RefreshData == true) {
            this.AssignedStores_GetData();
        }
    }
    AssignedStores_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.AssignedStores_Config
        );
        this.AssignedStores_Config = TConfig;
    }
    AssignedStores_RowSelected(ReferenceData) {
        swal({
            position: "top",
            title: "Remove store from RM ?",
            text: "Click on continue button to remove store",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                var pData =
                {
                    Task: "revokermstore",
                    ReferenceKey: ReferenceData.ReferenceKey,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
                    pData
                );
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Store removed from rm");
                            // this.Form_UpdateUser_Close();
                            this.AssignedStores_GetData();
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
        // var ReferenceKey = ReferenceData.ReferenceKey;
        // this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
        // // this._Router.navigate([this._HelperService.AppConfig.Pages.System.AdminUser.Dashboard, ReferenceKey]);
        // this._HelperService.RandomPassword = null;
        // this._HelperService.Get_UserAccountDetails(true);
        // this.Form_UpdateUser_Show();
    }

    public isReadOnly = true;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        } else {
            this.isReadOnly = true;
        }
    }
    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Show() {
        this._HelperService.OpenModal("Form_UpdateUser_Content");
    }
    Form_UpdateUser_Close() {
        this._HelperService.CloseModal("Form_UpdateUser_Content");
    }
    Form_UpdateUser_Load() {
        this.Form_UpdateUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            RoleKey: null,
            UserName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(256)
                ])
            ],
            Password: null,
            AccessPin: null,
            DisplayName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(25)
                ])
            ],
            FirstName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            LastName: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(10),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,
            Owners: [],
            Configuration: []
        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: "top",
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: "Click on udpate button to confirm changes",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.System,
                    _FormValue
                );
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Details updated successfully");
                            // this.Form_UpdateUser_Clear();
                            this.Form_UpdateUser_Close();
                            this._HelperService.Get_UserAccountDetails(true);

                            if (_FormValue.OperationType == "close") {
                            }
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }


    public StoresList_Config: OList;
    StoresList_Setup() {
        this.StoresList_Config = {
            Id:null,
            Sort:null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStoresLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "Stores",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', "StatusCode", this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.Status.Active, "="),
            PageRecordLimit: 5,
            Type: this._HelperService.AppConfig.ListType.All,
            TableFields: [
                {
                    DisplayName: "Merchant",
                    SystemName: "OwnerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Store Name",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Address",
                    SystemName: "Address",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                }
            ]
        };
        this.StoresList_Config = this._DataHelperService.List_Initialize(
            this.StoresList_Config
        );
        this.StoresList_GetData();
    }
    StoresList_ToggleOption(event: any, Type: any) {
        this.StoresList_Config = this._DataHelperService.List_Operations(
            this.StoresList_Config,
            event,
            Type
        );
        if (this.StoresList_Config.RefreshData == true) {
            this.StoresList_GetData();
        }
    }
    StoresList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
        this.StoresList_Config = TConfig;
    }
    StoresList_RowSelected(ReferenceData) {
        swal({
            position: "top",
            title: "Assign store to RM ?",
            text: "Click on continue button to assign store",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                var pData =
                {
                    Task: "assignstoretorm",
                    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
                    StoreKey: ReferenceData.ReferenceKey,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
                    pData
                );
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Store assigned to rm");
                            // this.Form_UpdateUser_Close();
                            this.AssignedStores_GetData();
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }


    public RMTerminals_Config: OList;
    RMTerminals_Setup() {
        this.RMTerminals_Config = {
            Id:null,
            Sort:null,
            Task: this._HelperService.AppConfig.Api.Core.GetRMTerminals,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCRmManager,
            Title: "Assigned Terminals",
            StatusType: "default",
            // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveOwnerKey, "="),
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'DisplayName asc',
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            SubReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
            TableFields: [
                {
                    DisplayName: "TID",
                    SystemName: "DisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Location",
                    SystemName: "StoreDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Address",
                    SystemName: "Address",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },

                {
                    DisplayName: "Merchant",
                    SystemName: "MerchantDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                {
                    DisplayName: "Provider",
                    SystemName: "OwnerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null
                },
                // {
                //     DisplayName: 'Last Tr',
                //     SystemName: "LastTransactionDate",
                //     DataType: this._HelperService.AppConfig.DataType.Date,
                //     Show: true,
                //     Search: false,
                //     Sort: true,
                //     ResourceId: null
                // }
            ]
        };
        this.RMTerminals_Config = this._DataHelperService.List_Initialize(
            this.RMTerminals_Config
        );
        this.RMTerminals_GetData();
    }
    RMTerminals_ToggleOption(event: any, Type: any) {
        this.RMTerminals_Config = this._DataHelperService.List_Operations(
            this.RMTerminals_Config,
            event,
            Type
        );
        if (this.RMTerminals_Config.RefreshData == true) {
            this.RMTerminals_GetData();
        }
    }
    RMTerminals_GetData() {
        var TConfig = this._DataHelperService.List_GetData(
            this.RMTerminals_Config
        );
        this.RMTerminals_Config = TConfig;
    }
    RMTerminals_RowSelected(ReferenceData) {
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AccountDetails.Rm, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
        // this._HelperService.RandomPassword = null;
        // this._HelperService.Get_UserAccountDetails(true);
        // this.Form_UpdateUser_Show();
    }
}
