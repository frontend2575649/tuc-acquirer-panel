import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { BranchDetailsComponent } from './branchdetails.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

const routes: Routes = [
    {
        path: "",
        component: BranchDetailsComponent,
        children: [
            { path: "/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../modules/accounts/tuterminals/branch/tuterminals.module#TUTerminalsModule" },

            { path: "branchupdate/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../panel/acquirer/BranchDivision/dashboard/dashboard.module#BranchUpdateModule" },

            { path: "sales/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant", PageName: "System.Menu.Branch",accounttypecode: "merchant" }, loadChildren: "../../../../modules/transactions/tusale/branch/acquirer/tusale.module#TUSaleModule" },

            { path: "manager/:referencekey/:referenceid", data: { permission: "manager", PageName: "System.Menu.Branch" }, loadChildren: "../../../acquirer/BranchDivision/manager/manager.module#ManagerModule" },

            { path: 'terminals/:referencekey/:referenceid', data: { 'permission': 'terminals', PageName: 'System.Menu.Branch' }, loadChildren: '../../../../modules/accounts/tuterminals/branch/tuterminals.module#TUTerminalsModule' },
            
            { path: 'stores/:referencekey/:referenceid', data: { 'permission': 'stores', PageName: 'System.Menu.Branch' }, loadChildren: '../../../../modules/accounts/tustores/branch/tustores.module#TUStoresModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BranchDeatilsRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        BranchDeatilsRoutingModule,
        LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [BranchDetailsComponent]
})
export class BranchDetailsModule { }
