import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../../service/service";
@Component({
  selector: 'app-subadmindetails',
  templateUrl: './subadmindetails.component.html',

})
export class SubadmindetailsComponent implements OnInit {
  slideOpen: any = false;
  _ShowMap = false;
  _DateDiff: any = {};
  public _SubAccountDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      RoleName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
  }

  myFunction() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");

  }

  DisplayDiv() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }

  ngOnInit() {

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([
          this._HelperService.AppConfig.Pages.System.NotFound
        ]);
      } else {
        this._ShowMap = true;
        this._HelperService._InitMap();
        Feather.replace();
        this._HelperService.AppConfig.ShowHeader = true;
        this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.Acquirer;
        this._HelperService.ContainerHeight = window.innerHeight;
        this.GetSubAccountDetails();
        this.Form_EditUser_Load();
      }
    });


  }

  EditBranch() {
    this._HelperService.AppConfig.ActiveReferenceKey = this._SubAccountDetails.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = this._SubAccountDetails.ReferenceId;
    this.Form_EditUser_Show();
  }

  GetSubAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccount,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts, pData);
    _OResponse.subscribe(
      _Response => {



        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._SubAccountDetails = _Response.Result;

          // 
          if (this._SubAccountDetails != undefined && this._SubAccountDetails.MobileNumber != undefined && this._SubAccountDetails.MobileNumber != null) {
            if (this._SubAccountDetails.MobileNumber.startsWith("234")) {
              this._SubAccountDetails.MobileNumber = this._SubAccountDetails.MobileNumber.substring(3, this._SubAccountDetails.length);
            }
          }
          // 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          this._HelperService._ReLocate();

          this._DateDiff = this._HelperService._DateRangeDifference(this._SubAccountDetails.StartDate, this._SubAccountDetails.EndDate);
          this._SubAccountDetails.StartDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.StartDate
          );
          this._SubAccountDetails.EndDateS = this._HelperService.GetDateS(
            this._SubAccountDetails.EndDate
          );
          this._SubAccountDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.CreateDate
          );
          this._SubAccountDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._SubAccountDetails.ModifyDate
          );
          this._SubAccountDetails.StatusI = this._HelperService.GetStatusIcon(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusB = this._HelperService.GetStatusBadge(
            this._SubAccountDetails.StatusCode
          );
          this._SubAccountDetails.StatusC = this._HelperService.GetStatusColor(
            this._SubAccountDetails.StatusCode
          );



        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateSubAccount,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,

      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      RoleId: [7, Validators.required],
      RoleKey: ['admin', Validators.required],


      Name: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(128)
        ])
      ],
      MobileNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(14)
        ])
      ],

      EmailAddress: [
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(2)
        ])
      ],
      StatusCode: this._HelperService.AppConfig.Status,

    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          // this.Form_EditUser_Clear();
          this._HelperService.CloseModal('Form_EditUser_Content');

          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

}
