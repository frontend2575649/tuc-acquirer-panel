import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { DaterangePickerComponent } from "ng2-daterangepicker";
import { ChangeContext } from "ng5-slider";
import { Observable } from "rxjs";
import swal from "sweetalert2";
import { DataHelperService, HelperService, OList, OResponse, OSelect } from "../../../../service/service";
declare var moment: any;

@Component({
    selector: "tu-campaign",
    templateUrl: "./campaign.component.html"
})
export class TUCampaignComponent implements OnInit {
    public CampaignKey = null;
    CampaignRewardType_FixedAmount = "fixedamount";
    CampaignRewardType_AmountPercentage = "amountpercentage";
    @ViewChild(DaterangePickerComponent)
    private _DaterangePickerComponent: DaterangePickerComponent;
    public _CampaignDetails =
        {
            ReferenceId: null,
            ReferenceKey: null,
            TypeCode: null,
            TypeName: null,
            SubTypeCode: null,
            SubTypeName: null,
            UserAccountKey: null,
            UserAccountDisplayName: null,
            Name: null,
            Description: null,
            StartDate: null,
            StartDateS: null,
            EndDate: null,
            EndDateS: null,
            SubTypeValue: null,
            MinimumInvoiceAmount: null,
            MaximumInvoiceAmount: null,
            MinimumRewardAmount: null,
            MaximumRewardAmount: null,
            ManagerKey: null,
            ManagerDisplayName: null,
            SmsText: null,
            Comment: null,
            CreateDate: null,
            CreatedByKey: null,
            CreatedByDisplayName: null,
            ModifyDate: null,
            ModifyByKey: null,
            ModifyByDisplayName: null,
            StatusId: null,
            StatusCode: null,
            StatusName: null,

            CreateDateS: null,
            ModifyDateS: null,
            StatusI: null,
            StatusB: null,
            StatusC: null,
        }

    public CampaignType = [
        {
            'TypeCode': 'owncardcustomers',
            'Name': 'Own Bank Card Customers'
        },
        {
            'TypeCode': 'othercardcustomers',
            'Name': 'Other Bank Card Customers'
        }
    ];

    public CampaignRewardType = [
        {
            'TypeCode': 'fixedamount',
            'Name': 'Fixed Amount'
        },
        {
            'TypeCode': 'amountpercentage',
            'Name': 'Percentage'
        }
    ];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this.GetCampaignDetails();
            }
        });
        this.Form_AddUser_Load();
        // this.TUTr_Setup();
        this.TUTr_Filter_Merchants_Load();
        this.TUTr_Filter_UserAccounts_Load();
        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Providers_Load();
        this.TUTr_Filter_Issuers_Load();
        this.TUTr_Filter_CardBrands_Load();
        this.TUTr_Filter_TransactionTypes_Load();
        this.TUTr_Filter_CardBanks_Load();
    }

    GetCampaignDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaign,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.IsFormProcessing = false;
                    this._CampaignDetails = _Response.Result;
                    if (this._DaterangePickerComponent != undefined) {
                        this._DaterangePickerComponent.datePicker.setStartDate(moment(this._CampaignDetails.StartDate).utc().format("YYYY-MM-DD"));
                        this._DaterangePickerComponent.datePicker.setEndDate(moment(this._CampaignDetails.EndDate).utc().format("YYYY-MM-DD"));

                    }
                    this._CampaignDetails.StartDateS = this._HelperService.GetDateS(
                        this._CampaignDetails.StartDate
                    );
                    this._CampaignDetails.EndDateS = this._HelperService.GetDateS(
                        this._CampaignDetails.EndDate
                    );
                    this._CampaignDetails.CreateDateS = this._HelperService.GetDateTimeS(
                        this._CampaignDetails.CreateDate
                    );
                    this._CampaignDetails.ModifyDateS = this._HelperService.GetDateTimeS(
                        this._CampaignDetails.ModifyDate
                    );
                    this._CampaignDetails.StatusI = this._HelperService.GetStatusIcon(
                        this._CampaignDetails.StatusCode
                    );
                    this._CampaignDetails.StatusB = this._HelperService.GetStatusBadge(
                        this._CampaignDetails.StatusCode
                    );
                    this._CampaignDetails.StatusC = this._HelperService.GetStatusColor(
                        this._CampaignDetails.StatusCode
                    );
                    //this.TUTr_Setup();
                    // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],


                    // : [null, Validators.compose([Validators.required])],

                    // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
                    // : 0,

                    // : 0,
                    // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(5)])],

                    // StartDate: [null, Validators.compose([Validators.required])],
                    // EndDate: [null, Validators.compose([Validators.required])],

                    this.Form_AddUser.patchValue(
                        {
                            Name: this._CampaignDetails.Name,
                            Description: this._CampaignDetails.Description,
                            TypeCode: this._CampaignDetails.TypeCode,
                            SubTypeCode: this._CampaignDetails.SubTypeCode,
                            SubTypeValue: this._CampaignDetails.SubTypeValue,
                            MinimumInvoiceAmount: this._CampaignDetails.MinimumInvoiceAmount,
                            MaximumInvoiceAmount: this._CampaignDetails.MaximumInvoiceAmount,
                            MinimumRewardAmount: this._CampaignDetails.MinimumRewardAmount,
                            MaximumRewardAmount: this._CampaignDetails.MaximumRewardAmount,
                            TStartDate: moment(this._CampaignDetails.StartDate).format('DD-MM-YYYY') + ' - ' + moment(this._CampaignDetails.EndDate).format('DD-MM-YYYY'),
                            StartDate: this._CampaignDetails.StartDate,
                            EndDate: this._CampaignDetails.EndDate,
                        }
                    );
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    RewardTypeChange() {
        this.Form_AddUser.patchValue(
            {
                SubTypeValue: 0,
                MinimumInvoiceAmount: 0,
                MaximumInvoiceAmount: 0,
                MinimumRewardAmount: 0,
                MaximumRewardAmount: 0,
            }
        );
    }

    SetCampaignType(TypeName) {
        this.Form_AddUser.patchValue(
            {
                TypeCode: TypeName,
            }
        );
    }

    ScheduleDateRangeChange(value) {
        this.Form_AddUser.patchValue(
            {
                TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
            }
        );
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start,
                EndDate: value.end,
            }
        );
    }
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this.Form_AddUser = this._FormBuilder.group({
            Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateCampaign,
            ReferenceKey: this._CampaignDetails.ReferenceKey,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            Description: null,
            TypeCode: [null, Validators.compose([Validators.required])],
            SubTypeCode: [null, Validators.compose([Validators.required])],
            SubTypeValue: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],

            MinimumInvoiceAmount: 0,
            MaximumInvoiceAmount: 0,

            MinimumRewardAmount: 0,
            MaximumRewardAmount: 0,

            TStartDate: [null, Validators.compose([Validators.required])],
            StartDate: [null, Validators.compose([Validators.required])],
            EndDate: [null, Validators.compose([Validators.required])],
        });
    }
    Form_AddUser_Process(_FormValue: any) {
        if (_FormValue.StartDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.EndDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward amount");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward amount must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward percentage");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward percentage must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue > 100) {
            this._HelperService.NotifyError("Reward percentage must be between than 1 - 100");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.MaximumRewardAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.MinimumInvoiceAmount == undefined) {
            this._HelperService.NotifyError("Enter minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.MinimumInvoiceAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else {
            swal({
                position: "top",
                title: "Update campaign ?",
                text: "Click on continue button to update campaign? ",
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
            }).then(result => {
                if (result.value) {
                    _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
                    _FormValue.StartDate = new Date();
                    _FormValue.EndDate = new Date();
                    this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this.GetCampaignDetails();
                                this._HelperService.NotifySuccess("Campaign updated successfully");
                            } else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            // console.log(_Error);
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }


    }


    UpdateCampaignStatus(status) {
        swal({
            position: "top",
            title: "Update campaign ?",
            text: "Update campaign status? ",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                var pData =
                {
                    Task: "updatecampaign",
                    ReferenceKey: this._CampaignDetails.ReferenceKey,
                    StatusCode: status,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Campaign deleted successfully");
                            // this.Form_UpdateUser_Close();
                            this.GetCampaignDetails();
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    DeleteCampaign() {
        swal({
            position: "top",
            title: "Delete campaign ?",
            text: "Details cannot be recovered after deletion, do you want to continue ? ",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                var pData =
                {
                    Task: "deletecampaign",
                    ReferenceKey: this._CampaignDetails.ReferenceKey,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Campaign status udpated successfully");
                            // this.Form_UpdateUser_Close();
                            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }
    DuplicateCampaign() {
        swal({
            position: "top",
            title: "Duplicate campaign ?",
            text: "Click on continue to duplicate campaign",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
        }).then(result => {
            if (result.value) {
                var pData =
                {
                    Task: "duplicatecampaign",
                    ReferenceKey: this._CampaignDetails.ReferenceKey,
                };
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(
                    this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess("Campaign duplicated successfully");
                            // this.Form_UpdateUser_Close();
                            this._HelperService.AppConfig.ActiveReferenceKey = _Response.Result;
                            this.GetCampaignDetails();
                        } else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    }
                );
            }
        });
    }




    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'InvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }



    TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
    TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
    TUTr_RewardRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'RewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_RewardRangeMinAmount = changeContext.value;
        this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'RewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
        if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
            {
                Id: null,
                Sort:null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaignTransactions,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
                Title: 'Rewards History',
                StatusType: 'transaction',
                SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'CampaignId', this._HelperService.AppConfig.DataType.Number, this._CampaignDetails.ReferenceId, '='),
                Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
                Type: this._HelperService.AppConfig.ListType.All,
                DefaultSortExpression: 'TransactionDate desc',
                TableFields: [
                    {
                        DisplayName: '#',
                        SystemName: 'ReferenceId',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Show: true,
                        Search: false,
                        Sort: false,
                        // NavigateField: 'UserAccountKey',
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer,
                    },
                    {
                        DisplayName: 'Date',
                        SystemName: 'TransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: true,
                        IsDateSearchField: true,
                    },
                    {
                        DisplayName: 'Transaction Status',
                        SystemName: 'StatusName',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'User',
                        SystemName: 'UserDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Mobile Number',
                        SystemName: 'UserMobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Type',
                        SystemName: 'TypeName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Invoice Amount',
                        SystemName: 'InvoiceAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Reward Amount',
                        SystemName: 'RewardAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: 'text-grey',
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'User Reward Amount',
                        SystemName: 'UserAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },
                    {
                        DisplayName: 'Commission Amount',
                        SystemName: 'CommissionAmount',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Show: true,
                        Search: false,
                        Sort: true,
                    },

                    {
                        DisplayName: 'Card Number',
                        SystemName: 'AccountNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Type',
                        SystemName: 'CardBrandName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Card Bank Provider',
                        SystemName: 'CardBankName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'TUC Card Number',
                        SystemName: 'TUCCardNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Merchant',
                        SystemName: 'ParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Store',
                        SystemName: 'SubParentDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Acquirer / Bank',
                        SystemName: 'AcquirerDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Terminal Provider',
                        SystemName: 'ProviderDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Issuer (Done by)',
                        SystemName: 'CreatedByDisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Show: true,
                        Search: false,
                        Sort: false,
                    },
                    {
                        DisplayName: 'Transaction Reference',
                        SystemName: 'ReferenceNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: false,
                        Sort: false,
                        ResourceId: null,
                    },


                ]
            }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        if (this._CampaignDetails.ReferenceId != undefined && this._CampaignDetails.ReferenceId != '') {
            this.TUTr_GetData();
        }
    }
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }
    TUTr_ToggleOption(event: any, Type: any) {
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (this.TUTr_Config.RefreshData == true) {
            this.TUTr_GetData();
        }
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    public TUTr_Filter_UserAccount_Option: Select2Options;
    public TUTr_Filter_UserAccount_Selected = 0;
    TUTr_Filter_UserAccounts_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "MobileNumber",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.AppUser
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_UserAccount_Option = {
            placeholder: 'Search Customer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_UserAccounts_Change(event: any) {
        if (event.value == this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_UserAccount_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_UserAccount_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'UserAccountId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_UserAccount_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_Merchant_Option: Select2Options;
    public TUTr_Filter_Merchant_Selected = 0;
    TUTr_Filter_Merchants_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Merchant
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Search By Merchant',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_Merchants_Change(event: any) {
        if (event.value == this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Merchant_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Merchant_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Store_Toggle = true;

        this.TUTr_Filter_Stores_Load();
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
            this.TUTr_Filter_Store_Toggle = false;
        }, 500);
    }

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.Store
                }
            ]
        };

        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SubParentId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }


    public TUTr_Filter_Provider_Option: Select2Options;
    public TUTr_Filter_Provider_Selected = 0;
    TUTr_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Provider_Option = {
            placeholder: 'Search By Provider',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Providers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Provider_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Provider_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        this.TUTr_Filter_Issuers_Load();
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);

    }


    public TUTr_Filter_Issuer_Option: Select2Options;
    public TUTr_Filter_Issuer_Selected = 0;
    TUTr_Filter_Issuers_Load() {
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }]
        };


        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.PosTerminal,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.Cashier,
            ], "=");

        if (this.TUTr_Filter_Provider_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Provider_Selected, '=');
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BankId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');

        if (this.TUTr_Filter_Store_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrictOr(_Select.SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
        }

        if (this.TUTr_Filter_Merchant_Selected != 0) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'SubOwnerOwnerId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Merchant_Selected, '=');
        }
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Issuer_Option = {
            placeholder: 'Search By Issuer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Issuers_Change(event: any) {
        if (event.value == this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Issuer_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Issuer_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Issuer_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_TransactionSources_Selected = 0;
    TUTr_Filter_TransactionSources_Change(event: number) {
        var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '=');
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_Filter_TransactionSources_Selected = event;
        if (this.TUTr_Filter_TransactionSources_Selected != 0) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '=');
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'SourceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionSources_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    public TUTr_Filter_TransactionType_Option: Select2Options;
    public TUTr_Filter_TransactionType_Selected = 0;
    TUTr_Filter_TransactionTypes_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreHelpersLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionType
                },
                {
                    SystemName: "SubParentCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.TransactionTypeReward
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_TransactionType_Option = {
            placeholder: 'Search By Tran. Type',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TUTr_Filter_TransactionTypes_Change(event: any) {
        if (event.value == this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_TransactionType_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_TransactionType_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'TypeId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_TransactionType_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_CardBrand_Option: Select2Options;
    public TUTr_Filter_CardBrand_Selected = 0;
    TUTr_Filter_CardBrands_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "TypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.HelperTypes.CardBrand
                }]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBrand_Option = {
            placeholder: 'Search By Card Brand',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_CardBrands_Change(event: any) {
        if (event.value == this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBrand_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBrand_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBrandId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBrand_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    public TUTr_Filter_CardBank_Option: Select2Options;
    public TUTr_Filter_CardBank_Selected = 0;
    TUTr_Filter_CardBanks_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreCommonsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields:
                [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                    {
                        SystemName: "TypeCode",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        SearchCondition: "=",
                        SearchValue: this._HelperService.AppConfig.HelperTypes.CardBank
                    }
                ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_CardBank_Option = {
            placeholder: 'Search By Card Bank',
            ajax: _Transport,
            multiple: false,
            allowClear: true,

        };
    }

    TUTr_Filter_CardBanks_Change(event: any) {
        if (event.value == this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_CardBank_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_CardBank_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'CardBankId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_CardBank_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }




}

