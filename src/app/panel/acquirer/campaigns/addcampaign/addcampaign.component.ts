import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
    OSelect,
    OList,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon
} from "../../../../service/service";
import swal from "sweetalert2";
declare var moment: any;

@Component({
    selector: "tu-addcampaign",
    templateUrl: "./addcampaign.component.html"
})
export class TUAddCampaignComponent implements OnInit {


    public CampaignKey = null;

    public CampaignType = [
        {
            'TypeCode': 'owncardcustomers',
            'Name': 'Own Bank Card Customers'
        },
        {
            'TypeCode': 'othercardcustomers',
            'Name': 'Other Bank Card Customers'
        }
    ];
    CampaignRewardType_FixedAmount = "fixedamount";
    CampaignRewardType_AmountPercentage = "amountpercentage";
    public CampaignRewardType = [
        {
            'TypeCode': 'fixedamount',
            'Name': 'Fixed Amount'
        },
        {
            'TypeCode': 'amountpercentage',
            'Name': 'Percentage'
        }
    ];

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
        this.TUTr_Filter_Merchant_Load();
    }

    ScheduleDateRangeChange(value) {

        this.Form_AddUser.patchValue(
            {
                TStartDate: this._HelperService.DateInUTC(moment(value.start)).format('DD-MM-YYYY') + ' - ' + this._HelperService.DateInUTC(moment(value.end)).format('DD-MM-YYYY'),
            }
        );
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start,
                EndDate: value.end,
            }
        );
    }

    SetCampaignType(TypeName) {
        this.Form_AddUser.patchValue(
            {
                TypeCode: TypeName,
            }
        );
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Close() {
        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns,
        ]);
    }
    Form_AddUser_Load() {
        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCampaign,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(50)])],
            Description: null,
            TypeCode: [null, Validators.compose([Validators.required])],
            SubTypeCode: [null, Validators.compose([Validators.required])],
            SubTypeValue: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],

            MinimumInvoiceAmount: 0,
            MaximumInvoiceAmount: 0,

            MinimumRewardAmount: 0,
            MaximumRewardAmount: [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
            TStartDate: [null, Validators.compose([Validators.required])],
            StartDate: [null, Validators.compose([Validators.required])],
            EndDate: [null, Validators.compose([Validators.required])],

            StatusCode: this._HelperService.AppConfig.Status.Campaign.Creating,
        });
    }

    RewardTypeChange() {
        this.Form_AddUser.patchValue(
            {
                SubTypeValue: 0,
                MinimumInvoiceAmount: 0,
                MaximumInvoiceAmount: 0,
                MinimumRewardAmount: 0,
                MaximumRewardAmount: 0,
            }
        );
    }
    Form_AddUser_Clear() {
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {
        if (_FormValue.StartDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.EndDate == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward amount");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_FixedAmount && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward amount must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Enter reward percentage");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue < 1) {
            this._HelperService.NotifyError("Reward percentage must be greater than 0");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.SubTypeValue > 100) {
            this._HelperService.NotifyError("Reward percentage must be between than 1 - 100");
        }
        else if (_FormValue.SubTypeCode == this.CampaignRewardType_AmountPercentage && _FormValue.MaximumRewardAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.SubTypeValue == undefined) {
            this._HelperService.NotifyError("Please select campaign Schedule");
        }
        else if (_FormValue.MinimumInvoiceAmount == undefined) {
            this._HelperService.NotifyError("Enter minimum purchase amount. Set to 0 for no limit");
        }
        else if (_FormValue.MinimumInvoiceAmount < 0) {
            this._HelperService.NotifyError("Enter valid minimum purchase amount. Set to 0 for no limit");
        }
        else {
            swal({
                title: "Create campaign ?",
                text: "Please make sure all data is correct. You can not edit campaign after publish",
                showCancelButton: true,
                position: this._HelperService.AppConfig.Alert_Position,
                animation: this._HelperService.AppConfig.Alert_AllowAnimation,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
                allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
                confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
            }).then(result => {
                if (result.value) {
                    _FormValue.Merchants = this._Merchants;
                    this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess("Campaign created successfully");
                                this.Form_AddUser_Clear();
                                if (_FormValue.OperationType == "close") {
                                    this.Form_AddUser_Close();
                                }
                                else {
                                    this.UpdateCampaignStatus(_Response.Result);
                                }

                            } else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        }
                    );
                }
            });
        }



    }

    UpdateCampaignStatus(CReferenceKey: string) {

        var pData =
        {
            Task: "updatecampaign",
            ReferenceKey: CReferenceKey,
            StatusCode: this._HelperService.AppConfig.Status.Campaign.Published
        };
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Campaign published successfully");
                    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );

    }
    ShowMerchantSelection = true;
    public ToggleMerchantSelect: boolean = false;
    public TUTr_Filter_Merchant_Option: Select2Options;
    public merchantId: number = null;
    public StoreTogglefield: boolean = false;
    TUTr_Filter_Merchant_Load() {

        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: "api/v3/con/accounts/",
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        };

        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Merchant_Option = {
            placeholder: 'Select Merchant',
            ajax: _Transport,
            multiple: true,
            allowClear: true,
        };
    }
    _Merchants = [];
    TUTr_Filter_Merchant_Change(event: any) {
        this._Merchants = [];
        console.log(event);
        if (event != undefined && event != null) {
            event.data.forEach(element => {
                this._Merchants.push(
                    {
                        ReferenceId: element.ReferenceId,
                        ReferenceKey: element.ReferenceKey,
                    }
                )
            });
        }

        //   this.Form_AddStore.patchValue(
        //     {
        //       AccountId: event.data[0].ReferenceId,
        //       AccountKey: event.data[0].ReferenceKey,
        //       MerchantName: event.data[0].DisplayName,
        //     }


        //   );



        // if(this.merchantId  != null){
        //   this.TUTr_Filter_Stores_Load();
        //   this.ToggleStoreSelect = true;
        //   this._ChangeDetectorRef.detectChanges();
        // }

    }

}
