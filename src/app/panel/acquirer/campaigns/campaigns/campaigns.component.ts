import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
declare var $: any;
import {
  DataHelperService,
  HelperService,
  OList,
  FilterHelperService,
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-campaigns",
  templateUrl: "./campaigns.component.html",
})
export class TUCampaignsComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  SaveEditValue: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "Campaign ID ",
      Value: true,
    },
    {
      Name: "Type ",
      Value: true,
    },

    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "Start",
      Value: true,
    },
    {
      Name: "End",
      Value: true,
    },
  ];
  EditColoum: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "Campaign ID ",
      Value: true,
    },
    {
      Name: "Type ",
      Value: true,
    },

    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "Start",
      Value: true,
    },
    {
      Name: "End",
      Value: true,
    },
  ];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) { 
    this._HelperService.ShowDateRange = false;
  }
  ngOnInit() {
    Feather.replace();
    this.UserAccounts_Setup();

    var temp = this._HelperService.GetStorage("BCampaignTable");
    if (temp != undefined && temp != null) {
      this.EditColoum = temp.config;
      this.SaveEditValue = JSON.parse(JSON.stringify(temp.config));
    }
  }

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = null;

  SetOtherFilters(): void {
    // this.UserAccounts_Config.SearchBaseConditions = [];
    // this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict(
    //   "",
    //   "UserAccountKey",
    //   this._HelperService.AppConfig.DataType.Text,
    //   this._HelperService.AppConfig.ActiveOwnerKey,
    //   "="
    // );


    this.UserAccounts_Config.SearchBaseConditions = [];
    this.UserAccounts_Config.SearchBaseCondition = null;
    this.UserAccounts_Config.SearchBaseCondition= this._HelperService.GetSearchConditionStrict(
      "",
      "UserAccountKey",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.ActiveOwnerKey,
      "="
    );
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Campaign));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }


  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "UserAccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.UserAccounts_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.UserAccounts_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "UserAccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this.MerchantsList_Filter_Owners_Selected,
        "="
      );
      this.UserAccounts_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.UserAccounts_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.UserAccounts_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "UserAccountKey",
          this._HelperService.AppConfig.DataType.Text,
          this.MerchantsList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.UserAccounts_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  EditCol() {
    this._HelperService.OpenModal("EditCol");
  }
  SaveEditCol() {
    this.EditColoum = JSON.parse(JSON.stringify(this.SaveEditValue));
    this._HelperService.SaveStorage("BCampaignTable", {
      config: this.EditColoum,
    });
    this._HelperService.CloseModal("EditCol");
  }

  Open_AddCampaign() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns
        .AddCampaign,
    ]);
  }
  public UserAccounts_Config: OList;
  UserAccounts_Setup() {
    this.UserAccounts_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaigns,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign,
      Title: "All Campaigns",
      StatusType: "campaign",
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict(
        "",
        "UserAccountKey",
        this._HelperService.AppConfig.DataType.Text,
        this._HelperService.AppConfig.ActiveOwnerKey,
        "="
      ),
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: "StartDate desc",
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "Name",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Type",
          SystemName: "TypeName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Reward Type",
          SystemName: "SubTypeName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: "Start",
          SystemName: "StartDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField:true,
          ResourceId: null,
        },

        
        
        {
          DisplayName: "End",
          SystemName: "EndDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
      ],
    };

    this.UserAccounts_Config = this._DataHelperService.List_Initialize(
      this.UserAccounts_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Campaign,
      this.UserAccounts_Config
    );
    this.UserAccounts_GetData();
  }
  UserAccounts_ToggleOption(event: any, Type: any) {
    // if (event != null) {
    //   for (let index = 0; index < this.UserAccounts_Config.Sort.SortOptions.length; index++) {
    //     const element = this.UserAccounts_Config.Sort.SortOptions[index];
    //     if (event.SystemName == element.SystemName) {

    //       element.SystemActive = true;

    //     }
    //     else {
    //       element.SystemActive = false;

    //     }

    //   }
    // }

    // this._HelperService.Update_CurrentFilterSnap(
    //   event,
    //   Type,
    //   this.UserAccounts_Config
    // );
    // this.UserAccounts_Config = this._DataHelperService.List_Operations(
    //   this.UserAccounts_Config,
    //   event,
    //   Type
    // );
    // if (
    //   (this.UserAccounts_Config.RefreshData == true)
    //   && this._HelperService.DataReloadEligibility(Type)
    // ) {
    //   this.UserAccounts_GetData();
    // }

    if (event != null) {
      for (let index = 0; index < this.UserAccounts_Config.Sort.SortOptions.length; index++) {
        const element = this.UserAccounts_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.UserAccounts_Config


    );

    this.UserAccounts_Config = this._DataHelperService.List_Operations(
      this.UserAccounts_Config,
      event,
      Type
    );

    if (
      (this.UserAccounts_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.UserAccounts_GetData();
    }

  }





  UserAccounts_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.UserAccounts_Config
    );
    this.UserAccounts_Config = TConfig;
  }
  UserAccounts_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCampaign,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Campaign,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns
        .Sales,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetCampaignConfig(this.UserAccounts_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.UserAccounts_GetData();
  }

  // Save_NewFilter() {
  //   swal({
  //     position: "center",
  //     title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
  //     text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
  //     input: "text",
  //     inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
  //     inputAttributes: {
  //       autocapitalize: "off",
  //       autocorrect: "off",
  //       //maxLength: "4",
  //       minLength: "4",
  //     },
  //     animation: false,
  //     customClass: this._HelperService.AppConfig.Alert_Animation,
  //     showCancelButton: true,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //     cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //     confirmButtonText: "Save",
  //     cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //   }).then((result) => {
  //     if (result.value) {
  //       this._HelperService._RefreshUI = false;
  //       this._ChangeDetectorRef.detectChanges();

  //       this._FilterHelperService._BuildFilterName_Store(result.value);
  //       this._HelperService.Save_NewFilter(
  //         this._HelperService.AppConfig.FilterTypeOption.Campaign
  //       );

  //       this._HelperService._RefreshUI = true;
  //       this._ChangeDetectorRef.detectChanges();
  //     }
  //   });
  // }



   Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Campaign
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: "Delete View?",
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Campaign
        );
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
        this.UserAccounts_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.UserAccounts_GetData();

    if (ButtonType == 'Sort') {
      $("#UserAccounts_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#UserAccounts_fdropdown").dropdown('toggle');
    }

  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetCampaignConfig(this.UserAccounts_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.UserAccounts_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetCampaignConfig(this.UserAccounts_Config);

    //#region setOtherFilters

    this.SetOtherFilters();

    //#endregion

    this.UserAccounts_GetData();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }
}
