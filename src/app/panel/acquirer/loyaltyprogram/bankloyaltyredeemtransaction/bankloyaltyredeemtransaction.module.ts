import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankloyaltyredeemtransactionComponent } from './bankloyaltyredeemtransaction.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Ng5SliderModule } from 'ng5-slider';

const routes: Routes = [{ path: "", component: BankloyaltyredeemtransactionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankloyaltyredeemtransactionRoutingModule { }

@NgModule({
  declarations: [BankloyaltyredeemtransactionComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BankloyaltyredeemtransactionRoutingModule,
    Select2Module,
    Daterangepicker,    
    Ng2FileInputModule,
    Ng5SliderModule,
    NgxPaginationModule,
  ]
})
export class BankloyaltyredeemtransactionModule { }
