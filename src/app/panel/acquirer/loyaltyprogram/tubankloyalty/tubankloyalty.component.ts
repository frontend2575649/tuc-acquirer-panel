import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { HCoreXAddress, HCXAddressConfig, locationType } from '../../../../component/hcxaddressmanager/hcxaddressmanager.component';
import { DataHelperService } from '../../../../service/datahelper.service';
import { HelperService } from '../../../../service/helper.service';
import { OUserDetails, OResponse } from '../../../../service/object.service';
import * as Feather from 'feather-icons';
declare let $: any;
declare var moment: any;

@Component({
  selector: 'app-tubankloyalty',
  templateUrl: './tubankloyalty.component.html',
})
export class TubankloyaltyComponent implements OnInit {

  public _isAddressLoaded = false;
  public programDetails: any = {};
  public _Address: HCoreXAddress = {};
  reward: any;
  comission: any;
  public minInvoiceAmt: any = 0;
  AddressChange(Address) {
    this._Address = Address;
  }
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }



  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  isLoaded: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ResetDateRange();
  }

  //#region mapCorrection 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
    if (this.ExpandedView) {
      // this._HelperService._MapCorrection();
    }
  }

  //#endregion

  //#region DetailShowHIde 

  HideStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  InitBackDropClickEvent(): void {
    // var backdrop: HTMLElement = document.getElementById("backdrop");

    // backdrop.onclick = () => {
    //   $(this.divView.nativeElement).removeClass('show');
    //   backdrop.classList.remove("show");
    // };
  }
  currentProgramDetails: any = {};
  acquireDetails: any = {};

  ngOnInit() {

    //#region GetStorageData 

    // var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    // if (StorageDetails != null) {
    //   this._HelperService.AppConfig.ActiveMerchantReferenceKey = StorageDetails.ReferenceKey;
    //   this._HelperService.AppConfig.ActiveMerchantReferenceId = StorageDetails.ReferenceId;
    //   this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
    //   this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    // }

    //#endregion
    //#region UIInitialize 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;

    // this._HelperService._InitMap();
    this.InitBackDropClickEvent();

    this.HideStoreDetail();
    this.acquireDetails = this._HelperService.GetStorage('hca').UserAccount.ProgramDetails;
    // if (this.acquireDetails.ProgramDetails && this.acquireDetails.ProgramDetails.length > 0) {
    //   this.currentProgramDetails = this.acquireDetails.ProgramDetails[0];
    //   this.GetProgramDetails();
    // }
    // if (this._HelperService.acquirerProgramDetails && this._HelperService.acquirerProgramDetails.length > 0) {
    //   this.currentProgramDetails = this._HelperService.acquirerProgramDetails[0];
    //   this.GetProgramDetails();

    // }
    this.GetProgramDetails();

    //#endregion

    //#region InitEditForm 

    // this.FormA_EditUser_Load();
    // this.FormB_EditUser_Load();
    // this.FormC_EditUser_Load();
    // this.GetStateCategories();

    //#endregion

    // this.GetMerchantDetails();

  }

  openComissionDialogue() {
    this._HelperService.OpenModal('EditComission')
  }

  openRewardDialogue() {
    this._HelperService.OpenModal('EditReward')
  }

  openInvoiceAmtDialogue() {
    this._HelperService.OpenModal('EditMinInvoiceAmt')
  }

  updateProgram(type) {
    if (this.reward < 0 || this.reward > 100) {
      this._HelperService.NotifyError("Reward Percentage should be between 0 to 100");
      return;
    }

    if(this.minInvoiceAmt < 0){
      this._HelperService.NotifyError("Min invoice amount should be greater than 0");
      return;
    }

    const pdata = {
      Task: "updateprogram",
      ReferenceId: this.acquireDetails.ProgramId,
      ReferenceKey: this.acquireDetails.ProgramReferenceKey,
      ProgramTypeCode: "programtype.grouployalty",
      SubscriptionId: this.programDetails.SubscriptionId,
      ComissionTypeCode: "commissiontype.zero",
      Comission: this.comission,
      RewardCap: this.reward,
      MinInvoiceAmount: this.minInvoiceAmt

    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pdata);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;
          this._HelperService.NotifySuccess("Program updated successfully!");
          this.GetProgramDetails();
          if (type === 'comission') {
            this._HelperService.CloseModal('EditComission')
          } else if (type === 'minInvoiceAmt') {
            this._HelperService.CloseModal('EditMinInvoiceAmt')
          } else {
            this._HelperService.CloseModal('EditReward')

          }
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  saveReward() {

  }

  //#region MerchantDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      Address: null,
      AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },

    }

  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this._MerchantDetails = _Response.Result;
          this.isLoaded = true;
          this._Address = this._MerchantDetails.AddressComponent;
          this._isAddressLoaded = true;
          //#region RelocateMarker 

          setTimeout(() => {
            this._isAddressLoaded = true;
          }, 300);

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          // this.FormC_EditUser_Latitude = this._MerchantDetails.Latitude;
          // this.FormC_EditUser_Longitude = this._MerchantDetails.Longitude;
          // this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          // this.FormC_EditUser_Longitude = _Response.Result.Longitude;
          if (this._MerchantDetails != undefined && this._MerchantDetails.MobileNumber != undefined && this._MerchantDetails.MobileNumber != null) {
            if (this._MerchantDetails.MobileNumber.startsWith("234")) {
              this._MerchantDetails.MobileNumber = this._MerchantDetails.MobileNumber.substring(3, this._MerchantDetails.length);
            }
          }

          if (this._MerchantDetails.ContactPerson != undefined && this._MerchantDetails.ContactPerson.MobileNumber != undefined && this._MerchantDetails.ContactPerson.MobileNumber != null) {
            if (this._MerchantDetails.ContactPerson.MobileNumber.startsWith("234")) {
              this._MerchantDetails.ContactPerson.MobileNumber = this._MerchantDetails.ContactPerson.MobileNumber.substring(3, this._MerchantDetails.ContactPerson.length);
            }
          }
          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 

          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetProgramDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: "getprogram",
      ReferenceKey: this.acquireDetails.ProgramReferenceKey,
      ReferenceId: this.acquireDetails.ProgramId
    }
    // console.log("pData........", pData)
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Program, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.programDetails = _Response.Result;
          this.reward = this.programDetails.RewardCap;
          this.comission = this.programDetails.Comission;
          this.minInvoiceAmt = this.programDetails.MinInvoiceAmount;
          this.isLoaded = true;
          this.programDetails.StartDateS = this._HelperService.GetDateS(
            this.programDetails.CreateDate
          );

          this.programDetails.StatusI = this._HelperService.GetStatusIcon(
            this.programDetails.StatusCode
          );
          this.programDetails.StatusB = this._HelperService.GetStatusBadge(
            this.programDetails.StatusCode
          );
          this.programDetails.StatusC = this._HelperService.GetStatusColor(
            this.programDetails.StatusCode
          );
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

}
export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}