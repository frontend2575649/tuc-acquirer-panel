import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';


@Component({
    selector: 'app-addbranch',
    templateUrl: './addbranch.component.html',

})
export class AddbranchComponent implements OnInit {

    public _Address: HCoreXAddress = {};
    // public _AddressStore: HCoreXAddress = {};
    AddressChange(Address) {
        this._Address = Address;
        // this._AddressStore = Address;
    }

    public isActive: boolean = false;
    public _ShowMap = false;
    subscription: any;
    public _Roles = [
        {
            'id': 0,
            'text': 'Select Roles',
            'apival': 'Select Roles'
        },
        {
            'id': 6,
            'text': 'Manager',
            'apival': 'manager'
        },
        {
            'id': 7,
            'text': 'Administratior/Sub Account',
            'apival': 'admin'
        },
        {
            'id': 8,
            'text': 'RM',
            'apival': 'rm'
        }
    ];
    _CurrentAddress: any = {};
    _ShowInput: boolean = false;
    _SubRegions: any[] = [];
    select2: boolean = true;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
    }


    ngOnInit() {
        Feather.replace();
        this._ShowMap = true;
        this._HelperService._InitMap();
        this.subscription = this._HelperService.navchange
            .subscribe(item => {
                this._ChangeDetectorRef.detectChanges();
            });

        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onFinished: (event, currentIndex) => {
                if (this.isActive) {
                    if (this.MForm_AddUser.valid) {
                        this.Form_AddUser_Process(this.Form_AddUser.value);
                    }
                    else {
                        this._HelperService.NotifyError('Please Enter All Fields Value');
                    }
                } else {
                    if (this.ExistingManager.ReferenceKey) {
                        this.Form_AddUser_Process(this.Form_AddUser.value);
                    } else {

                        this._HelperService.NotifyError('Please Enter All Fields Value');
                    }

                }
            },
            onStepChanging: (event, currentIndex, newIndex) => {
                console.log(newIndex)
                if (currentIndex < newIndex) {
                    // Step 1 form validation
                    if (currentIndex === 0) {
                        var isStage1Valid: boolean = this.Form_AddUser.controls['Name'].valid
                            && this.Form_AddUser.controls['DisplayName'].valid
                            && this.Form_AddUser.controls['PhoneNumber'].valid
                            && this.Form_AddUser.controls['BranchCode'].valid
                            && this.Form_AddUser.controls['EmailAddress'].valid;
                        if (isStage1Valid) {
                            return true;
                        } else {
                            this._HelperService.NotifyError('Please Enter All Fields Value');
                        }
                    }
                    // Step 2 form validation
                    if (currentIndex === 1) {
                        var isStage2Valid: boolean = this.Form_AddUser.controls['CityName'].valid
                            && this.Form_AddUser.controls['StateName'].valid
                            // && this.Form_AddUser.controls['RegionName'].valid
                            && this.Form_AddUser.controls['Address'].valid
                        if (isStage2Valid) {

                            return true;
                        } else {
                            this._HelperService.NotifyError('Please Enter All Fields Value');
                        }
                    }

                    // Always allow step back to the previous step even if the current step is not valid.
                } else { return true; }
            }

        });

        this.Form_AddUser_Load();
        this.GetRoles_List()
        this.GetMangers_List();
        this.AssignManger_List();
        this.S2_Region_Load();
        this.MForm_AddUser_Load();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];
            // if (this._CoreDataHelper.OCoreParameter.ReferenceKey == null) {
            //     this._Router.navigate([this._DataStoreService.PageLinks.Core.NotFound]);
            // }
            // else {
            //     this.Get_Details();
            // } 
        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }

    MForm_AddUser: FormGroup;
    ExistingManager: any = {};

    MForm_AddUser_Load() {
        this.MForm_AddUser = this._FormBuilder.group({
            Name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            // RoleKey: [null],
            // RoleId: [null],
            OwnerKey: [null, Validators.required],
            OwnerId: [null, Validators.required],
            // ReferenceKey: [null, Validators.required],
            // ReferenceId: [null, Validators.required],

        });
    }
    MForm_AddUser_Clear() {
        this.MForm_AddUser.reset();
        this.MForm_AddUser_Load();
    }

    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;

        // let geocoder = new google.maps.Geocoder();
        // let latlng = { lat: event.coords.lat, lng: event.coords.lng };
        // geocoder.geocode({ 'location': latlng }, function (results) {
        //     if (results[0]) {
        //         console.log(results[0]);
        //     } else {
        //         console.log('No results found');
        //     }
        // });

        // this._CurrentAddress = event;
        // console.log(this._CurrentAddress);
    }


    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;

        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

        // console.log(this._CurrentAddress);

        this.select2 = false;
        this._ChangeDetectorRef.detectChanges();

        if (this._CurrentAddress.sublocality_level_1) {
            this._SubRegions.push({
                text: this._CurrentAddress.sublocality_level_1,
                id: 0
            });

            if (this._CurrentAddress.sublocality_level_2) {
                this._SubRegions.push({
                    text: this._CurrentAddress.sublocality_level_2,
                    id: 1
                });

                if (this._CurrentAddress.sublocality_level_3) {
                    this._SubRegions.push({
                        text: this._CurrentAddress.sublocality_level_3,
                        id: 2
                    });

                    if (this._CurrentAddress.sublocality_level_4) {
                        this._SubRegions.push({
                            text: this._CurrentAddress.sublocality_level_4,
                            id: 3
                        });
                    }
                }
            }
        }

        this.select2 = true;
        this._ChangeDetectorRef.detectChanges();

    }


    toogleShowInput(): void {
        this._ShowInput = !this._ShowInput;
    }

    Form_AddUser_Show() {
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Close() {

        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ReferredMerchants]);
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Load() {

        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveBranch,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            PhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            RegionName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            BranchCode: [null, Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(18)])],
            Latitude: 0,
            Longitude: 0,
            //RegionId: '',
            //RegionKey: '',
            StatusCode: this._HelperService.AppConfig.Status.Active,
            Manager: [null],
            Address: [null],
            StateName: [null],
            CityName: [null],

        });
    }
    Form_AddUser_Clear() {
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(_FormValue: any) {

        if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError('Please select business location');
        }
        else {
            swal({
                position: 'top',
                title: 'Create branch account ?',
                text: 'Please verify information and click on continue button to create branch',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                // confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {


                    _FormValue.Manager = (this.isActive) ? this.MForm_AddUser.value : this.ExistingManager;
                    // _FormValue.Longitude = this.Form_AddUser_Longitude;
                    // _FormValue.Latitude = this.Form_AddUser_Latitude;

                    _FormValue = this.ReFormat_RequestBody();
                    // this.MerchantSaveRequest.Address = this._Address.Address;

                    _FormValue.Address = this._Address,
                        _FormValue.AddressComponent = this._Address,

                        this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    // console.log("formvalue", _FormValue);
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess(_Response.Message);
                                this.Form_AddUser_Clear();
                                this.Form_AddUser_Load();
                                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Branch]);


                                if (_FormValue.OperationType == 'edit') {
                                    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.MerchantManager, _Response.Result.ReferenceKey]);
                                    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Merchant.Dashboard, _Response.Result.ReferenceKey]);
                                }
                                else if (_FormValue.OperationType == 'close') {
                                    this.Form_AddUser_Close();
                                }
                            }
                            else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
            });

        }
    }

    //City DropDown
    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;
    GetRoles_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: ['Name asc'],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'Name',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
        this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetRoles_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetRoles_Transport,
            multiple: false,
        };
    }

    GetRoles_ListChange(event: any) {

        this.Form_AddUser.controls['OwnerId'].setValue(null);
        this.Form_AddUser.controls['OwnerKey'].setValue(null);

        this.MForm_AddUser.patchValue(
            {
                RoleId: event.value,
                RoleKey: event.data[0].apival,



            }
        );

        this._ShowRole = false;
        this._ChangeDetectorRef.detectChanges();

        if (event.value == 8) { //RM
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 6; //Manager

            this.AssignManger_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();
        } else if (event.value == 7) { //SubAccount

            this._ShowReportingM = false;

            this.Form_AddUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
            this.Form_AddUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);

            this._MangerTypeId = undefined;
        }
        else if (event.value == 6) { //Manager
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 7; //Subaccount
            this.AssignManger_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();

        } else {
            this._MangerTypeId = undefined;
            this._ShowReportingM = true;
            this.AssignManger_List();
        }

        this._ShowRole = true;
        this._ChangeDetectorRef.detectChanges();

        this.AssignManger_List();
    }

    public _MangerTypeId: number;
    public _ShowRole: boolean = true;
    public _ShowReportingM: boolean = true;



    public AssignManger_Option: Select2Options;
    public AssignManger_Transport: any;
    AssignManger_List() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect;

        _Select =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
            ]
        }


        // if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
        //     _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
        // }

        this.AssignManger_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.AssignManger_Option = {
            placeholder: PlaceHolder,
            ajax: this.AssignManger_Transport,
            multiple: false,
        };
    }
    AssignManger_ListChange(event: any) {
        // console.log(event, "manager event");
        this.MForm_AddUser.patchValue(
            {
                OwnerId: event.data[0].ReferenceId,
                OwnerKey: event.data[0].ReferenceKey,

                // 
                // Name: event.data[0].Name,
                // MobileNumber: event.data[0].MobileNumber,
                // EmailAddress: event.data[0].EmailAddress
                // 
            }
        );
    }


    // Manager DropDown
    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: "AccountTypeCode",
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: "=",
                //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
                // }
            ]
        }

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '6', '=');
        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
        debugger;
        this.ExistingManager.ReferenceKey = event.data[0].ReferenceKey;
        this.ExistingManager.ReferenceId = event.data[0].ReferenceId;
        this.ExistingManager.EmailAddress = event.data[0].EmailAddress;
        this.ExistingManager.Name = event.data[0].Name;
        this.ExistingManager.MobileNumber = event.data[0].MobileNumber;

        this.MForm_AddUser.patchValue(
            {
                Name: event.data[0].Name,
                MobileNumber: event.data[0].MobileNumber,
                EmailAddress: event.data[0].EmailAddress,
                // ReferenceId: event.data[0].ReferenceId,
                // ReferenceKey: event.data[0].ReferenceKey,
            }
        );


    }


    //Region Drop Down
    public S2_Region_Option: Select2Options
    S2_Region_Load() {

        this.S2_Region_Option = {
            placeholder: 'Select Region',
            multiple: false,
            allowClear: false,
        };
    }

    S2_Region_Change(event: any) {

        this.Form_AddUser.patchValue(
            {
                RegionName: event.data[0].text
            }

        );

    }

    toogleIsActive(): void {
        this.isActive = !this.isActive;

        if (this.isActive) {
            this.ExistingManager = {};
        }

        // else {
        //     this.MForm_AddUser.controls['OwnerKey'].setValue(null);
        //     this.MForm_AddUser.controls['OwnerId'].setValue(0);
        // }
    }


    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        var MformValue: any = this.MForm_AddUser.value;

        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            AccountId: formValue.AccountId,
            AccountKey: formValue.AccountKey,
            BranchId: formValue.BranchId,
            BranchKey: formValue.BranchKey,
            RmId: formValue.RmId,
            RmKey: formValue.RmKey,

            DisplayName: formValue.DisplayName,
            // 
            // **********

            BranchCode: formValue.BranchCode,
            PhoneNumber: formValue.PhoneNumber,


            // ******************
            // 
            Name: formValue.Name,
            FirstName: formValue.FirstName,

            EmailAddress: formValue.EmailAddress,
            RewardPercentage: formValue.RewardPercentage,
            // 
            Address: this._Address.Address,
            AddressComponent: this._Address,
            // 



            Manager: {

                Name: MformValue.Name,
                MobileNumber: MformValue.MobileNumber,
                EmailAddress: MformValue.EmailAddress,
                ReferenceId: MformValue.ReferenceId, 
                ReferenceKey: MformValue.ReferenceKey,
            },

            // Stores: [],
            StatusCode: formValue.StatusCode

        };

        return formRequest;

    }

}
