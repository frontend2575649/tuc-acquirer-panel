import { ChangeDetectorRef, Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {



  public TodayDate: any;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit() {

    this._HelperService.FullContainer = false;

    this.TodayDate = this._HelperService.GetDateTime(new Date());

    //start time and end time for overview
    this.TodayStartTime = moment();
    this.TodayEndTime = moment();

    //start time and end time for last 7 days data
    this.BeforeSevenDayTime = moment().subtract(7, 'day').startOf('day');
    this.TodayTime = moment().endOf('day');

    this.barChartLabels = this._HelperService.CalculateLastSevenDay(this.TodayStartTime);

    this.Alert_Terminal_Load();
    this.GetAccountOverviewLite();
    this.GetLastSevenData();

  }

  //#region 7daychartconfig 

  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }

  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' }
  ];


  //#endregion

  //#region last7day 

  public BeforeSevenDayTime = null;
  public TodayTime = null;

  public _LastSevenDaysData: any = {};

  GetLastSevenData() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getterminalactivityhistory',
      StartTime: this.BeforeSevenDayTime,
      EndTime: this.TodayTime,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LastSevenDaysData = _Response.Result;


          for (let i = 0; i < this._LastSevenDaysData.length; i++) {
            // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;
            this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Active;
            this.barChartData[1].data[i] = this._LastSevenDaysData[i].Data.Idle;
            this.barChartData[2].data[i] = this._LastSevenDaysData[i].Data.Dead;
            this.barChartData[3].data[i] = this._LastSevenDaysData[i].Data.Inactive;

          }

          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region dougnutconfig 

  public pieChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: false,
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  };

  // public doughnutcard = [{ backgroundColor: ['#FFC20A', '#00cccc', '#f10075', '#0168fa'] }];
  
  public doughnutcard = [{ backgroundColor: ['#f10075', '#FFC20A', '#00cccc', '#0168fa'] }];
  public datapieCard = {
    labels: ['Verve', 'Master Card', 'Visa', 'Other Cards'],
    datasets: [{
      data: [],
    }],

  };

  public doughnuttype = [{ backgroundColor: ['#00cccc', '#f10075'] }];
  public datapieType = {
    labels: ['Cash', 'Card',],
    datasets: [{
      data: [0, 0],
      backgroundColor: ['#66a4fb', '#4cebb5']
    }]
  };

  //#endregion

  //#region accountoverview 

  TodayStartTime = null;
  TodayEndTime = null;



  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverview',
      StartTime: this.TodayStartTime,
      EndTime: this.TodayEndTime,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          // console.log(this._AccountOverview);

          //#region calculatetype 

          this._AccountOverview['CardTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this._AccountOverview['CashTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;

          this.datapieType.datasets[0].data[1] = this._AccountOverview['CardTransactions'];
          this.datapieType.datasets[0].data[0] = this._AccountOverview['CashTransactions'];

          //#endregion

          //#region calculatetermperc 

          if (this._AccountOverview.TerminalStatus['Total'] != 0) {
            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }

          //#endregion

          //#region calculateothers 

          var other: any = {
            Name: "Other",
            Transactions: 0,
            Amount: 0.0,
            TransactionPerc: 0.0
          }


          for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
            let element = this._AccountOverview['CardTypeSale'][index];

            other.Transactions += element['Transactions'];
            other.Amount += element['Amount'];

            this.datapieCard.datasets[0].data.push(element['Transactions']);
            //#region CardTypePerc 

            this._AccountOverview['CardTypeSale'][index].TransactionPer = this._HelperService.
              DivideTwoNumbers(element.Transactions, this._AccountOverview["TotalTransactions"]) * 100;
            this._AccountOverview['CardTypeSale'][index].AmountPer = this._HelperService.
              DivideTwoNumbers(element.Amount, this._AccountOverview["TotalSale"]) * 100;

            //#endregion
          }

          other.Transactions = this._AccountOverview["TotalTransactions"] - other['Transactions'];
          this.datapieCard.datasets[0].data.push(other.Transactions);

          other.Amount = this._AccountOverview["TotalSale"] - other['Amount'];

          other.TransactionPer = this._HelperService.
            DivideTwoNumbers(other['Transactions'], this._AccountOverview["TotalTransactions"]) * 100;
          other.AmountPer = this._HelperService.
            DivideTwoNumbers(other['Amount'], this._AccountOverview["TotalSale"]) * 100;

          this._AccountOverview['Others'] = other;

          //#endregion
          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              // console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //click event handler for Card Dougnout and TypeDougnut
  onChartClick(event: any): void {

  }

  //#endregion

  //#region terminal 

  public TerminalsList_Config: OList;

  Alert_Terminal_Load() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: {
        SortDefaultName: null,
        SortDefaultColumn: 'LastTransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: " Terminal List",
      StatusType: "default",
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      Type: this._HelperService.AppConfig.ListType.All,
      //SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.blocked', '='),
      DefaultSortExpression: 'LastTransactionDate desc',
      PageRecordLimit: this._HelperService.AppConfig.ListRecordLimit[5],

      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'ApplicationStatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config,
    );
    // console.log(this.TerminalsList_Config);
    this.TerminalsList_GetData();
  }

  TerminalsList_ToggleOption(event: any, Type: any) {
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }
  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTerm(
      this.TerminalsList_Config, undefined, true
    );
    this.TerminalsList_Config = TConfig;
  }

  public _AccountOverview: OAccountOverview =
    {
      CardTypeSale: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      Idle: 0,
      Dead: 0,
      Active: 0,
      Inactive: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      TerminalStatus: 0,
      DeadTerminals: 0,
      Total: 0,
      TotalMerchants: 0,
      TotalTerminals: 0,
      ActiveStores: 0,
      TotalStores: 0,
      Ptsp: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      CashTransactionAmount: 0,
      CashTransactionsPerc: 0,
      CashTransactions: 0,
      CardTransactionsAmount: 0,
      CardTransactionsPerc: 0,
      CardTransactions: 0,
      Others: {}
    }

  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }
  //#endregion

}

export class OAccountOverview {
  public TotalMerchants: any;
  public TotalTerminals: any;
  public CardTypeSale: any;
  public ActiveStores: any;
  public TotalStores: any;
  public Ptsp: any;
  public TotalSale: any;
  public TotalTransactions: any;
  public CashTransactionAmount: any;
  public CashTransactionsPerc: any;
  public CashTransactions: any;
  public CardTransactionsAmount: any;
  public CardTransactionsPerc: any;
  public CardTransactions: any;
  public Others: any;
  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}