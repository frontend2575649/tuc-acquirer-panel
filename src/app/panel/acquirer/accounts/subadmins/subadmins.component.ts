import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon, FilterHelperService
} from "../../../../service/service";
import swal from "sweetalert2";
;

declare var $: any;
@Component({
  selector: 'app-subadmins',
  templateUrl: './subadmins.component.html',
})
export class SubadminsComponent implements OnInit {
  // _imageConfig =
  //   {

  //     Images: [],
  //     profileImages: [],
  //     StartDateConfig: {
  //     },
  //     EndDateConfig: {
  //     },
  //     DefaultStartDate: null,
  //     DefaultEndDate: null,
  //     StartDate: null,
  //     EndDate: null,
  //   }
  // private InputFileComponent_Term: InputFileComponent;
  // _ImageManager =
  //   {
  //     TCroppedImage: null,
  //     ActiveImage: null,
  //     ActiveImageName: null,
  //     ActiveImageSize: null,
  //     Option: {
  //       MaintainAspectRatio: "true",
  //       MinimumWidth: 800,
  //       MinimumHeight: 400,
  //       MaximumWidth: 800,
  //       MaximumHeight: 400,
  //       ResizeToWidth: 800,
  //       ResizeToHeight: 400,
  //       Format: "jpg",
  //     }
  //   }

  public ResetFilterControls: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }
  ngOnInit() {
    Feather.replace();
    this.SubAccountList_Setup();
    this.SubAccountList_Filter_Owners_Load();
    this.Form_AddUser_Load();
  }

  Form_AddUser_Open() {
    this._HelperService.OpenModal('Form_AddUser_Content');
  }

  // SetOtherFilters(): void {
  //   this.SubAccountList_Config.SearchBaseConditions = [];
  //   this.SubAccountList_Config.SearchBaseCondition = null;
  // }


  public SubAccountList_Config: OList;
  SubAccountList_Setup() {
    this.SubAccountList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
      Title: "SubAccounts",
      StatusType: "default",
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      //DefaultSortExpression: 'CreateDate desc',
      IsDownload: true,
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'Name',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Mobile Number',
          SystemName: 'MobileNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          DefaultValue: 'ThankUCash',
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: 'Email Address',
          SystemName: 'EmailAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        // {
        //   DisplayName: 'Role Name',
        //   SystemName: 'RoleName',
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: 'text-right',
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        //   NavigateField: "ReferenceKey",
        //   NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        // },
        {
          DisplayName: 'Terminals',
          SystemName: 'Terminals',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: 'text-right',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date text-right',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          IsDateSearchField: true,
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Dashboard

          //     DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          // SystemName: "CreateDate",
          // DataType: this._HelperService.AppConfig.DataType.Date,
          // Class: "td-date text-right",
          // Show: true,
          // Search: false,
          // Sort: true,
          // ResourceId: null,
          // IsDateSearchField:true,
        },
      ]
    };
    this.SubAccountList_Config = this._DataHelperService.List_Initialize(
      this.SubAccountList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.SubAccountList_Config
    );
    this.SubAccountList_GetData();
  }
  SubAccountList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.SubAccountList_Config.Sort.SortOptions.length; index++) {
        const element = this.SubAccountList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event, Type, this.SubAccountList_Config
    );

    this.SubAccountList_Config = this._DataHelperService.List_Operations(
      this.SubAccountList_Config, event, Type
    );

    if ((this.SubAccountList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
      this.SubAccountList_GetData();
    }

  }
  SubAccountList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.SubAccountList_Config
    );
    this.SubAccountList_Config = TConfig;
  }
  SubAccountList_RowSelected(ReferenceData) {

    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.AccountDetails.SubAdminDetails, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
  }


  public SubAccountList_Filter_Owners_Option: Select2Options;
  public SubAccountList_Filter_Owners_Selected = 0;
  SubAccountList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant,
        this._HelperService.AppConfig.AccountType.Acquirer,
        this._HelperService.AppConfig.AccountType.PGAccount,
        this._HelperService.AppConfig.AccountType.PosAccount
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.SubAccountList_Filter_Owners_Option = {
      placeholder: 'Sort by Referrer',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  SubAccountList_Filter_Owners_Change(event: any) {
    if (event.value == this.SubAccountList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '=');
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SubAccountList_Config.SearchBaseConditions);
      this.SubAccountList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.SubAccountList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '=');
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.SubAccountList_Config.SearchBaseConditions);
      this.SubAccountList_Filter_Owners_Selected = event.value;
      this.SubAccountList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.SubAccountList_Filter_Owners_Selected, '='));
    }
    this.SubAccountList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveSubAccount,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      RoleId: [7, Validators.required],
      RoleKey: ['admin', Validators.required],

      Name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      StatusCode: this._HelperService.AppConfig.Status.Active,

    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    // _FormValue.DisplayName = _FormValue.FirstName;
    // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this.SubAccountList_Config = _Response.Result;
          // if (this.SubAccountList_Config != undefined && this.SubAccountList_Config.MobileNumber != undefined && this.SubAccountList_Config.MobileNumber != null) {
          //   if (this.SubAccountList_Config.MobileNumber.startsWith("234")) {
          //     // tslint:disable-next-line:max-line-length
          //     this.SubAccountList_Config.MobileNumber = this.SubAccountList_Config.MobileNumber.slice(3, this.SubAccountList_Config.length);
          //   }
          // }

          this._HelperService.NotifySuccess("Account created successfully");
          this.Form_AddUser_Clear();
          this._HelperService.CloseModal('Form_AddUser_Content');
          this.SubAccountList_Setup();

          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }



  //#region OwnerFilter

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = null;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.MerchantsList_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.SubAccountList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );
    this.OwnerEventProcessing(event);
  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict("", "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text, this.MerchantsList_Filter_Owners_Selected, "="
      );
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase, this.SubAccountList_Config.SearchBaseConditions);
      this.MerchantsList_Filter_Owners_Selected = null;
    } else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict("", "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text, this.MerchantsList_Filter_Owners_Selected, "="
      );
      this.SubAccountList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase, this.SubAccountList_Config.SearchBaseConditions
      );
      this.MerchantsList_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.SubAccountList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict("", "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text, this.MerchantsList_Filter_Owners_Selected, "="
        )
      );
    }
    this.SubAccountList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.SubAccountList_Config.SearchBaseConditions = [];
    this.SubAccountList_Config.SearchBaseCondition = null;
    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }


  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
    this.SetOtherFilters();
    this.SubAccountList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    console.log(Type, index);
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
    this.SetOtherFilters();
    this.SubAccountList_GetData();
  }



  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();
        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }



  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();
        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Merchant
        );
        this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
        this.SubAccountList_GetData();
        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.SubAccountList_GetData();
    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.SubAccountList_Config);
    this.SetOtherFilters();
    this.SubAccountList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.MerchantsList_Filter_Owners_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  // *************************************
  // image add
  // Form_Promote_Show() {
  //   this._HelperService.Icon_Crop_Clear();
  //   this.InitImagePicker(this.InputFileComponent_Term);
  //   this._HelperService.OpenModal("_Icon_Cropper_Modal");
  //   // this._HelperService.OpenModal("_PreviewGeneral");



  // }

  // CurrentImagesCount: number = 0;
  // private InitImagePicker(InputFileComponent: InputFileComponent) {
  //   if (InputFileComponent != undefined) {
  //     this.CurrentImagesCount = 0;
  //     this._HelperService._InputFileComponent = InputFileComponent;
  //     InputFileComponent.onChange = (files: Array<InputFile>): void => {
  //       if (files.length >= this.CurrentImagesCount) {
  //         this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
  //       }
  //       this.CurrentImagesCount = files.length;
  //     };
  //   }
  // }
  // Form_ManagePromote: FormGroup;

  // onImageAccept(value) {
  //   setTimeout(() => {
  //     this.Form_AddUser.patchValue(
  //       {
  //         TImage: null,
  //       }
  //     );
  //   }, 300);
  //   this._ImageManager.ActiveImage = value;
  //   this._ImageManager.ActiveImageName = value.file.name;
  //   this._ImageManager.ActiveImageName = value.file.size;
  // }
  // Icon_B64Cropped(base64: string) {
  //   this._ImageManager.TCroppedImage = base64;
  // }
  // Icon_B64CroppedDone() {
  //   var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
  //   var ImageItem =
  //   {
  //     OriginalContent: this._ImageManager.TCroppedImage,
  //     Name: this._ImageManager.ActiveImageName,
  //     Size: this._ImageManager.ActiveImageSize,
  //     Extension: ImageDetails.Extension,
  //     Content: ImageDetails.Content
  //   };
  //   if (this._imageConfig.profileImages.length == 0) {
  //     this._imageConfig.profileImages.push(
  //       {
  //         ImageContent: ImageItem,
  //         IsDefault: 1,
  //       }
  //     );
  //   }
  //   else {
  //     this._imageConfig.profileImages.push(
  //       {
  //         ImageContent: ImageItem,
  //         IsDefault: 0,
  //       }
  //     );
  //   }
  //   this._ImageManager.TCroppedImage = null;
  //   this._ImageManager.ActiveImage = null;
  //   this._ImageManager.ActiveImageName = null;
  //   this._ImageManager.ActiveImageSize = null;
  // }
  // Icon_Crop_Clear() {
  //   this._ImageManager.TCroppedImage = null;
  //   this._ImageManager.ActiveImage = null;
  //   this._ImageManager.ActiveImageName = null;
  //   this._ImageManager.ActiveImageSize = null;
  //   this._HelperService.CloseModal('_Icon_Cropper_Modal');
  // }
  // RemoveImage(Item) {
  //   this._imageConfig.profileImages = this._imageConfig.profileImages.filter(x => x != Item);
  // }


}