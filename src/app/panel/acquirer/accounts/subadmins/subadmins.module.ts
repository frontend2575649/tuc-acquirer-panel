import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SubadminsComponent } from './subadmins.component';
import { MainPipe } from '../../../../service/main-pipe.module'

const routes: Routes = [
  { path: '', component: SubadminsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdminsRoutingModule { }


@NgModule({
  declarations: [SubadminsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    SubAdminsRoutingModule,
    ImageCropperModule,
    MainPipe
  ]
})
export class SubadminsModule { }
