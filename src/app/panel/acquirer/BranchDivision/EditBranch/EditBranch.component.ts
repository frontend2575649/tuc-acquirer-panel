import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";;
import swal from 'sweetalert2';
declare var $: any;

import { HCoreXAddress, HCXAddressConfig, locationType } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';


@Component({
  selector: 'app-EditBranch',
  templateUrl: './EditBranch.component.html',
})
export class EditBranchComponent implements OnInit {

  public _Address: HCoreXAddress = {};
  public _isAddressLoaded = false;


  AddressChange(Address) {
    this._Address = Address;
  }
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }


  public isActive: boolean = false;
  _DateDiff: any = {};
  public _BranchDetails: any =
    {
      ManagerName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      RoleId: null,
      RoleKey: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
      // 
      Address: null,
      AddressComponent: this._Address,
      // 
    }
  _CurrentAddress: any = {};
  _Currentcity: any = {};
  _SubRegions: any[] = [];
  select2: boolean = true;
  cityname: any;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {

  }

  ngOnInit() {



    // this.S2_City_Load();
    // this.AssignManger_List();
    // this.S2_Region_Load()
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this.Form_EditUser_Load();
        this.MForm_EditUser_Load();
        this.GetMangers_List();

        this.GetBranchDetails();
      }
    });
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  MForm_EditUser: FormGroup;

  MForm_EditUser_Load() {
    this.MForm_EditUser = this._FormBuilder.group({
      ReferenceId: [null,],
      ReferenceKey: [null,],
      // OwnerId: [null],
      // OwnerKey: [null],
      // RoleId: [this._BranchDetails.RoleId],
      // RoleKey: [this._BranchDetails.RoleKey],
      // Name: ["managerone", Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      // MobileNumber: [95548374938, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
      // EmailAddress: ["managerone@email.com", Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])]
    });
  }
  MMForm_EditUser_Clear() {
    this.MForm_EditUser.reset();
    this.MForm_EditUser_Load();
  }
  Form_EditUser: FormGroup;
  Form_EditUser_Address: string = null;
  Form_EditUser_CityName: string = null;
  Form_EditUser_city: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Address'].setValue(address.formatted_address);

    //  this.Form_EditUser_CityName = address.formatted_cityname;
    //  this.Form_EditUser.controls['CityName'].setValue(address.formatted_cityname);

    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    this.Form_EditUser_city = this._CurrentAddress.administrative_area_level_2;
    this._BranchDetails.CityName = this._CurrentAddress.administrative_area_level_2;
    // console.log(this._CurrentAddress.administrative_area_level_2)
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);

    this.select2 = false;
    this._ChangeDetectorRef.detectChanges();

    if (this._CurrentAddress.sublocality_level_1) {
      this._SubRegions.push({
        text: this._CurrentAddress.sublocality_level_1,
        id: 0
      });

      if (this._CurrentAddress.sublocality_level_2) {
        this._SubRegions.push({
          text: this._CurrentAddress.sublocality_level_2,
          id: 1
        });

        if (this._CurrentAddress.sublocality_level_3) {
          this._SubRegions.push({
            text: this._CurrentAddress.sublocality_level_3,
            id: 2
          });

          if (this._CurrentAddress.sublocality_level_4) {
            this._SubRegions.push({
              text: this._CurrentAddress.sublocality_level_4,
              id: 3
            });
          }
        }
      }
    }


  }
  Form_EditUser_Show() {
    // this._HelperService.OpenModal('Form_EditUser_Content');
  }
  Form_EditUser_Close() {

    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ReferredMerchants]);
    // this._HelperService.OpenModal('Form_EditUser_Content');
  }
  Form_EditUser_Load() {

    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.UpdateBranch,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      PhoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      // Manager: [null],
    });
  }
  Form_EditUser_Clear() {
    this.Form_EditUser_Latitude = 0;
    this.Form_EditUser_Longitude = 0;
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    // if(_FormValue.Maneger.ReferenceKey && _FormValue.Maneger.ReferenceId == null){
     
      
    //   this.MForm_EditUser.patchValue(
    //     {
    //       ReferenceKey: this._BranchDetails.ManagerReferenceKey,
    //       ReferenceId: this._BranchDetails.ReferenceId
    //     })
    // }


    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError('Please select business location');
    }
    else {

      swal({
        position: 'top',
        title: 'Edit branch account ?',
        text: 'Please verify information and click on continue button to edit branch ',
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        // confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      }).then((result) => {
        if (result.value) {
          // _FormValue.DisplayName = _FormValue.FirstName;
          // _FormValue.Name = _FormValue.FirstName + ' ' + _FormValue.LastName;
          _FormValue.Longitude = this.Form_EditUser_Longitude
          _FormValue.Latitude = this.Form_EditUser_Latitude
          _FormValue.Manager = this.MForm_EditUser.value;
          _FormValue.Address = this._Address,
          _FormValue.AddressComponent = this._Address,
          this._HelperService.IsFormProcessing = true;
          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, _FormValue);
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {

                this._HelperService.NotifySuccess(_Response.Message);
                this.Form_EditUser_Clear();
                this.Form_EditUser_Load();
                this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Accounts.Branch]);

                if (_FormValue.OperationType == 'edit') {
                  this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.MerchantManager, _Response.Result.ReferenceKey]);
                  // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Merchant.Dashboard, _Response.Result.ReferenceKey]);
                }
                else if (_FormValue.OperationType == 'close') {
                  this.Form_EditUser_Close();
                }
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            });
        }
      });


    }
  }

  //City DropDown
  public S2_City_Option: Select2Options;
  public City_Selected = 0;
  S2_City_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getcitybyregionarea,
      Location: this._HelperService.AppConfig.NetworkLocation.V1.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.S2_City_Option = {
      placeholder: 'Select City ',
      ajax: _Transport,
      multiple: false,
      allowClear: false,
    };
  }
  S2_City_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        CityKey: event.value
      }
    );
  }
  mapaddress: any = {};


  ManagerNameFetch:any;

  GetBranchDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetBranch,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._BranchDetails = _Response.Result;
          // console.log(this._BranchDetails);
          this.ManagerNameFetch=this._BranchDetails.ManagerName;
          this._isAddressLoaded = true;
          this._Address = this._BranchDetails.AddressComponent;
          this.GetMangers_Option.placeholder = this._BranchDetails.ManagerName;
          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this.Form_EditUser_Latitude = _Response.Result.Latitude;
            this.Form_EditUser_Longitude = _Response.Result.Longitude;

            this.Form_EditUser.controls['Latitude'].setValue(_Response.Result.Latitude);
            this.Form_EditUser.controls['Longitude'].setValue(_Response.Result.Longitude);
          }

          this.MForm_EditUser.controls['ReferenceKey'].setValue(_Response.Result.ManagerKey);
          this.MForm_EditUser.controls['ReferenceId'].setValue(_Response.Result.ManagerId);

          this._DateDiff = this._HelperService._DateRangeDifference(this._BranchDetails.StartDate, this._BranchDetails.EndDate);
          this._BranchDetails.StartDateS = this._HelperService.GetDateS(
            this._BranchDetails.StartDate
          );
          this._BranchDetails.EndDateS = this._HelperService.GetDateS(
            this._BranchDetails.EndDate
          );
          this._BranchDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._BranchDetails.CreateDate
          );
          this._BranchDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._BranchDetails.ModifyDate
          );
          this._BranchDetails.StatusI = this._HelperService.GetStatusIcon(
            this._BranchDetails.StatusCode
          );
          this._BranchDetails.StatusB = this._HelperService.GetStatusBadge(
            this._BranchDetails.StatusCode
          );
          this._BranchDetails.StatusC = this._HelperService.GetStatusColor(
            this._BranchDetails.StatusCode
          );



        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //City DropDown
  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;
  GetRoles_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreParameters,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: '',
      SortCondition: ['Name asc'],
      Fields: [
        {
          SystemName: 'ReferenceKey',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: 'Name',
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ],
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'TypeCode', this._HelperService.AppConfig.DataType.Text, 'hcore.role', '=');
    this.GetRoles_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRoles_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRoles_Transport,
      multiple: false,
    };
  }

  GetRoles_ListChange(event: any) {

    this.MForm_EditUser.controls['OwnerId'].setValue(null);
    this.MForm_EditUser.controls['OwnerKey'].setValue(null);

    this.MForm_EditUser.patchValue(
      {
        RoleId: event.value,
        RoleKey: event.data[0].apival
      }
    );

    this._ShowRole = false;
    this._ChangeDetectorRef.detectChanges();

    if (event.value == 8) { //RM
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 6; //Manager

      this.AssignManger_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();
    } else if (event.value == 7) { //SubAccount

      this._ShowReportingM = false;

      this.MForm_EditUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
      this.MForm_EditUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);

      this._MangerTypeId = undefined;
    }
    else if (event.value == 6) { //Manager
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 7; //Subaccount
      this.AssignManger_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();

    } else {
      this._MangerTypeId = undefined;
      this._ShowReportingM = true;
      this.AssignManger_List();
    }

    this._ShowRole = true;
    this._ChangeDetectorRef.detectChanges();

    this.AssignManger_List();
  }

  public _MangerTypeId: number;
  public _ShowRole: boolean = true;
  public _ShowReportingM: boolean = true;



  public AssignManger_Option: Select2Options;
  public AssignManger_Transport: any;
  AssignManger_List() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect;
    _Select =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
      ]
    }

    this.AssignManger_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.AssignManger_Option = {
      placeholder: PlaceHolder,
      ajax: this.AssignManger_Transport,
      multiple: false,
    };
  }
  AssignManger_ListChange(event: any) {
    this.MForm_EditUser.patchValue(
      {
        OwnerId: event.data[0].ReferenceId,
        OwnerKey: event.data[0].ReferenceKey

      }
    );

  }

  //Region Drop Down

  public S2_Region_Option: Select2Options;
  public Region_Selected = 0;
  S2_Region_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getregionsbycountry,
      Location: this._HelperService.AppConfig.NetworkLocation.V1.acquirer,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },

      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.S2_Region_Option = {
      placeholder: 'Select Region ',
      ajax: _Transport,
      multiple: false,
      allowClear: false,
    };
  }
  S2_Region_Change(event: any) {
    this.Form_EditUser.patchValue(
      {
        RegionKey: event.value
      }
    );
  }



  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '6', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {
    // alert(event);
    this.MForm_EditUser.patchValue(
      {
        ReferenceKey: event.data[0].ReferenceKey,
        ReferenceId: event.data[0].ReferenceId
      }
    );
  }
  Form_EditUser_Block() {

  }

}
