import { Component, OnInit, Renderer2 } from '@angular/core';
import { HelperService, OResponse, OOverview, DataHelperService, OList, OSelect } from '../../service/service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
declare var moment: any;
declare var $: any;
declare var PerfectScrollbar: any;
import * as Feather from 'feather-icons';
import * as Chart from "../../../../node_modules/chart.js/dist/Chart.js";
import * as Cookies from '../../../assets/js/js.cookie.js'

@Component({
    selector: 'acquirer-root',
    templateUrl: './acquirer.component.html',
})
export class TUAcquirerComponent implements OnInit {

    public _Store_Option: Select2Options;
    public isGroupLoyalty:boolean = false
    constructor(
        public _DataHelperService: DataHelperService,
        public _Router: Router,
        public _HelperService: HelperService,
        private renderer2: Renderer2
    ) {
    }

    ngAfterViewInit(): void {
    }

    ngOnInit() {

        this.isGroupLoyalty = this._HelperService.GetStorage("hca").UserAccount.IsGroupLoyalty

        this._HelperService.darkStyle = Cookies.get("mapColor");

        // Chart.defaults.global.defaultFontColor = 'white';
        // Chart.defaults.global.defaultFontSize = '16';

        $("li").click(() => {

            var backdrop: HTMLElement = document.getElementById("backdrop");
            backdrop.classList.remove("show");

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                this._HelperService.CloseModal(element.id);
            }

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal-backdrop");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                element.classList.remove("modal-backdrop");
            }

        });

        $('body').on('click', '.df-mode', (e) => {
            e.preventDefault();

            for (let index = 0; index < $('.df-mode').length; index++) {
                const element = $('.df-mode')[index];
                setTimeout(() => {
                    if (element.classList.length == 3) {
                        if (element.getAttribute('data-title') === 'dark') {
                            this._HelperService.CheckMode = 'dark';
                            this._HelperService.darkStyle = [
                                {
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "administrative.locality",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#263c3f"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#6b9a76"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#38414e"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#212a37"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#9ca5b3"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#1f2835"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#f3d19c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#2f3948"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit.station",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#515c6d"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                }
                            ];

                            // console.log("i am here");
                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        } else {
                            this._HelperService.CheckMode = 'light';
                            this._HelperService.darkStyle = [];
                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        }
                    }
                }, 50);
            }
        });

        setTimeout(() => {
            var hasMode = Cookies.get('df-mode');

            this._HelperService.CheckMode = (hasMode === 'dark') ? 'dark' : 'light';
            if (hasMode == 'dark') {
                this._HelperService.darkStyle = [
                    {
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "administrative.locality",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#263c3f"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#6b9a76"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#38414e"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#212a37"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#9ca5b3"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#1f2835"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#f3d19c"
                            }
                        ]
                    },
                    {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#2f3948"
                            }
                        ]
                    },
                    {
                        featureType: "transit.station",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#515c6d"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    }
                ];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
            else {
                this._HelperService.darkStyle = [];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
        }, 50);
        const s = this.renderer2.createElement('script');
        s.type = 'text/javascript';
        s.src = '../../../assets/js/dashforge.aside.js';
        s.text = ``;
        this.renderer2.appendChild(document.body, s);

        const s2 = this.renderer2.createElement('script');
        s2.type = 'text/javascript';
        s2.src = '../../../../assets/js/dashforge.settings.js';
        s2.text = ``;
        this.renderer2.appendChild(document.body, s2);
        Feather.replace();

    }


    ProcessLogout() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.LogoutTitle,
            text: "Click on Logout button to confirm",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Logout,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Logout
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        // console.log(_Error);
                    });
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
            }
            else { }
        });



    }
}