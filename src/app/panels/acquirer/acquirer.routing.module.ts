import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TUAcquirerComponent } from "./acquirer.component";
import { HelperService } from "../../service/helper.service";
const routes: Routes = [
  {
    path: "acquirer",
    component: TUAcquirerComponent,
    canActivateChild: [HelperService],
    children: [
      { path: 'apps', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/hcapps/tuapplist/tuapplist.module#TUAppListModule' },
      { path: 'app', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/hcapps/hcapps.module#HCAppsModule' },
      //Branch ANd Division
      { path: "branch", data: { permission: "terminals", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/accounts/branch/branch.module#BranchModule" },
      { path: "branchdetails", data: { permission: "terminals", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/accountdetails/BranchDetails/branchdetails.module#BranchDetailsModule" },
      { path: "branchdetails/editbranch/:referencekey/:referenceid", data: { permission: "Branch", PageName: "System.Menu.Branch" }, loadChildren: "../../panel/acquirer/BranchDivision/EditBranch/EditBranch.module#EditBranchModule" },



      //End 
      { path: "activity", data: { permission: "activity", PageName: "System.Menu.Activity" }, loadChildren: "./../../modules/dashboards/tuactivity/acquirer/tuactivity.module#TUActivityModule" },
      { path: "salesreport", data: { permission: "salesreport", PageName: "System.Menu.SaleReport" }, loadChildren: "./../../modules/dashboards/tusalesreport/acquirer/tusalesreport.module#TUSalesReportModule" },
      { path: "rmreports", data: { permission: "rmreport", PageName: "System.Menu.RMReport" }, loadChildren: "./../../modules/dashboards/turmreport/acquirer/turmreport.module#TURmReportModule" },
      { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/acquirer/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: "salesanalytics", data: { permission: "dashboard", PageName: "System.Menu.SalesAnalytics" }, loadChildren: "./../../panel/acquirer/analytics/salesanalytics/salesanalytics.module#TUSalesAnalyticsModule" },
      { path: "addcampaign", data: { permission: "terminals", PageName: "System.Menu.AddCampaign" }, loadChildren: "./../../panel/acquirer/campaigns/addcampaign/addcampaign.module#TUAddCampaignModule" },
      { path: "campaigns", data: { permission: "terminals", PageName: "System.Menu.Campaigns" }, loadChildren: "./../../panel/acquirer/campaigns/campaigns/campaigns.module#TUCampaignsModule" },
      { path: "campaign/:referencekey", data: { permission: "terminals", PageName: "System.Menu.Campaign" }, loadChildren: "./../../panel/acquirer/campaigns/campaign/campaign.module#TUCampaignModule" },
      { path: "rm/:referencekey/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Rm" }, loadChildren: "./../../panel/acquirer/accountdetails/rm/rm.module#TURelationshipManagerModule" },
      { path: "rms", data: { permission: "terminals", PageName: "System.Menu.Rms" }, loadChildren: "./../../panel/acquirer/accounts/relationshipmanagers/relationshipmanagers.module#TURelationshipManagersModule" },
      { path: "addrm", data: { permission: "terminals", PageName: "System.Menu.AddRm" }, loadChildren: "./../../panel/acquirer/accountcreation/rm/tuaddrm.module#TUAddRmModule" },
      { path: "addbranch", data: { permission: "branch", PageName: "System.Menu.Branch" }, loadChildren: "./../../panel/acquirer/accountcreation/addbranch/addbranch.module#AddbranchModule" },
      { path: "merchants", data: { permission: "terminals", PageName: "System.Menu.Merchants" }, loadChildren: "./../../modules/accounts/tumerchants/acquirer/tumerchants.module#TUMerchantsModule" },
      { path: "bulkmerchants", data: { permission: "terminals", PageName: "System.Menu.Merchants" }, loadChildren: "./../../modules/accounts/tumerchants/bulkmerchant/tumerchants.module#TUBulkMerchantsModule" },
      { path: "stores", data: { permission: "stores", PageName: "System.Menu.Stores" }, loadChildren: "./../../modules/accounts/tustores/acquirer/tustores.module#TUStoresModule" },
      { path: "temp", data: { permission: "stores", PageName: "System.Menu.Stores" }, loadChildren: "./../../modules/accounts/temp/hclist.module#HCListModule" },
      { path: 'subaccounts', data: { 'permission': 'stores', PageName: 'System.Menu.Managers' }, loadChildren: './../../modules/accounts/tusubaccounts/acquirer/tusubaccounts.module#TUSubAccountsModule' },
      { path: 'targets', data: { 'permission': 'targets', PageName: 'System.Menu.Targets' }, loadChildren: './../../pages/dashboard/tutargets/tutargets.module#TUTargetsModule' },
      { path: 'saleshistory', data: { 'permission': 'merchants', PageName: 'System.Menu.SalesHistory' }, loadChildren: './../../modules/transactions/tusale/acquirer/tusale.module#TUSaleModule' },
      { path: 'terminal', data: { 'permission': 'customers', PageName: 'System.Menu.Terminal' }, loadChildren: './../../modules/accounts/tuterminal/tuterminal.module#TUTerminalModule' },
      { path: 'terminals', data: { 'permission': 'terminals', PageName: 'System.Menu.Terminals' }, loadChildren: './../../modules/accounts/tuterminals/acquirer/tuterminals.module#TUTerminalsModule' },
      { path: "customers", data: { permission: "customers", PageName: "System.Menu.Customers" }, loadChildren: "./../../pages/accounts/tuappusers/tuappusers.module#TUAppUsersListModule" },
      { path: "live/terminals", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../modules/dashboards/tuliveterminals/acquirer/tuliveterminals.module#TULiveTerminalsModule" },
      { path: "live/merchants", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../modules/dashboards/tulivemerchants/acquirer/tulivemerchants.module#TULiveMerchantsModule" },
      { path: "merchant", data: { permission: "merchant", PageName: "System.Menu.Merchant" }, loadChildren: "./../../modules/accountdetails/tumerchant/acquirer/tumerchant.module#TUMerchantModule" },
      { path: "terminal", data: { permission: "terminal", PageName: "System.Menu.Terminal" }, loadChildren: "./../../modules/accountdetails/tuterminal/acquirer/tuterminal.module#TUTerminalModule" },
      { path: "store", data: { permission: "terminal", PageName: "System.Menu.Store" }, loadChildren: "./../../modules/accountdetails/tustore/acquirer/tustore.module#TUStoreModule" },
      { path: "manager", data: { permission: "manager", PageName: "System.Menu.Manager" }, loadChildren: "./../../modules/accountdetails/tumanager/acquirer/tumanager.module#TUManagerModule" },
      { path: "manageralternative", data: { permission: "manager", PageName: "System.Menu.Manager" }, loadChildren: "./../../modules/accountdetails/tumanager/acquireralternate/tumanageralternate.module#TUManagerAlternateModule" },
      { path: "campaign", data: { permission: "campaign", PageName: "System.Menu.Campaign" }, loadChildren: "./../../modules/accountdetails/tucampaign/acquirer/tucampaign.module#TUCampaignModule" },
      { path: 'bankloyalty', data: { 'permission': 'acquirer', PageName: 'System.Menu.Merchant' }, loadChildren: '../../panel/acquirer/loyaltyprogram/tubankloyalty/tubankloyalty.module#TubankloyaltyModule' },


// Bulk 

      { path: "customers/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/bulkcustomer/customerupload.module#TUCustomerUploadModule" },
      { path: "allcustomers", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tucustomers/tucustomers.module#TUCustomersModule" },


      // FAQs
      { path: 'faqcategories', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tufaqs/tufaqs.module#TUFAQsModule' },

      {
        path: "referredmerchants",
        data: { permission: "terminals", PageName: "System.Menu.Merchants" },
        loadChildren:
          "./../../pages/accounts/tuacquirermerchants/tuacquirermerchants.module#TUAcquirerMerchantsModule"
      },
      {
        path: "subadmindetails/:referencekey/:referenceid",
        data: { permission: "admin", PageName: "System.Menu.SubAdmin" },
        loadChildren:
          "./../../panel/acquirer/accountdetails/subadmindetails/subadmindetails.module#SubadmindetailsModule"
      },
      {
        path: "subadmins",
        data: { permission: "admin", PageName: "System.Menu.SubAccounts" },
        loadChildren:
          "./../../panel/acquirer/accounts/subadmins/subadmins.module#SubadminsModule"
      },
      { path: "profile", data: { permission: "customers", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcprofile/hcprofile.module#HCProfileModule" },
      {
        path: "terminallive",
        data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
        loadChildren:
          "./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module#TUMerchantTerminalsLiveModule"
      },
      {
        path: "terminalssale",
        data: {
          permission: "merchants",
          PageName: "System.Menu.CommissionHistory"
        },
        loadChildren:
          "./../../pages/transactions/tuterminalssale/tuterminalssale.module#TUTerminalsSaleModule"
      },
      {
        path: "customerupload",
        data: { permission: "stores", PageName: "System.Menu.CustomerManager" },
        loadChildren:
          "./../../pages/useronboarding/customerupload/customerupload.module#TUCustomerUploadModule"
      },
      {
        path: "customeronboarding",
        data: { permission: "stores", PageName: "System.Menu.CustomerManager" },
        loadChildren:
          "./../../pages/useronboarding/customeronboarding/customeronboarding.module#TUCustomerOnboardingModule"
      },
      {
        path: "merchantupload",
        data: { permission: "stores", PageName: "System.Menu.BulkUplaod" },
        loadChildren:
          "./../../pages/useronboarding/bulkmerchant/merchantupload.module#TUMerchantUploadModule"
      },
      {
        path: "bulkcustomerupload",
        data: { permission: "acquirer", PageName: "System.Menu.CustomerUpload" },
        loadChildren:
          "./../../pages/useronboarding/bulkcustomer/customerupload.module#TUCustomerUploadModule"
      },
      {
        path: "merchantupload",
        data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
        loadChildren:
          "./../../pages/useronboarding/merchantupload/merchantupload.module#TUMerchantUploadModule"
      },
      {
        path: "merchantonboarding",
        data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
        loadChildren:
          "./../../pages/useronboarding/merchantonboarding/merchantonboarding.module#TUMerchantOnboardingModule"
      },
      {
        path: "merchantmanager/:referencekey",
        data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
        loadChildren:
          "./../../pages/useronboarding/merchantmanager/merchantmanager.module#TUMerchantManagerModule"
      },
      {
        path: "addterminal",
        data: { permission: "stores", PageName: "System.Menu.MerchantManager" },
        loadChildren:
          "./../../pages/useronboarding/tuaddposterminal/tuaddposterminal.module#TUAddPosTerminalModule"
      }]
      
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUAcquirerRoutingModule { }
