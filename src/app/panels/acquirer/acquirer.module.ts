import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TUAcquirerComponent } from './acquirer.component';
import { TUAcquirerRoutingModule } from './acquirer.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        TUAcquirerComponent,
    ],
    imports: [
        TUAcquirerRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [TUAcquirerComponent]
})
export class TUAcquirerModule { }
