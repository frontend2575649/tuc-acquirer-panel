import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TURmComponent } from "./rm.component";
import { HelperService } from "../../service/helper.service";
const routes: Routes = [
    {
        path: "rm",
        component: TURmComponent,
        canActivateChild: [HelperService],
        children: [
            {
                path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
                loadChildren:
                    "./../../panel/rm/dashboards/root/dashboard.module#TUDashboardModule"
            },
            {
                path: "stores", data: { permission: "dashboard", PageName: "System.Menu.Stores" },
                loadChildren:
                    "./../../panel/rm/accounts/stores/stores.module#StoresModule"
            },
            {
                path: "terminals", data: { permission: "dashboard", PageName: "System.Menu.Terminals" },
                loadChildren:
                    "./../../panel/rm/accounts/terminals/terminals.module#TerminalsModule"
            },
            {
                path: "live/terminals", data: { permission: "dashboard", PageName: "System.Menu.Terminals" },
                loadChildren:
                    "./../../panel/rm/live/liveterminals/liveterminals.module#LiveTerminalsModule"
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TURmRoutingModule { }
