import { Component, OnInit } from '@angular/core';
import { HelperService, OResponse, OOverview, DataHelperService, OList } from '../../service/service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
declare var moment: any;

@Component({
    selector: 'rm-root',
    templateUrl: './rm.component.html',
})
export class TURmComponent implements OnInit {

    constructor(
        public _DataHelperService: DataHelperService,
        public _Router: Router,
        public _HelperService: HelperService
    ) {

    }
    ngOnInit() {
        if (this._HelperService.UserAccount == null) {
            window.location.href = "/account/login";
        }
        else {
            var ActiveAccountKey = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveAcquirer);
            if (ActiveAccountKey != undefined && ActiveAccountKey != null && ActiveAccountKey != '') {
                this._HelperService.AppConfig.ActiveOwnerKey = ActiveAccountKey.ReferenceKey;
                this._HelperService.AppConfig.ActiveOwnerDisplayName = ActiveAccountKey.DisplayName;
            }
        }
    }

    ProcessLogout() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Logout
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess(_Response.Message);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                // console.log(_Error);
            });
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
        this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
    }
}