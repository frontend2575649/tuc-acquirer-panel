import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from "ngx-pagination";
import { TUTargetsComponent } from "./tutargets.component";


const routes: Routes = [{ path: "", component: TUTargetsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUTargetsRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    Ng5SliderModule,
    TUTargetsRoutingModule,
  ],
  declarations: [TUTargetsComponent]
})
export class TUTargetsModule { }
