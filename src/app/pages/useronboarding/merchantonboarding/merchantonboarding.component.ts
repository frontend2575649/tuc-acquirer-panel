import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { HCoreXAddress } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';

import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from '../../../service/service';
import swal from 'sweetalert2';
// import steps from "../../../../assets/js/jquery.steps.min.js"
declare var $: any;
import * as Feather from 'feather-icons';

@Component({
    selector: 'hc-merchantonboarding',
    templateUrl: './merchantonboarding.component.html',
})
export class TUMerchantOnboardingComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef
    ) {
        this._ContactInfo = true;
        this._StoreInfoCopy = false;
    }



    public ShowStoreAddressLocator = false;
    public _Address: HCoreXAddress = {};
    public _AddressStore: HCoreXAddress = {};
    AddressChange(Address) {
        // debugger;
        this._Address = Address;
    }
    acquireDetails: any = {};
    programId:any

    ngOnInit() {

        $('#wizard1').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            enablePagination: false,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onFinished: (event, currentIndex) => {
                //this.Form_AddUser_Process();
            },
            labels: {
                next: "Save",
            }
        });
        Feather.replace();
        // this.TerminalsList_Filter_Providers_Load();
        this.Form_AddUser_Load();
        // this.F_AddStore_Load();
        // this.GetAcquirersList();
        // this.GetBranches_List();
        // this.GetMangers_List();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params['referencekey'];

        });
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();

        this.acquireDetails = this._HelperService.GetStorage('hca').UserAccount.ProgramDetails;
        if (this.acquireDetails) {
            this.programId = this.acquireDetails.ProgramId;
        }
    }

    TerminalId: string;
    public _ContactInfo: boolean;
    public _StoreInfoCopy: boolean;
    public GetAcquirersOption: Select2Options;
    public MerchantSaveRequest: any;

    GetAcquirersList() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccounts,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: '',
            SortCondition: [],
            Fields: [
                {
                    SystemName: 'ReferenceKey',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: 'DisplayName',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true,
                },
                {
                    SystemName: 'AccountTypeCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.AccountType.Acquirer,
                },
                {
                    SystemName: 'StatusCode',
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: '=',
                    SearchValue: this._HelperService.AppConfig.Status.Active,
                }
            ],
        }
        // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetAcquirersOption = {
            placeholder: PlaceHolder,
            ajax: _Transport,
            multiple: false,
        };
    }
    GetAcquirersListChange(event: any) {
        this.Form_AddUser.patchValue(
            {
                OwnerKey: event.value
            }
        );
    }

    _SavedMerchant: any = {};
    Form_AddUser: FormGroup;
    Form_AddUser_Address: string = null;
    Form_AddUser_Latitude: number = 0;
    Form_AddUser_Longitude: number = 0;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_AddUser_PlaceMarkerClick(event) {
        this.Form_AddUser_Latitude = event.coords.lat;
        this.Form_AddUser_Longitude = event.coords.lng;
    }
    public Form_AddUser_AddressChange(address: Address) {
        this.Form_AddUser_Latitude = address.geometry.location.lat();
        this.Form_AddUser_Longitude = address.geometry.location.lng();
        this.Form_AddUser_Address = address.formatted_address;
        this.Form_AddUser.controls['Latitude'].setValue(this.Form_AddUser_Latitude);
        this.Form_AddUser.controls['Longitude'].setValue(this.Form_AddUser_Longitude);
        this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

        if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.reset();
        }
        else {
            this.Form_AddUser.controls['Address'].setValue(address.formatted_address);
            this.Form_AddUser.controls['MapAddress'].setValue(address.formatted_address);
        }
    }

    reset() {
        this.Form_AddUser.controls['Address'].reset()
        this.Form_AddUser.controls['CityName'].reset()
        this.Form_AddUser.controls['StateName'].reset()
        this.Form_AddUser.controls['CountryName'].reset()
    }

    Form_AddUser_Show() {
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Close() {

        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ReferredMerchants]);
        // this._HelperService.OpenModal('Form_AddUser_Content');
    }
    Form_AddUser_Load() {


        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveMerchant,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            // BranchId: [null, Validators.required],
            // BranchKey: [null, Validators.required],
            // RmId: [null, Validators.required],
            // RmKey: [null, Validators.required],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            // FirstName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
            BusinessOwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            // MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            // RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
            // ReferralCode: [null],
            StatusCode: this._HelperService.AppConfig.Status.Active,
        });
    }


    // _ToogleContactInfo() {
    //     this._ContactInfo = !this._ContactInfo;

    //     if (!this._ContactInfo) {
    //         this.Form_AddUser.controls['FirstName'].patchValue(this.Form_AddUser.controls['Name'].value);
    //         this.Form_AddUser.controls['MobileNumber'].patchValue(this.Form_AddUser.controls['ContactNumber'].value);
    //         this.Form_AddUser.controls['EmailAddress'].patchValue(this.Form_AddUser.controls['EmailAddress'].value);
    //     } else {
    //         this.Form_AddUser.controls['FirstName'].patchValue(null);
    //         this.Form_AddUser.controls['MobileNumber'].patchValue(null);
    //         this.Form_AddUser.controls['EmailAddress'].patchValue(null);
    //     }
    // }

    Form_AddUser_Clear() {
        this.Form_AddUser_Latitude = 0;
        this.Form_AddUser_Longitude = 0;
        //this.Form_AddUser.reset();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        //this.Form_AddUser_Load();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    Form_AddUser_Process(value?: any) {
        var _FormValue = this.Form_AddUser.value;
        _FormValue.Latitude = this.Form_AddUser_Latitude;
        _FormValue.Longitude = this.Form_AddUser_Longitude;
        _FormValue.ProgramId = this.programId;
        // if (_FormValue.RewardPercentage == undefined) {
        //     this._HelperService.NotifyError('Enter reward percentage');
        // }
        // else if (isNaN(_FormValue.RewardPercentage) == true) {
        //     this._HelperService.NotifyError('Enter valid reward percentage');
        // }
        // else if (parseFloat(_FormValue.RewardPercentage) > 100) {
        //     this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        // }
        // else if (parseFloat(_FormValue.RewardPercentage) < 0) {
        //     this._HelperService.NotifyError('Reward percentage must be greater than 0 and less than 101');
        // }
        // else 

        if (this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
            this._HelperService.NotifyError("Please set your registered address");
        }
        else {
            swal({
                position: 'top',
                title: 'Create merchant account ?',
                text: 'Please verify information and click on continue button to create merchant',
                animation: false,
                customClass: this._HelperService.AppConfig.Alert_Animation,
                showCancelButton: true,
                confirmButtonColor: this._HelperService.AppConfig.Color_Red,
                cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
                confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
                cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
            }).then((result) => {
                if (result.value) {

                    var tAddress = this._Address;
                    // console.log(tAddress,"store address");

                    this._AddressStore = tAddress;
                    this.ShowStoreAddressLocator = true;


                    this.MerchantSaveRequest = {};
                    this.MerchantSaveRequest = this.ReFormat_RequestBody();
                    this.MerchantSaveRequest.Address = this._Address.Address;
                    this.MerchantSaveRequest.AddressComponent = this._Address;
                    this.MerchantSaveRequest.ProgramId = _FormValue.ProgramId;


                    this._HelperService.IsFormProcessing = true;
                    let _OResponse: Observable<OResponse>;
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, this.MerchantSaveRequest);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {

                                this._SavedMerchant = _Response.Result;
                                this._HelperService.NotifySuccess(_Response.Message);
                                this._HelperService.CloseModal('_PreviewGeneral');
                                $("#wizard1").steps("next", {});
                                this._ToogleStoreCopy();

                            }
                            else {
                                this._HelperService.NotifyError(_Response.Message);
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
            });
        }
    }

    F_AddStore: FormGroup;
    _SavedStore: any = {};
    _CurrentAddress: any = {};
    _CurrentAddressStore: any = {};

    public F_AddStore_AddressChange(address: Address) {
        this._CurrentAddressStore = this._HelperService.GoogleAddressArrayToJson(address.address_components);
        if (this._CurrentAddressStore.country != this._HelperService.UserCountrycode) {
            this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
            this.reset1();
        }
        else {
            this.F_AddStore.controls['Address'].setValue(address.formatted_address);
            this.F_AddStore.controls['MapAddress'].setValue(address.formatted_address);
        }
    }

    reset1() {
        this.F_AddStore.controls['Address'].reset()
        this.F_AddStore.controls['CityName'].reset()
        this.F_AddStore.controls['StateName'].reset()
        this.F_AddStore.controls['CountryName'].reset()
    }
    // F_AddStore_Show() {
    // }
    // F_AddStore_Close() {
    //     this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Stores]);
    // }
    F_AddStore_Load() {
        this.F_AddStore = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.SaveStore,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            MerchantKey: [null],
            MerchantId: [null],
            // BranchKey: [null],
            // BranchId: [null],
            // RmKey: [null],
            // RmId: [null],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            EmailAddress: [null, Validators.compose([Validators.email, Validators.minLength(2)])],
            StatusCode: this._HelperService.AppConfig.Status.Active
        });
    }
    F_AddStore_Clear() {
        this.F_AddStore.reset();
        // this.F_AddStore_Load();
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this._HelperService.GetRandomNumber();
        this._HelperService.GeneratePassoword();
    }
    F_AddStore_Process() {
        if (this._AddressStore.Address == undefined || this._AddressStore.Address == null || this._AddressStore.Address == "") {
            this._HelperService.NotifyError("Please enter store location")
        }
        else {

            this.F_AddStore.controls['MerchantKey'].setValue(this._SavedMerchant.ReferenceKey);
            this.F_AddStore.controls['MerchantId'].setValue(this._SavedMerchant.ReferenceId);

            var _FormValue = this.F_AddStore.value;
            _FormValue.OwnerKey = this._SavedMerchant.ReferenceKey;
            _FormValue.Name = _FormValue.DisplayName;

            _FormValue.Address = this._AddressStore.Address,
                _FormValue.AddressComponent = this._AddressStore,


                this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account, _FormValue);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        this._SavedStore = _Response.Result;
                        this._HelperService.NotifySuccess('Store added');
                        this._HelperService.CloseModal('_PreviewStore');
                        $("#wizard1").steps("next", {});
                        this._ToogleStoreCopy();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }

    AddressChangeStore(Item) {
        this._AddressStore = Item;
    }


    Form_AddTerminal = this._FormBuilder.group({
        TerminalId: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
    });

    public TerminalsList_Filter_Provider_Option: Select2Options;
    public TerminalsList_Filter_Provider_Selected = 0;
    TerminalsList_Filter_Providers_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "AccountTypeCode",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    SearchCondition: "=",
                    SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Filter_Provider_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    ProviderName: any;

    TerminalsList_Filter_Providers_Change(event: any) {

        // console.log("eventasd", event);
        // console.log("event", event.value, event.data[0].DisplayName);
        this.ProviderName = event.data[0].DisplayName;

        if (event.value == this.TerminalsList_Filter_Provider_Selected) {

        }
        else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
            this._ProviderId = 0;
            this._ChangeDetectorRef.detectChanges();
            this._ProviderId = event.value;
            this._ProviderKey = event.data[0].ReferenceKey;
            this.TerminalsLists_Load();
            this._ChangeDetectorRef.detectChanges();
        }
    }

    _ToogleStoreCopy(): void {
        this._StoreInfoCopy = !this._StoreInfoCopy;

        // if (this._StoreInfoCopy) {
        if (true) {
            this.F_AddStore.controls['ContactNumber'].setValue(this.Form_AddUser.controls['ContactNumber'].value);
            this.F_AddStore.controls['DisplayName'].setValue(this.Form_AddUser.controls['DisplayName'].value);
            this.F_AddStore.controls['EmailAddress'].setValue(this.Form_AddUser.controls['EmailAddress'].value);
            this.F_AddStore.controls['Address'].setValue(this.Form_AddUser.controls['Address'].value);
            this.F_AddStore.controls['MapAddress'].setValue(this.Form_AddUser.controls['MapAddress'].value);
            this.F_AddStore.controls['CityName'].setValue(this.Form_AddUser.controls['CityName'].value);
            this.F_AddStore.controls['StateName'].setValue(this.Form_AddUser.controls['StateName'].value);
            this.F_AddStore.controls['CountryName'].setValue(this.Form_AddUser.controls['CountryName'].value);
            this.F_AddStore.controls['Latitude'].setValue(this.Form_AddUser.controls['Latitude'].value);
            this.F_AddStore.controls['Longitude'].setValue(this.Form_AddUser.controls['Longitude'].value);

            // this.F_AddStore_Latitude = this.Form_AddUser.controls['Latitude'].value;
            // this.F_AddStore_Longitude = this.Form_AddUser.controls['Longitude'].value;
            // this.F_AddStore_Address = this.Form_AddUser.controls['Address'].value;
        } else {


        }
    }

    public _ProviderId: number = 0;
    public _ProviderKey: string = '';

    public _TerminalList: any[] = [];
    public _SelectedTerminal: any = null;

    public TerminalsList_Option: Select2Options;
    public TerminalsList_Selected = 0;
    TerminalsLists_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: 'getterminals',
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceKey",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                {
                    SystemName: "ProviderId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    SearchCondition: "=",
                    SearchValue: this._ProviderId.toString()
                }
            ]
        };
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TerminalsList_Option = {
            placeholder: 'Filter by PTSP',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    TerminalsLists_Change(event: any) {


        if (event.value == this.TerminalsList_Selected) {

        }
        else if (event.value != this.TerminalsList_Selected) {
            this._SelectedTerminal = event.data[0];
        }
    }

    AddTerminal(): void {


        var terminal: any = {
            OperationType: 'edit',
            Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
            TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
            MerchantId: this._SavedMerchant.ReferenceId,
            MerchantKey: this._SavedMerchant.ReferenceKey,
            StoreId: this._SavedStore.ReferenceId,
            StoreKey: this._SavedStore.ReferenceKey,
            ProviderId: this._ProviderId,
            ProviderKey: this._ProviderKey,
            AcquirerId: this._HelperService.UserAccount.AccountId,
            AcquirerKey: this._HelperService.UserAccount.AccountKey,
            StatusCode: "default.active"
        };
        var terminalone: any = {
            OperationType: 'edit',
            Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
            TerminalId: this.Form_AddTerminal.controls['TerminalId'].value,
            MerchantId: this._SavedMerchant.ReferenceId,
            MerchantKey: this._SavedMerchant.ReferenceKey,
            StoreId: this._SavedStore.ReferenceId,
            StoreKey: this._SavedStore.ReferenceKey,
            ProviderId: this._ProviderId,
            ProviderKey: this._ProviderKey,
            AcquirerId: this._HelperService.UserAccount.AccountId,
            AcquirerKey: this._HelperService.UserAccount.AccountKey,
            StatusCode: "default.active",
            ProviderName: this.ProviderName
        };

        var ElementIndex: number = null;


        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].TerminalId == terminal.TerminalId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex == null) {
            // this._SelectedTerminal.element = undefined;
            // this._TerminalList.push(terminal);
            this._TerminalList.push(terminalone);
        }

    }


    AddTerminal_Process() {

        if (this._TerminalList.length == 0) {
            this._HelperService.NotifyError("Select Atleast One Terminal");
            return;
        }

        this._HelperService.terminalsavecount = 0;

        for (let index = 0; index < this._TerminalList.length; index++) {
            this._HelperService.IsFormProcessing = true;
            var _FormValue = this._TerminalList[index];


            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(
                this._HelperService.AppConfig.NetworkLocation.Acquirer.V3.Account,
                _FormValue
            );
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {

                        this._HelperService.terminalsavecount += 1;

                        this.Form_AddUser_Clear();

                        if (this._HelperService.terminalsavecount == this._TerminalList.length) {
                            this._ShowTerminalPreview();
                        }

                    } else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                }
            );
        }
    }

    terminal_Close() {
        //this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.AcquirerPanel.Merchants]);
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchants]);

    }

    _ShowGeneralPreview() {
        if (this._Address == undefined || this._Address == null || this._Address == "") {
            this._HelperService.NotifyError('Please select address');
        }
        else {
            this._HelperService.OpenModal('_PreviewGeneral');
        }
    }


    _ShowStorePreview() {
        if (this._AddressStore == undefined || this._AddressStore == null || this._AddressStore == "") {
            this._HelperService.NotifyError('Please select address');
        }
        else {
            this._HelperService.OpenModal('_PreviewStore');
        }
    }

    _ShowTerminalPreview() {
        this._HelperService.OpenModal('_PreviewTerminal');
    }

    RemoveTerminal(ReferenceId: number): void {

        var ElementIndex: number = null;
        for (let index = 0; index < this._TerminalList.length; index++) {
            if (this._TerminalList[index].ReferenceId == ReferenceId) {
                ElementIndex = index;
                break;
            }
        }

        if (ElementIndex != null) {
            this._TerminalList.splice(ElementIndex, 1);
        }

    }
    public GetBranches_Option: Select2Options;
    public GetBranches_Transport: any;
    GetBranches_List() {
        var PlaceHolder = "Select Branch";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            // SearchCondition: "",
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        }

        this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetBranches_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetBranches_Transport,
            multiple: false,
        };
    }
    GetBranches_ListChange(event: any) {
        // alert(event);\
        this.Form_AddUser.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId

            }
        );

        this.F_AddStore.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId

            }
        );
    }
    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "Name",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },

            ]
        }

        _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
        // alert(event);
        this.Form_AddUser.patchValue(
            {
                RmKey: event.data[0].ReferenceKey,
                RmId: event.data[0].ReferenceId
            }
        );

        this.F_AddStore.patchValue(
            {
                RmKey: event.data[0].ReferenceKey,
                RmId: event.data[0].ReferenceId
            }
        );
    }



    ReFormat_RequestBody(): void {
        var formValue: any = this.Form_AddUser.value;
        var formRequest: any = {

            OperationType: 'new',
            Task: formValue.Task,
            AccountId: formValue.AccountId,
            AccountKey: formValue.AccountKey,
            // BranchId: formValue.BranchId,
            // BranchKey: formValue.BranchKey,
            // RmId: formValue.RmId,
            // RmKey: formValue.RmKey,
            DisplayName: formValue.DisplayName,
            Name: formValue.Name,
            // FirstName: formValue.FirstName,
            BusinessOwnerName: formValue.BusinessOwnerName,
            // ReferralCode: formValue.ReferralCode,

            // MobileNumber: formValue.MobileNumber,
            ContactNumber: formValue.ContactNumber,
            EmailAddress: formValue.EmailAddress,
            // RewardPercentage: formValue.RewardPercentage,
            // 
            Address: this._Address.Address,
            AddressComponent: this._Address,
            // 

            // ContactPerson: {
            //     FirstName: formValue.FirstName,
            //     LastName: formValue.LastName,
            //     MobileNumber: formValue.MobileNumber,
            //     EmailAddress: formValue.EmailAddress
            // },
            Stores: [],
            StatusCode: formValue.StatusCode

        };

        return formRequest;

    }


}


