import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import swal from 'sweetalert2';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OUserDetails } from '../../service/service';

@Component({
    selector: 'hc-profile',
    templateUrl: './hcprofile.component.html',
})
export class HCProfileComponent implements OnInit {
    // OShowPassword: boolean = true;
    public Password:string;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {
    }
    ngOnInit() {
        this._HelperService.FullContainer = true;
        this.Form_UpdateUser_Load();
        this.Get_UserAccountDetails();
        this.Form_EditPassword_Load();
        this.Form_UpdateContactUser_Load();
        this.Form_Edit_Load();
        this.Form_UpdateCredentialUser_Load();
    }
    public isReadOnly = true;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        }
        else {
            this.isReadOnly = true;
        }
    }

    CityDet:any={};
    public Get_UserAccountDetails() {
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccount,
            Reference: this._HelperService.GetSearchConditionStrict('', 'ReferenceKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.UserAccount.AccountKey, '='),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService._UserAccount = _Response.Result as OUserDetails;
                    this.CityDet=_Response.Result.AddressComponent,
                    this.Password=_Response.Result.Password,
                    // console.log(this.CityDet);
                    // console.log(this._HelperService._UserAccount);
                    this._HelperService._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.CreateDate);
                    this._HelperService._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(this._HelperService._UserAccount.ModifyDate);
                    this._HelperService._UserAccount.StatusI = this._HelperService.GetStatusIcon(this._HelperService._UserAccount.StatusCode);
                    this._HelperService.IsFormProcessing = false;
                }
                else {
                    this._HelperService.IsFormProcessing = false;

                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {

                this._HelperService.HandleException(_Error);
            });
    }

    Form_UpdateUser: FormGroup;
    Form_UpdateUser_Show() {
    }
    Form_UpdateUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
            // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            RoleKey: null,
            Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
            DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
            ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
            EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            GenderCode: this._HelperService.AppConfig.Gender.Male,
            DateOfBirth: null,
            Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
            WebsiteUrl: [null, Validators.compose([Validators.minLength(2), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.WebsiteUrl)])],
            Description: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: [],
        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateUser_Load();
    }
    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Account details updated');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }
    Form_UpdateCredentialUser: FormGroup;
    Form_UpdateCredentialUser_Show() {
    }
    Form_UpdateCredentialUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateCredentialUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateCredentialUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
            // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            RoleKey: null,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            Description: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: [],
        });
    }
    Form_UpdateCredentialUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateCredentialUser_Load();
    }
    Form_UpdateCredentialUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Account details updated');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }

    Form_UpdateContactUser: FormGroup;
    Form_UpdateContactUser_Show() {
    }
    Form_UpdateContactUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateContactUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateContactUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            // AccountTypeCode: this._HelperService.AppConfig.AccountType.Admin,
            // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
            // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
            OwnerKey: null,
            RoleKey: null,
            FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
            SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
            DateOfBirth: null,
            Description: null,
            ReferralCode: null,
            ReferralUrl: null,

            CountValue: 0,
            AverageValue: 0,

            ApplicationStatusCode: null,

            EmailVerificationStatus: 0,
            EmailVerificationStatusDate: null,

            NumberVerificationStatus: 0,
            NumberVerificationStatusDate: null,

            CountryKey: null,
            RegionKey: null,
            RegionAreaKey: null,
            CityKey: null,
            CityAreaKey: null,

            IconContent: this._HelperService._FileSelect_Icon_Data,
            PosterContent: this._HelperService._FileSelect_Poster_Data,

            Owners: [],
            Configuration: [],
        });
    }
    Form_UpdateContactUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
        this.Form_UpdateContactUser_Load();
    }
    Form_UpdateContactUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                // _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Account details updated');
                        }
                        else {
                            this._HelperService.NotifyError('Unable to udpate account details');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }


    // for old password
    OShowPassword: boolean = true;
    OToogleShowHidePassword(): void {
        this.OShowPassword = !this.OShowPassword;
    }

    // for new password

    OShownewPassword: boolean = true;
    OToogleShowHidenewPassword(): void {
        this.OShownewPassword = !this.OShownewPassword;
    }

    Form_EditPassword: FormGroup;
    Form_EditPassword_Load() {
        this.Form_EditPassword = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountPassword,
            OldPassword: [null, Validators.required],
            Password: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern(this._HelperService.AppConfig.ValidatorRegex.Password)])],
        });
    }
    Form_EditPassword_Clear() {
        this.Form_EditPassword.reset();
    }
    Form_EditPassword_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: 'Update password',
            // text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                if (_FormValue.OldPassword != _FormValue.Password) {
                    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                    _OResponse.subscribe(
                        _Response => {
                            this._HelperService.IsFormProcessing = false;
                            if (_Response.Status == this._HelperService.StatusSuccess) {
                                this._HelperService.NotifySuccess('Password updated');
                                this.Form_EditPassword_Clear();
                            }
                            else {
                                this._HelperService.NotifyError('Enter valid old password');
                            }
                        },
                        _Error => {
                            this._HelperService.IsFormProcessing = false;
                            this._HelperService.HandleException(_Error);
                        });
                }
                else {
                    this._HelperService.NotifyError('Old Password and New Password should not be same');
                }
            }
        });

    }

    Form_Edit: FormGroup;
    Form_Edit_Load() {
        this.Form_Edit = this._FormBuilder.group({
            OperationType: 'new',
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccountAccessPin,
            OldAccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
            AccessPin: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
        });
    }
    Form_Edit_Clear() {
        this.Form_Edit.reset();
    }
    Form_Edit_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Blues,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.ReferenceKey = this._HelperService.UserAccount.AccountKey;
                // _FormValue.AuthPin = result.value;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess('Password updated');
                            this.Form_Edit_Clear();
                        }
                        else {
                            this._HelperService.NotifyError('Enter valid old password');
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }




}