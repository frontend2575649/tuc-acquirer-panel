import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import swal from 'sweetalert2';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../service/service';

@Component({
    selector: 'tu-appusers',
    templateUrl: './tuappusers.component.html',
})
export class TUAppUsersComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.AppUsers_Setup();
    }



    public AppUsers_Config: OList;
    AppUsers_Setup() {

        this.AppUsers_Config =
            {
                Id:null,
            Sort:null,
                Task: this._HelperService.AppConfig.Api.ThankUCash.GetAppUsers,
                Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
                Title: 'Merchants',
                StatusType: 'default',
                Type: this._HelperService.AppConfig.ListType.Owner,
                ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
                TableFields: [
                    {
                        DisplayName: 'Name',
                        SystemName: 'DisplayName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    {
                        DisplayName: 'Mobile',
                        SystemName: 'MobileNumber',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    {
                        DisplayName: 'Email',
                        SystemName: 'EmailAddress',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    {
                        DisplayName: 'Gender',
                        SystemName: 'GenderName',
                        DataType: this._HelperService.AppConfig.DataType.Text,
                        Class: '',
                        Show: false,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    // {
                    //     DisplayName: 'Credit',
                    //     SystemName: 'Credit',
                    //     DataType: this._HelperService.AppConfig.DataType.Decimal,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.AppUser.Dashboard
                    // },
                    // {
                    //     DisplayName: 'Debit',
                    //     SystemName: 'Debit',
                    //     DataType: this._HelperService.AppConfig.DataType.Decimal,
                    //     Class: '',
                    //     Show: true,
                    //     Search: true,
                    //     Sort: true,
                    //     ResourceId: null,
                    //     
                    //     // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.AppUser.Dashboard
                    // },
                    {
                        DisplayName: 'Total Purchase',
                        SystemName: 'Purchase',
                        DataType: this._HelperService.AppConfig.DataType.Decimal,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    {
                        DisplayName: 'Visits',
                        SystemName: 'Transactions',
                        DataType: this._HelperService.AppConfig.DataType.Number,
                        Class: '',
                        Show: true,
                        Search: true,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },

                    {
                        DisplayName: 'Last Visit',
                        SystemName: 'LastTransactionDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    {
                        DisplayName: "Active Since",
                        SystemName: 'CreateDate',
                        DataType: this._HelperService.AppConfig.DataType.Date,
                        Class: 'td-date',
                        Show: true,
                        Search: false,
                        Sort: true,
                        ResourceId: null,
                        
                        // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
                    },
                    // {
                    //     DisplayName: this._HelperService.AppConfig.CommonResource.CreatedBy,
                    //     SystemName: 'CreatedByDisplayName',
                    //     DataType: this._HelperService.AppConfig.DataType.Text,
                    //     Show: true,
                    //     Search: false,
                    //     Sort: true,
                    //     ResourceId: null,
                    // }
                ]
            }
        this.AppUsers_Config = this._DataHelperService.List_Initialize(this.AppUsers_Config);
        this.AppUsers_GetData();
    }
    AppUsers_ToggleOption_Date(event: any, Type: any) {
        this._HelperService.GetAppUsersOverview(this._HelperService.AppConfig.ActiveOwnerKey, null, event.start, event.end);
        this.AppUsers_ToggleOption(event, Type);
    }
    AppUsers_ToggleOption(event: any, Type: any) {
        this.AppUsers_Config = this._DataHelperService.List_Operations(this.AppUsers_Config, event, Type);
        if (this.AppUsers_Config.RefreshData == true) {
            this.AppUsers_GetData();
        }
    }
    AppUsers_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.AppUsers_Config);
        this.AppUsers_Config = TConfig;
    }
    AppUsers_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

}