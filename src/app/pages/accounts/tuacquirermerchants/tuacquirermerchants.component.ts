import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon
} from "../../../service/service";
import swal from "sweetalert2";

@Component({
  selector: "tu-acquirermerchants",
  templateUrl: "./tuacquirermerchants.component.html"
})
export class TUAcquirerMerchantsComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }
  ngOnInit() {
    this.PosTerminalsList_Setup();
  }

  public PosTerminalsList_Config: OList;
  PosTerminalsList_Setup() {
    this.PosTerminalsList_Config = {
      Id:null,
            Sort:null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.ThankU,
      Title: "Merchants",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.Owner,
      Status: 2,
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      TableFields: [
        {
          DisplayName: "Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        },
        {
          DisplayName: "Contact",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        },
        {
          DisplayName: "Email",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        },
        {
          DisplayName: "Stores",
          SystemName: "SubAccounts",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null
        },
        {
          DisplayName: "Reward %",
          SystemName: "RewardPercentage",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        },
        {
          DisplayName: "Status",
          SystemName: "StatusName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
            .PanelAcquirer.MerchantManager
        }
      ]
    };
    this.PosTerminalsList_Config = this._DataHelperService.List_Initialize(
      this.PosTerminalsList_Config
    );
    this.PosTerminalsList_GetData();
  }
  PosTerminalsList_ToggleOption(event: any, Type: any) {
    this.PosTerminalsList_Config = this._DataHelperService.List_Operations(
      this.PosTerminalsList_Config,
      event,
      Type
    );
    if (this.PosTerminalsList_Config.RefreshData == true) {
      this.PosTerminalsList_GetData();
    }
  }
  PosTerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.PosTerminalsList_Config
    );
    this.PosTerminalsList_Config = TConfig;
  }
  PosTerminalsList_RowSelected(ReferenceData) {
    var ReferenceKey = ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
  }
}
