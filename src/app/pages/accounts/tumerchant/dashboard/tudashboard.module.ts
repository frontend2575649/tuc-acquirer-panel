import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { ChartsModule } from "ng2-charts";
import { AgmCoreModule } from "@agm/core";
import { AgmOverlays } from "agm-overlays";
import { TUMerchantDashboardComponent } from "./tudashboard.component";
const routes: Routes = [{ path: "", component: TUMerchantDashboardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUMerchantDashboardRoutingModule {}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    ChartsModule,
    TUMerchantDashboardRoutingModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo"
    })
  ],
  declarations: [TUMerchantDashboardComponent]
})
export class TUMerchantDashboardModule {}
