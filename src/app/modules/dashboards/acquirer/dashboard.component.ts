import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import * as Feather from 'feather-icons';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit {
    @ViewChild(DaterangePickerComponent)
    private picker: DaterangePickerComponent;
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        private renderer2: Renderer2
    ) {
    }
    public TodayStartTime = null;
    public TodayEndTime = null;

    Type = 5;
    StartTime = null;
    EndTime = null;
    CustomType = 1;
    ngOnInit() {

        this._HelperService.FullContainer = false;
        if (this.StartTime == undefined) {
            this.StartTime = moment().startOf('month');
            this.EndTime = moment().endOf('month');
        }

        if (this.TodayStartTime == undefined) {
            this.TodayStartTime = moment().startOf('day');
            this.TodayEndTime = moment().add(1, 'minutes');
        }
        this.LoadData();
    }

    UserAnalytics_DateChange(Type) {
        this.Type = Type;
        var SDate;
        if (Type == 1) {
            SDate =
            {
                start: moment().startOf('day'),
                end: moment().endOf('day'),
            }
        }
        else if (Type == 2) {
            SDate =
            {
                start: moment().subtract(1, 'days').startOf('day'),
                end: moment().subtract(1, 'days').endOf('day'),
            }
        }
        else if (Type == 3) {
            SDate =
            {
                start: moment().startOf('isoWeek'),
                end: moment().endOf('isoWeek'),
            }
        }
        else if (Type == 4) {
            SDate =
            {
                start: moment().subtract(1, 'weeks').startOf('isoWeek'),
                end: moment().subtract(1, 'weeks').endOf('isoWeek'),
            }
        }
        else if (Type == 5) {
            SDate =
            {
                start: moment().startOf('month'),
                end: moment().endOf('month'),
            }
        }
        else if (Type == 6) {
            SDate =
            {
                start: moment().startOf('month').subtract(1, 'month'),
                end: moment().startOf('month').subtract(1, 'days'),
            }
        }
        else if (Type == 7) {
            SDate =
            {
                start: new Date(2017, 0, 1, 0, 0, 0, 0),
                end: moment().endOf('day'),
            }
        }
        if (this.picker.datePicker != undefined) {
            this.picker.datePicker.setStartDate(SDate.start);
            this.picker.datePicker.setEndDate(SDate.end);
        }
        this.StartTime = SDate.start;
        this.EndTime = SDate.end;
        this.LoadData();
    }
    LoadData() {

        this.StartTime = new Date(2017, 0, 1, 0, 0, 0, 0);
        this.EndTime = moment().add(2, 'days');

        this.GetSalesSummary();
        this.GetAccountOverviewLite();
        this._HelperService.GetAccountOverview(this._HelperService.AppConfig.ActiveOwnerKey, null, this.StartTime, this.EndTime);
        this._HelperService.GetRewardTypeOverview(this._HelperService.AppConfig.ActiveOwnerKey, null, this.StartTime, this.EndTime);
    }







    GetSalesSummary() {
        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getsalessummary',
            UserAccountId: this._HelperService.AppConfig.ActiveOwnerId,
            UserAccountTypeId: this._HelperService.AppConfig.AccountType.AcquirerId,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCAnalytics, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }






    GetAccountOverviewLite() {
        this._HelperService.IsFormProcessing = true;
        var Data = {
            Task: 'getaccountoverviewlite',
            StartTime: this.TodayStartTime,
            EndTime: this.TodayEndTime,
            UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }
    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null, Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
            Title: 'Sales History',
            StatusType: 'transaction',
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'TransactionDate desc',
            RefreshCount: false,
            PageRecordLimit: 6,
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: true,
                    IsDateSearchField: true,
                },
            ]
        }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,

        }






}


export class OAccountOverview {
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
}