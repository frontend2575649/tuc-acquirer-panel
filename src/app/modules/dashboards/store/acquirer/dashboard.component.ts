import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse } from '../../../../service/service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit, OnDestroy {

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef

    ) {
        this._HelperService.ShowDateRange = true;
    }

    //#region DougnutConfig 

    public dougnoutOptions = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
            display: false,
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    };

    public doughnutcard = [{ backgroundColor: ['#FFC20A', '#10b759'] }];
    public datapieCard = {
        labels: ['OtherCard', 'Owner Card'],
        datasets: [{
            data: [0, 0],
        }],

    };

    public doughnuttype = [{ backgroundColor: ['#00cccc', '#f10075'] }];
    public datapieType = {
        labels: ['Cash', 'Card'],
        datasets: [{
            data: [0, 0],
            backgroundColor: ['#66a4fb', '#4cebb5']
        }]
    };

    //#endregion

    public _DateSubscription: Subscription = null;

    ngOnDestroy() {
        this._DateSubscription.unsubscribe();
    }

    ngOnInit() {



        this._HelperService.FullContainer = false;

        this._HelperService.ResetDateRange();

        this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
            this.GetAccountOverviewLite();
        });

        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

            if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
            } else {
                this._HelperService.ReloadEventEmit.emit(0);
                this.GetAccountOverviewLite();
            }
        });

    }

    //#region AccountOverview 

    @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

    public _AccountOverview: OAccountOverview =
        {
            ActiveMerchants: 0,
            ActiveMerchantsDiff: 0,
            ActiveTerminals: 0,
            ActiveTerminalsDiff: 0,
            CardRewardPurchaseAmount: 0,
            CardRewardPurchaseAmountDiff: 0,
            CashRewardPurchaseAmount: 0,
            CashRewardPurchaseAmountDiff: 0,
            Merchants: 0,
            PurchaseAmount: 0,
            PurchaseAmountDiff: 0,
            Terminals: 0,
            Transactions: 0,
            TransactionsDiff: 0,
            TotalTransactions: 0,
            TotalSale: 0,
            AverageTransactions: 0,
            AverageTransactionAmount: 0,
            CashTransactionAmount: 0,
            CashTransPerTerm: 0,
            CardTransactionsAmount: 0,
            CardTransPerTerm: 0,
            OwnerCardTotal: 0,
            OtherCardTotal: 0,
            OwnerCardTransactions: 0,
            CardTypeSale: [],
            Others: {}
            // OwnerCardTypeSale:[]


        }

    GetAccountOverviewLite() {



        this._HelperService.toogleFormProcessingFlag(true);
        var Data = {
            Task: 'getaccountoverview',
            StartTime: this._HelperService.DateRangeStart,
            EndTime: this._HelperService.DateRangeEnd,
            AccountId: this._HelperService.UserAccount.AccountId,
            AccountKey: this._HelperService.UserAccount.AccountKey,
            SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,

            // SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
            // SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
        _OResponse.subscribe(
            _Response => {



                // console.log(this._AccountOverview);



                this._HelperService.toogleFormProcessingFlag(false);

                var OwnerCardTotal: any = {
                    "Name": "OwnerAllCards",
                    "Transactions": 0,
                    "TransactionsPerc": 0.0,
                    "Amount": 0.0
                }

                var OtherCardTotal: any = {
                    "Name": "OtherAllCards",
                    "Transactions": 0,
                    "TransactionsPerc": 0.0,
                    "Amount": 0.0
                }

                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._AccountOverview = _Response.Result as OAccountOverview;

                    this.datapieType.datasets[0].data[1] = this._AccountOverview['CardTransactions'];
                    this.datapieType.datasets[0].data[0] = this._AccountOverview['CashTransactions'];

                    //#region CardCashTransPerc 

                    this._AccountOverview["CardTransPerTerm"] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
                    this._AccountOverview["CashTransPerTerm"] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;

                    //#endregion




                    //#region OwnerCardTotals 
                    for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
                        const element = this._AccountOverview['CardTypeSale'][index];
                        var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview['TotalTransactions']);
                        element['TransactionPer'] = element['Transactions'] * _TempVal;

                    }
                    for (let index = 0; index < this._AccountOverview['OwnerCardTypeSale'].length; index++) {
                        const element = this._AccountOverview['OwnerCardTypeSale'][index];
                        OwnerCardTotal.Transactions += element.Transactions;
                        OwnerCardTotal.Amount += element.Amount;
                    }

                    //#endregion

                    //#region OwnerOtherCardTransPerc 

                    OwnerCardTotal.TransactionsPerc = (this._HelperService.DivideTwoNumbers(OwnerCardTotal.Transactions, this._AccountOverview['OwnerCardTransactions']) * 100);
                    OtherCardTotal.TransactionsPerc = (this._HelperService.DivideTwoNumbers(OtherCardTotal.Transactions, this._AccountOverview['OwnerCardTransactions']) * 100);

                    //#endregion

                    //#region OtherCardTotals 

                    OtherCardTotal.Transactions = this._AccountOverview['OwnerCardTransactions'] - OwnerCardTotal.Transactions;
                    OtherCardTotal.Amount = this._AccountOverview['OwnerCardTransactionsAmount'] - OwnerCardTotal.Amount;

                    //#endregion

                    this.datapieCard.datasets[0].data[1] = OwnerCardTotal.Transactions;
                    this.datapieCard.datasets[0].data[0] = OtherCardTotal.Transactions;

                    this._AccountOverview['OwnerCardTotal'] = OwnerCardTotal;
                    this._AccountOverview['OtherCardTotal'] = OtherCardTotal;

                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }

                var other: any = {
                    Name: "Other",
                    Transactions: 0,
                    Amount: 0.0,
                    TransactionPer: 0

                }

                //#region otherTotals 

                for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
                    let element = this._AccountOverview['CardTypeSale'][index];
                    other.Transactions += element['Transactions'];
                    other.Amount += element['Amount'];
                }

                other.Transactions = this._AccountOverview["TotalTransactions"] - other.Transactions;
                other.Amount = this._AccountOverview["TotalSale"] - other.Amount;
                other.TransactionPer = (this._HelperService.DivideTwoNumbers(other.Transactions, this._AccountOverview['TotalTransactions']) * 100)

                //#endregion

                this._AccountOverview['Others'] = other;

                this.components.forEach(a => {
                    try {
                        if (a.chart) a.chart.update();
                    } catch (error) {
                        console.log('chartjs error');
                    }
                });
            },

            _Error => {
                this._HelperService.toogleFormProcessingFlag(false);
                this._HelperService.HandleException(_Error);
            });
    }

    //#endregion

    //#region Transactions 

    public TUTr_Config: OList;
    TUTr_Setup() {
        this.TUTr_Config =
        {
            Id: null, Sort: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetSaleTransactions,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCTransCore,
            Title: 'Sales History',
            StatusType: 'transaction',
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'CreatedById', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
            Status: this._HelperService.AppConfig.StatusList.transactiondefaultitem,
            Type: this._HelperService.AppConfig.ListType.All,
            DefaultSortExpression: 'TransactionDate desc',
            RefreshCount: false,
            PageRecordLimit: 6,
            TableFields: [
                {
                    DisplayName: '#',
                    SystemName: 'ReferenceId',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Show: true,
                    Search: false,
                    Sort: false
                },
                {
                    DisplayName: 'Date',
                    SystemName: 'TransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Show: true,
                    Search: false,
                    Sort: true,
                    IsDateSearchField: true,
                },
            ]
        }
        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this.TUTr_GetData();
    }
    TUTr_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }

    //#endregion

}

export class OAccountOverview {
    public TotalTransactions: number;
    public Others: any;
    public TotalSale: number;
    public AverageTransactions: number;
    public AverageTransactionAmount: number;
    public CashTransactionAmount: number;
    public CashTransPerTerm: number;
    public CardTransactionsAmount: number;
    public CardTransPerTerm: number;
    public OwnerCardTotal: number;
    public OtherCardTotal: number;
    public CardTypeSale: any;
    public Merchants: number;
    public ActiveMerchants: number;
    public ActiveMerchantsDiff: number;
    public Terminals: number;
    public ActiveTerminals: number;
    public ActiveTerminalsDiff: number;
    public Transactions: number;
    public TransactionsDiff: number;
    public PurchaseAmount: number;
    public PurchaseAmountDiff: number;
    public CashRewardPurchaseAmount: number;
    public CashRewardPurchaseAmountDiff: number;
    public CardRewardPurchaseAmount: number;
    public CardRewardPurchaseAmountDiff: number;
    public OwnerCardTransactions: number;
}