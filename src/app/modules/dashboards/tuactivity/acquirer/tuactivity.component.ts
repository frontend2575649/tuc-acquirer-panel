import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseChartDirective } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
import * as Feather from 'feather-icons';
declare var moment: any;

@Component({
  selector: "tuactivity",
  templateUrl: "./tuactivity.component.html",
  styles: [
    `
      agm-map {
        height: 300px;
      }
    `
  ]
})
export class TUActivityComponent implements OnInit {

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {

  }

  //#endregion

  ngOnInit() {
    this._HelperService.FullContainer = false;
    
  
    Feather.replace();

    //start time and end time for overview
    this.TodayStartTime = moment().subtract(6, 'd').startOf('day');
    this.TodayEndTime = moment().endOf('day');
    this.GetBranches_List();
    this.GetMangers_List();
    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.TodayStartTime), moment(this.TodayEndTime));
    this.LoadData();
// console.log(this.barChartLabels)
  }

  LoadData() {

    this.GetAccountOverviewLite();
    this.GetLastSevenData();
    this.Alert_Terminal();
  }

  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {
    this.TodayStartTime = moment(event.start).startOf("day");
    this.TodayEndTime = moment(event.end).endOf("day");

    //#region refreshTerminals 

    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    this.TerminalsList_GetData();

    //#endregion



    this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(event.start).startOf("day"), moment(event.end).endOf("day"));

    this.GetAccountOverviewLite();
    this.GetLastSevenData();
    this._HelperService.StopClickPropogation();
  }

  //#endregion

  //#region Filters 

  //#region Branch 

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }
    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: "All Branches",
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  GetBranches_ListChange(event: any) {
    // this.Form_AddUser.patchValue(
    //     {
    //         RoleKey: event.value
    //     }
    // );

  }

  //#endregion

  //#region Manager 

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: "All RMs",
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {

  }

  //#endregion

  //#endregion

  //#region AccountOverview 

  public TodayStartTime = null;
  public TodayEndTime = null;

  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      TerminalStatus: {},
      TotalTerminals: 0,
      TotalSale: 0,
      TotalTransactions: 0,
      AverageTransactionAmount: 0,
      ActiveTerminalsPerc: 0,
      IdleTerminalsPerc: 0,
      DeadTerminalsPerc: 0,
      UnusedTerminalsPerc: 0,
      Active: 0,
      Total: 0,
      Idle: 0,
      Dead: 0,
      Inactive: 0,



    }

  GetAccountOverviewLite() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverview',
      StartTime: this.TodayStartTime,
      EndTime: this.TodayEndTime,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._AccountOverview = _Response.Result as OAccountOverview;

          this._AccountOverview['CardTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CardTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          this._AccountOverview['CashTransactionsPerc'] = (this._HelperService.DivideTwoNumbers(this._AccountOverview['CashTransactions'], this._AccountOverview['TotalTransactions'])) * 100;
          if (this._AccountOverview.TerminalStatus['Total'] != 0) {
            var _TempVal = this._HelperService.DivideTwoNumbers(100, this._AccountOverview.TerminalStatus['Total']);
            this._AccountOverview["IdleTerminalsPerc"] = this._AccountOverview.TerminalStatus['Idle'] * _TempVal;
            this._AccountOverview["ActiveTerminalsPerc"] = this._AccountOverview.TerminalStatus['Active'] * _TempVal;
            this._AccountOverview["DeadTerminalsPerc"] = this._AccountOverview.TerminalStatus['Dead'] * _TempVal;
            this._AccountOverview["UnusedTerminalsPerc"] = this._AccountOverview.TerminalStatus['Inactive'] * _TempVal;
          } else {
            this._AccountOverview["IdleTerminalsPerc"] = 0;
            this._AccountOverview["ActiveTerminalsPerc"] = 0;
            this._AccountOverview["DeadTerminalsPerc"] = 0;
            this._AccountOverview["UnusedTerminalsPerc"] = 0;
          }
          var other: any = {
            Name: "Other",
            Transactions: 0,
            Amount: 0.0,
            TransactionPerc: 0.0
          }

          for (let index = 0; index < this._AccountOverview['CardTypeSale'].length; index++) {
            let element = this._AccountOverview['CardTypeSale'][index];

            other.Transactions += element['Transactions'];
            other.Amount += element['Amount'];


          }

          other.Transactions = this._AccountOverview["TotalTransactions"] - other['Transactions'];
          other.Amount = this._AccountOverview["TotalSale"] - other['Amount'];
          other.TransactionPerc = this._HelperService.DivideTwoNumbers(other['Transactions'], this._AccountOverview["TotalTransactions"]) * 100;

          this._AccountOverview['Others'] = other;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region BarChartConfig 
  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }

  public barChartLabels = [];
  // public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545', '#DC3545'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartColors = [{ backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }, { backgroundColor: [] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [], label: 'Active' },
    { data: [], label: 'Idle' },
    { data: [], label: 'Dead' },
    { data: [], label: 'Inactive' },
    // changed the bar charts name
    // { data: [], label: 'Remote' },
    // { data: [], label: 'Remote' },
    // { data: [], label: 'Visit' },
    // { data: [], label: 'Visit' },
  ];

  //#endregion

  //#region last7day 

  public _LastSevenDaysData: any = {};

  GetLastSevenData() {

    // this.barChartData = [
    //   { data: [], label: 'Remote' },
    //   { data: [], label: 'Remote' },
    //   { data: [], label: 'Visit' },
    //   { data: [], label: 'Visit' },
    // ];
    this.barChartData = [
      { data: [], label: 'Active' },
      { data: [], label: 'Idle' },
      { data: [], label: 'Dead' },
      { data: [], label: 'Inactive' },
    ];

    this.barChartColors = [
      { backgroundColor: [] },
      { backgroundColor: [] },
      { backgroundColor: [] },
      { backgroundColor: [] }
    ];

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getterminalactivityhistory',
      StartTime: this.TodayStartTime,
      EndTime: this.TodayEndTime,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LastSevenDaysData = _Response.Result;

          for (let i = 0; i < this._LastSevenDaysData.length; i++) {
            // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;

            this.barChartColors[0].backgroundColor.push('#00CCCC');
            this.barChartColors[1].backgroundColor.push('#FFC20A');
            this.barChartColors[2].backgroundColor.push('#F10875');
            this.barChartColors[3].backgroundColor.push('#0168FA');

            this.barChartData[0].data.push(this._LastSevenDaysData[i].Data.Active);
            this.barChartData[1].data.push(this._LastSevenDaysData[i].Data.Idle);
            this.barChartData[2].data.push(this._LastSevenDaysData[i].Data.Dead);
            this.barChartData[3].data.push(this._LastSevenDaysData[i].Data.Inactive);

          }

          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  //#region Terminals 

  public TerminalsList_Config: OList;
  Alert_Terminal() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: " Terminal List",
      StatusType: "default",
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      Type: this._HelperService.AppConfig.ListType.All,
      SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      DefaultSortExpression: 'LastTransactionDate  desc',
      RefreshData: true,
      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Merchant',
          SystemName: 'MerchantDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'ApplicationStatusId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config,
    );
    this.TerminalsList_GetData();

  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (this.TerminalsList_Config.RefreshData == true) {
      this.TerminalsList_GetData();
    }
  }

  TerminalsList_GetData() {
    var TConfig = this._DataHelperService.List_GetDataTerm(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }

  TerminalsList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }

  //#endregion

}
export class OAccountOverview {
  public TerminalStatus?: any;
  public TotalTerminals?: number;
  public TotalSale?: number;
  public AverageTransactionAmount?: number;
  public TotalTransactions?: number;
  public ActiveTerminalsPerc?: number;
  public IdleTerminalsPerc?: number;
  public DeadTerminalsPerc?: number;
  public UnusedTerminalsPerc?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public Idle?: number;
  public Total?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}