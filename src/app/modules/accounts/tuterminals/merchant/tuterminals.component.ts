import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from "../../../../service/service";
import { timeout } from 'rxjs/operators';
declare var moment: any;
declare var PerfectScrollbar: any;
declare var $: any;
import swal from "sweetalert2";
@Component({
  selector: "tu-tuterminals",
  templateUrl: "./tuterminals.component.html"
})
export class TUTerminalsComponent implements OnInit {

  Terminals = [];

  StoreName: string;
  TerminalId: string;
  SerialNumber: string
  ProviderName: string;
  TIDs: string[] = [];
  TID: string;
  TID2: string;
  TID3: string;
  TID4: string;
  TID5: string;


  CountTerminals: number;
  curentsize = 0;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

    this._HelperService.ShowDateRange = false;

  }

  ngOnInit() {
    Feather.replace();



    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this.TerminalsList_Setup();
        this.GetTerminalCount();

        //#region TerminalDropDowns 

        this.TerminalsList_Filter_Stores_Load();
        this.TerminalsList_Filter_Banks_Load();
        this.TerminalsList_Filter_Providers_Load();
        this.TerminalsList_Filter_Merchants_Load();

        //#endregion

        //#region  Dropdowns

        this.ProvidersList_Load();
        this.StoresList_Load();
        this.Form_AddUser_Load();
        this.GetBranches_List();
        this.GetMangers_List();

        //#endregion
      }
    });
  }


  AddTID() {
    for (let index = 0; index < this.TIDs.length; index++) {
      const element = this.TIDs[index];
      // console.log(element.length)
      // debugger;
      // element.replace(/\s+/g, ' ')
      if (element.trim().length > 1) {
        this.Terminals.push({
          TerminalId: element, StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value, ProviderId: this.Form_AddUser.controls['ProviderId'].value, ProviderKey: this.Form_AddUser.controls['ProviderKey'].value, StoreId: this.Form_AddUser.controls['StoreId'].value, StoreKey: this.Form_AddUser.controls['StoreKey'].value, AcquirerId: this._HelperService.UserAccount.AccountId,
          AcquirerKey: this._HelperService.UserAccount.AccountKey,
          MerchantId: this._HelperService.AppConfig.ActiveReferenceId,
          MerchantKey: this._HelperService.AppConfig.ActiveReferenceKey
        });

      }
    }
    this.TIDs = [];
    this.TID = null;
  }
  DeleteRowTerminal(indexOfTID) {
    // console.log("Term", this.Terminals)
    if (indexOfTID > -1) {
      this.Terminals.splice(indexOfTID, 1);

    }
    // console.log("Term After", this.Terminals)

  }

  RemoveTID(i) {
    if (i > -1) {
      this.TIDs.splice(i, 1);
    }

    // this.TIDs = this.TIDs.splice(i, 1);
  }

  AddMore() {
    // this.Terminals = this.Terminals.concat([{}, {}, {}, {}, {}]);

    for (let index = 0; index < this.CountTerminals; index++) {
      this.Terminals.push({ StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value });

    }
    this.CountTerminals = 0;
    // this.Terminals = this.Terminals.concat([{ StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value },
    // { StoreName: this.Form_AddUser.controls['StoreName'].value, ProviderName: this.Form_AddUser.controls['ProviderName'].value }]);

  }


  KeyUP(): void {
    this.TIDs = this.TID.split(',');
  }

  CopyToAll() {
    for (let index = 0; index < this.Terminals.length; index++) {
      const element = this.Terminals[index];

      element['StoreName'] = this.Form_AddUser.controls['StoreName'].value
      element['StoreId'] = this.Form_AddUser.controls['StoreId'].value
      element['StoreKey'] = this.Form_AddUser.controls['StoreKey'].value

      element['ProviderName'] = this.Form_AddUser.controls['ProviderName'].value
      element['ProviderId'] = this.Form_AddUser.controls['ProviderId'].value
      element['ProviderKey'] = this.Form_AddUser.controls['ProviderKey'].value

    }
  }

  //#region Terminals 



  ToogleType(type: string): void {
    this.TerminalsList_Config.Type = type;
    this.TerminalsList_GetData();
  }


  public _AOverview: any = {
    TerminalStatus: {
      Active: 0,
      Dead: 0,
      Idle: 0,
      Inactive: 0,
      Total: 0
    }
  };



  public TerminalsList_Config: OList;
  TerminalsList_Setup() {
    this.TerminalsList_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminals,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "All Terminals",
      StatusType: "default",
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '='),
      Type: this._HelperService.AppConfig.ListType.All,
      DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: 'TID',
          SystemName: 'TerminalId',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Provider',
          SystemName: 'ProviderName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Store',
          SystemName: 'StoreName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Trans',
          SystemName: 'TotalTransaction',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: false,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: true,
          IsDateSearchField: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Status',
          SystemName: 'StatusCode',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
      ]
    };
    this.TerminalsList_Config = this._DataHelperService.List_Initialize(
      this.TerminalsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Terminal,
      this.TerminalsList_Config
    );
    this.TerminalsList_GetData();
  }
  TerminalsList_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TerminalsList_Config.Sort.SortOptions.length; index++) {
        const element = this.TerminalsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {

          element.SystemActive = true;

        }
        else {
          element.SystemActive = false;

        }
      }
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TerminalsList_Config);
    this.TerminalsList_Config = this._DataHelperService.List_Operations(
      this.TerminalsList_Config,
      event,
      Type
    );
    if (
      (this.TerminalsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TerminalsList_GetData();
    }
  }

  public ListType: number;

  MerchantsList_ListTypeChange(Type) {
    this.ListType = Type;
    this.TerminalsList_Setup();
  }


  TerminalsList_GetData() {


    this.TerminalsList_Config.ListType = this.ListType;
    this.TerminalsList_Config.SearchBaseCondition = "";


    if (this.TerminalsList_Config.ListType == 1) { this.TerminalsList_Config.Type = "active"; }
    else if (this.TerminalsList_Config.ListType == 2) { this.TerminalsList_Config.Type = "idle"; }
    else if (this.TerminalsList_Config.ListType == 3) { this.TerminalsList_Config.Type = 'dead'; }

    else if (this.TerminalsList_Config.ListType == 4) { this.TerminalsList_Config.Type = 'unused'; }
    else if (this.TerminalsList_Config.ListType == 5) { this.TerminalsList_Config.Type = 'all'; }
    else { this.TerminalsList_Config.DefaultSortExpression = 'CreateDate desc'; }


    var TConfig = this._DataHelperService.List_GetData(
      this.TerminalsList_Config
    );
    this.TerminalsList_Config = TConfig;
  }


  TerminalsList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Terminal.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveTerminal,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminal
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);


  }

  //#endregion

  //#region AddUser 

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");

    setTimeout(() => {
      const scroll1 = new PerfectScrollbar('#tertable', {
        suppressScrollX: true
      });
    }, 120);

  }
  Form_AddUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    setTimeout(() => {
      this._HelperService.CloseModal("Form_AddUser_Content");
    }, 3000);

  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;



    this.Form_AddUser = this._FormBuilder.group({



      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.SaveTerminal,
      // AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      // AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Online,
      // RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource
      //   .System,

      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StoreName: null,
      // RmId: [null, Validators.required],
      // RmKey: [null, Validators.required],
      AcquirerId: this._HelperService.UserAccount.AccountId,
      AcquirerKey: this._HelperService.UserAccount.AccountKey,
      // BankKey: [null],
      // BranchId: [null, Validators.required],
      // BranchKey: [null, Validators.required],
      ProviderId: [null, Validators.required],
      ProviderKey: [null, Validators.required],
      ProviderName: null,
      TerminalId: [null],
      MerchantId: null,
      MerchantKey: null,


      // FirstName: [
      //   null,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.minLength(2),
      //     Validators.maxLength(128)
      //   ])
      // ],
      StatusCode: this._HelperService.AppConfig.Status.Active,

    });
  }
  Form_AddUser_Clear() {
    //  this.Terminals=null;
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  Form_AddUser_Process(_FormValue: any) {
    this.ResetFilterUI();
    // this.Form_AddUser.patchValue(
    //   {
    //     Form_AddUser.MerchantId= this._HelperService.AppConfig.ActiveReferenceId,
    //      MerchantKey: this._HelperService.AppConfig.ActiveReferenceKey,
    //   }
    // );

    var BulkTerminals = {
      Task: this._HelperService.AppConfig.Api.Core.SaveTerminals,
      Terminals: this.Terminals,
    }
    // _FormValue.TerminalId = this._HelperService.GenerateId();
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      BulkTerminals
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("New Terminal Added successfully");
          this.Terminals = [];
          this.Form_AddUser_Clear();
          this.Form_AddUser_Close();
          this.TerminalsList_Setup();
          this._HelperService.emitDetailsChangeEvent();
          if (_FormValue.OperationType == "close") {
            this.Form_AddUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }



  //#endregion

  //#region Terminal Dropdowns 

  //#region Bank 

  public TerminalsList_Filter_Bank_Option: Select2Options;
  public TerminalsList_Filter_Bank_Selected = 0;
  TerminalsList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Bank_Option = {
      placeholder: 'Sort by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Banks_Change(event: any) {
    if (event.value == this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.TerminalsList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '=');
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
      this.TerminalsList_Filter_Bank_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquirerId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Bank_Selected, '='));
    }
    this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  //#region provider 

  public TerminalsList_Filter_Provider_Option: Select2Options;
  public TerminalsList_Filter_Provider_Selected = 0;
  TerminalsList_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Provider_Option = {
      placeholder: 'Filter by PTSP',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Providers_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.ProvderEventProcessing(event);





    // if (event.value == this.TerminalsList_Filter_Provider_Selected) {
    //   var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
    //   this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
    //   this.TerminalsList_Filter_Provider_Selected = 0;
    // }
    // else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
    //   var SearchCase = this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '=');
    //   this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TerminalsList_Config.SearchBaseConditions);
    //   this.TerminalsList_Filter_Provider_Selected = event.value;
    //   this.TerminalsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'ProviderId', this._HelperService.AppConfig.DataType.Number, this.TerminalsList_Filter_Provider_Selected, '='));
    // }
    // this.TerminalsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }
  ProvderEventProcessing(event): void {
    if (event.value == this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Provider_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Provider_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Provider_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Provider_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ProviderId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Provider_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region store 

  public TerminalsList_Filter_Store_Option: Select2Options;
  public TerminalsList_Filter_Store_Selected = 0;
  TerminalsList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Store_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Store_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Store_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }


  //#endregion

  //#endregion

  //#region Dropdowns

  //#region store 

  public StoreKey: string = null;

  public StoresList_Option: Select2Options;
  public StoresList_Selected = 0;
  StoresList_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Change(event: any) {

    this.Form_AddUser.patchValue(
      {
        StoreKey: event.data[0].ReferenceKey,
        StoreId: event.data[0].ReferenceId,
        StoreName: event.data[0].DisplayName,



      }

    );

  }


  // ApplyFilters(event: any, Type: any, ButtonType: any): void {
  //   this._HelperService.MakeFilterSnapPermanent();
  //   this.TerminalsList_GetData();

  //   if (ButtonType == 'Sort') {
  //     $("#Applist_sdropdown").dropdown('toggle');
  //   } else if (ButtonType == 'Other') {
  //     $("#Applist_fdropdown").dropdown('toggle');
  //   }

  //   this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  // }

  // ResetFilters(event: any, Type: any): void {
  //   this._HelperService.ResetFilterSnap();
  //   this._FilterHelperService.SetMerchantConfig(this.Applist_Config);
  //   this.SetOtherFilters();

  //   this.Applist_GetData();

  //   this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  // }

  //#endregion


  OwnerEventProcessing(event: any): void {
    if (event.value == this.ProvidersList_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.ProvidersList_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.ProvidersList_Selected = null;
    } else if (event.value != this.ProvidersList_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.ProvidersList_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.ProvidersList_Selected = event.data[0].ReferenceKey;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "MerchantReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.ProvidersList_Selected,
          "="
        )
      );
    }

    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  public TerminalsList_Filter_Merchant_Option: Select2Options;
  public TerminalsList_Filter_Merchant_Selected = 0;
  TerminalsList_Filter_Merchants_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Merchant
        // }
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TerminalsList_Filter_Merchant_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TerminalsList_Filter_Merchants_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.TerminalsList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Merchant
    );

    this.MerchantEventProcess(event);


  }

  MerchantEventProcess(event: any): void {
    if (event.value == this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Merchant_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Merchant_Selected = 0;
    } else if (event.value != this.TerminalsList_Filter_Merchant_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreId",
        this._HelperService.AppConfig.DataType.Number,
        this.TerminalsList_Filter_Merchant_Selected,
        "="
      );
      this.TerminalsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.TerminalsList_Config.SearchBaseConditions
      );
      this.TerminalsList_Filter_Merchant_Selected = event.value;
      this.TerminalsList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreId",
          this._HelperService.AppConfig.DataType.Number,
          this.TerminalsList_Filter_Merchant_Selected,
          "="
        )
      );
    }
    this.TerminalsList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }



  SetOtherFilters(): void {
    this.TerminalsList_Config.SearchBaseConditions = [];
    this.TerminalsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Merchant));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Merchant_Selected = null;
      this.MerchantEventProcess(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Provider));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Provider_Selected = null;
      this.ProvderEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }

    CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Terminal.Store));
    if (CurrentIndex != -1) {
      this.TerminalsList_Filter_Store_Selected = null;
      this.StoreEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.TerminalsList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);

    this.SetOtherFilters();

    this.TerminalsList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Terminal,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
        this.TerminalsList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.TerminalsList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#TerminalsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TerminalsList_fdropdown").dropdown('toggle');
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TerminalsList_Config);
    this.SetOtherFilters();

    this.TerminalsList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.TerminalsList_Filter_Providers_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }






  //#region provider 

  public ProvidersList_Option: Select2Options;
  public ProvidersList_Selected = 0;
  ProvidersList_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }
      ]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.ProvidersList_Option = {
      placeholder: 'Select PTSP',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  ProvidersList_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        ProviderKey: event.data[0].ReferenceKey,
        ProviderId: event.data[0].ReferenceId,
        ProviderName: event.data[0].DisplayName,


      }
    );
    // console.log("Name", event.data[0]);
  }

  //#endregion

  //#region branch 

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      // SearchCondition: "",
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }



    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }
  GetBranches_ListChange(event: any) {
    // alert(event);\
    this.Form_AddUser.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId

      }
    );

  }

  //#endregion

  //#region manager 

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }
  GetMangers_ListChange(event: any) {
    // alert(event);
    this.Form_AddUser.patchValue(
      {
        RmKey: event.data[0].text,
        RmId: event.data[0].ReferenceId
      }
    );
  }

  //#endregion

  //#endregion

  //#region Terminals Count 



  GetTerminalCount() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverview',
      StartTime: new Date(2017, 0, 1, 0, 0, 0, 0),
      EndDate: moment().endOf('day'),
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      SubAccountId: this._HelperService.AppConfig.ActiveReferenceId,
      SubAccountKey: this._HelperService.AppConfig.ActiveReferenceKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AOverview = _Response.Result;
          // console.log(this._AOverview, "accountoverview");
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  //#endregion

  public ResetFilterControls: boolean = true;
  // ResetFilterUI(): void {
  //   this.ResetFilterControls = false;
  //   this._ChangeDetectorRef.detectChanges();

  //   this.TerminalsList_Filter_Stores_Load();
  //   this.TerminalsList_Filter_Providers_Load();
  //   this.TerminalsList_Setup();

  //   this.ResetFilterControls = true;
  //   this._ChangeDetectorRef.detectChanges();

  // }




  // GetOverviews(ListOptions: any, Task: string): any {
  //   this._HelperService.IsFormProcessing = true;

  //   ListOptions.SearchCondition = '';
  //   ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
  //   if (ListOptions.ActivePage == 1) {
  //     ListOptions.RefreshCount = true;
  //   }
  //   var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

  //   if (ListOptions.Sort.SortDefaultName) {
  //     ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
  //     ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
  //     ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
  //   }

  //   if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
  //     if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
  //       SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
  //     }
  //     else {
  //       SortExpression = ListOptions.Sort.SortColumn + ' desc';
  //     }
  //   }


  //   var pData = {
  //     Task: Task,
  //     TotalRecords: ListOptions.TotalRecords,
  //     Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
  //     Limit: ListOptions.PageRecordLimit,
  //     RefreshCount: ListOptions.RefreshCount,
  //     SearchCondition: ListOptions.SearchCondition,
  //     SortExpression: SortExpression,
  //     Type: ListOptions.Type,
  //     ReferenceKey: ListOptions.ReferenceKey,
  //     StartDate: ListOptions.StartDate,
  //     EndDate: ListOptions.EndDate,
  //     ReferenceId: ListOptions.ReferenceId,
  //     SubReferenceId: ListOptions.SubReferenceId,
  //     SubReferenceKey: ListOptions.SubReferenceKey,
  //     AccountId: ListOptions.AccountId,
  //     AccountKey: ListOptions.AccountKey,
  //     ListType: ListOptions.ListType,
  //     IsDownload: false,
  //   };

  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._AOverview.TerminalStatus = _Response.Result.Data as any;
  //       }
  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);

  //     });


  // }



}
