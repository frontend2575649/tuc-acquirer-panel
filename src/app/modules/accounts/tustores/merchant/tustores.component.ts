import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, FilterHelperService, OList, OResponse, OSelect } from "../../../../service/service";
declare var $: any;
import swal from "sweetalert2";
import { HCoreXAddress, HCXAddressConfig, locationType } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';

@Component({
  selector: "tu-stores",
  templateUrl: "./tustores.component.html"
})
export class TUStoresComponent implements OnInit {
  public SaveAccountRequest: any;


  _CurrentAddress: any = {};

  public _Address: HCoreXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form
    }
  AddressChange(Address) {
    this._Address = Address;
  }

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.ShowDateRange = false;
  }
  ngOnInit() {

    Feather.replace();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
      } else {

        //#region DropdownInit 

        this.StoresList_Filter_Owners_Load();
        this.Form_AddStore_Load();
        this.GetBranches_List();
        this.GetMangers_List();

        //#endregion

        this.StoresList_Setup();

      }
    });
  }

  //#region Stores 

  public StoresList_Config: OList;
  StoresList_Setup() {


    var SearchCondition = undefined;
    if (this._HelperService.AppConfig.ActiveReferenceId != 0 && this._HelperService.AppConfig.ActiveReferenceKey != undefined && this._HelperService.AppConfig.ActiveReferenceKey != null && this._HelperService.AppConfig.ActiveReferenceKey != "") {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '=');
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, "0", '>');
    }
    if (SearchCondition != undefined) {
      this.StoresList_Config = {
        Id: "StoresList",
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        SubReferenceId: this._HelperService.UserAccount.AccountId,
        SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Title: "All Stores",
        StatusType: "default",
        Type: this._HelperService.AppConfig.ListType.All,
        // DefaultSortExpression: 'CreateDate desc',
        TableFields: [
          {
            DisplayName: 'Store Name',
            SystemName: 'DisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'ContactNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Email Address',
            SystemName: 'EmailAddress',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'POS',
            SystemName: 'Terminals',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'R. %',
            SystemName: 'RewardPercentage',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '_600',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Tr',
            SystemName: 'LastTransactionDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Login',
            SystemName: 'LastLoginDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            IsDateSearchField: true,
            Sort: true,
            ResourceId: null,
          },
        ]
      };
    }
    else {
      this.StoresList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        Title: "All Stores",
        StatusType: "default",
        SubReferenceId: this._HelperService.UserAccount.AccountId,
        SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Type: this._HelperService.AppConfig.ListType.All,
        // DefaultSortExpression: 'CreateDate desc',
        TableFields: [
          {
            DisplayName: 'Name',
            SystemName: 'DisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'ContactNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Email Address',
            SystemName: 'EmailAddress',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Merchant',
            SystemName: 'OwnerDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Terminals',
            SystemName: 'Terminals',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'R. %',
            SystemName: 'RewardPercentage',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '_600',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Tr',
            SystemName: 'LastTransactionDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Login',
            SystemName: 'LastLoginDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },

          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            IsDateSearchField: true,
            ResourceId: null,
          },
        ]

      };
    }

    this.StoresList_Config = this._DataHelperService.List_Initialize_ForStores(
      this.StoresList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {


    if (event != null) {
      for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
        const element = this.StoresList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.StoresList_Config


    );

    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );

    if (
      (this.StoresList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.StoresList_GetData();
    }
  }
  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.StoresList_Config
    );
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);


  }

  //#endregion

  //#region AddStore 

  Form_AddStore: FormGroup;

  //#region AddStore Map 

  Form_AddStore_Address: string = null;
  Form_AddStore_Latitude: number = 0;
  Form_AddStore_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddStore_PlaceMarkerClick(event) {
    this.Form_AddStore_Latitude = event.coords.lat;
    this.Form_AddStore_Longitude = event.coords.lng;
  }
  public Form_AddStore_AddressChange(address: Address) {

    this.Form_AddStore_Latitude = address.geometry.location.lat();
    this.Form_AddStore_Longitude = address.geometry.location.lng();
    this.Form_AddStore_Address = address.formatted_address;

    this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
    this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    if (this._CurrentAddress.country != this._HelperService.UserCountrycode) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountrycode);
      this.reset();

    }
    else {
      // this.Form_AddStore.controls['Address'].setValue(address.formatted_address);
      this.Form_AddStore.controls['MapAddress'].setValue(address.formatted_address);
    }


  }


  reset() {
    //this.Form_AddStore.controls['Address'].reset()
    this.Form_AddStore.controls['MapAddress'].reset()
    this.Form_AddStore.controls['CityName'].reset()
    this.Form_AddStore.controls['StateName'].reset()
    this.Form_AddStore.controls['CountryName'].reset()

  }



  //#endregion

  Form_AddStore_Show() {
    this._HelperService.OpenModal("Form_AddStore_Content");
  }
  Form_AddStore_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_AddStore_Content");
  }
  Form_AddStore_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddStore = this._FormBuilder.group({
      OperationType: 'new',
      Task: this._HelperService.AppConfig.Api.Core.SaveStore,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      MerchantId: this._HelperService.AppConfig.ActiveReferenceId,
      MerchantKey: this._HelperService.AppConfig.ActiveReferenceKey,
      BranchId: [null, Validators.required],
      BranchKey: [null, Validators.required],
      RmId: [null, Validators.required],
      RmKey: [null, Validators.required],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],

      MapAddress: this.Form_AddStore_Address,
      StatusCode: this._HelperService.AppConfig.Status.Active,

      // Address: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      // Latitude: [null, Validators.compose([Validators.required])],
      // Longitude: [null, Validators.compose([Validators.required])],
      // CityName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(100)])],
      // StateName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      // CountryName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
    });
  }
  Form_AddStore_Clear() {
    this.Form_AddStore.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddStore_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddStore_Process(_FormValue: any) {

    if (!this._Address.Address == undefined || this._Address.Address == null || this._Address.Address == "") {
      this._HelperService.NotifyError('Please select business location');
    }
    else {

      _FormValue.MapAddress = this.Form_AddStore_Address;

      this._HelperService.IsFormProcessing = true;
      this.SaveAccountRequest = this.ReFormat_RequestBody();

      let _OResponse: Observable<OResponse>;
      var Request = this.CreateStoreRequest(_FormValue);

      // _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, _FormValue);
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, Request);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Store added');
            this.Form_AddStore_Clear();
            this.Form_AddStore_Load();
            this.Form_AddStore_Close();
            this.StoresList_Setup();
            this._HelperService.emitDetailsChangeEvent();
            if (_FormValue.OperationType == 'edit') {
              this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Store.Dashboard, _Response.Result.ReferenceKey]);
            }
            else if (_FormValue.OperationType == 'close') {
              this.Form_AddStore_Clear();
            }
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }
  //#endregion

  //#region Dropdowns 

  //#region Owners

  public StoresList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = 0;
  StoresList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: 'Sort by Owner',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Filter_Owners_Change(event: any) {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
    }

    this.StoresList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  //#region Branches 

  public GetBranches_Option: Select2Options;
  public BranchId_Selected: number = null;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    }



    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetBranches_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetBranches_Transport,
      multiple: false,
    };
  }


  // GetBranches_ListChange(event: any) {
  //   this.BranchId_Selected = event.data[0].ReferenceId
  //   this.Form_AddStore.patchValue(
  //     {
  //       BranchKey: event.data[0].ReferenceKey,
  //       BranchId: event.data[0].ReferenceId

  //     }
  //   );

  // }



  public ToggleManagerSelect: boolean = true
  GetBranches_ListChange(event: any) {
    // alert(event);\
    this.BranchId_Selected = event.data[0].ReferenceId
    this.Form_AddStore.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId

      }
    );
    this.ToggleManagerSelect = false;
    this.GetMangers_List();

    setTimeout(() => {
      this.ToggleManagerSelect = true;
      this.GetMangers_List();
    }, 500);


  }
  //#endregion

  //#region Managers 

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  // GetMangers_List() {
  //   debugger;
  //   console.log("branchid---",this.BranchId_Selected);
  //   var PlaceHolder = "Select Manager";
  //   var _Select: OSelect =
  //   {
  //     Task: this._HelperService.AppConfig.Api.Core.GetManagers,
  //     Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
  //     ReferenceKey: this._HelperService.UserAccount.AccountKey,
  //     ReferenceId: this._HelperService.UserAccount.AccountId,
  //     SearchCondition: "",
  //     SortCondition: [],
  //     Fields: [
  //       {
  //         SystemName: "ReferenceId",
  //         Type: this._HelperService.AppConfig.DataType.Number,
  //         Id: true,
  //         Text: false,
  //       },
  //       {
  //         SystemName: "Name",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: false,
  //         Text: true
  //       }
  //     ]
  //   }

  //   _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
  //   _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BranchId', this._HelperService.AppConfig.DataType.Text, this.BranchId_Selected, '=');
  //   this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
  //   this.GetMangers_Option = {
  //     placeholder: PlaceHolder,
  //     ajax: this.GetMangers_Transport,
  //     multiple: false,
  //   };
  // }

  GetMangers_List() {
    console.log("branchid--", this.BranchId_Selected)
    var PlaceHolder = "Select Manager";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetManagers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: "AccountTypeCode",
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: "=",
        //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
        // }
      ]
    }

    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'BranchId', this._HelperService.AppConfig.DataType.Text, this.BranchId_Selected, '=');
    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMangers_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMangers_Transport,
      multiple: false,
    };
  }



  GetMangers_ListChange(event: any) {
    // alert(event);
    this.Form_AddStore.patchValue(
      {
        RmKey: event.data[0].ReferenceKey,
        RmId: event.data[0].ReferenceId
      }
    );
  }

  //#endregion

  //#endregion

  public ResetFilterControls: boolean = true;
  // ResetFilterUI(): void {
  //   this.ResetFilterControls = false;
  //   this._ChangeDetectorRef.detectChanges();

  //   this.StoresList_Filter_Owners_Load();
  //   this.StoresList_Setup();

  //   this.ResetFilterControls = true;
  //   this._ChangeDetectorRef.detectChanges();
  // }



  //#region OwnerFilter

  public MerchantsList_Filter_Owners_Option: Select2Options;
  public MerchantsList_Filter_Owners_Selected = null;
  MerchantsList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.AccountType.Merchant
      ]
      , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: 'Sort by Owner',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  MerchantsList_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.StoresList_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
    }

    this.StoresList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.StoresList_Config.SearchBaseConditions = [];
    this.StoresList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.MerchantsList_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }


  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.StoresList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
    this.SetOtherFilters();
    this.StoresList_GetData();
  }


  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Store(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  //   Save_NewFilter() {
  //     swal({
  //         position: "center",
  //         title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
  //         text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
  //         // input: "text",
  //         html:
  //             '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
  //             '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
  //             '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
  //         focusConfirm: false,
  //         preConfirm: () => {
  //             return {
  //                 filter: document.getElementById('swal-input1')['value'],
  //                 private: document.getElementById('swal-input2')['checked']
  //             }
  //         },
  //         // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
  //         // inputAttributes: {
  //         //   autocapitalize: "off",
  //         //   autocorrect: "off",
  //         //   maxLength: "4",
  //         //   minLength: "4",
  //         // },
  //         animation: false,
  //         customClass: this._HelperService.AppConfig.Alert_Animation,
  //         showCancelButton: true,
  //         confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //         cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //         confirmButtonText: "Save",
  //         cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //     }).then((result) => {
  //         if (result.value) {

  //             if (result.value.filter.length < 5) {
  //                 this._HelperService.NotifyError('Enter filter name length greater than 4');
  //                 return;
  //             }

  //             this._HelperService._RefreshUI = false;
  //             this._ChangeDetectorRef.detectChanges();

  //             this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

  //             var AccessType: number = result.value.private ? 0 : 1;
  //             this._HelperService.Save_NewFilter(
  //                 this._HelperService.AppConfig.FilterTypeOption.Stores,
  //                 AccessType
  //             );

  //             this._HelperService._RefreshUI = true;
  //             this._ChangeDetectorRef.detectChanges();
  //         }
  //     });
  // }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetStoreConfig(this.StoresList_Config);
        this.StoresList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.StoresList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
    this.SetOtherFilters();

    this.StoresList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.MerchantsList_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }



  ReFormat_RequestBody(): void {
    var formValue: any = this.Form_AddStore.value;
    var formRequest: any = {
      OperationType: 'new',
      Task: formValue.Task,
      DisplayName: formValue.DisplayName,
      Name: formValue.Name,
      ContactNumber: formValue.ContactNumber,
      EmailAddress: formValue.EmailAddress,
      RewardPercentage: formValue.RewardPercentage,
      WebsiteUrl: formValue.WebsiteUrl,
      ReferralCode: formValue.ReferralCode,
      Description: formValue.Description,
      UserName: formValue.UserName,
      Password: formValue.Password,
      StatusCode: formValue.StatusCode,
      // ContactPerson: {
      //   FirstName: formValue.FirstName,
      //   LastName: formValue.LastName,
      //   MobileNumber: formValue.MobileNumber,
      //   EmailAddress: formValue.EmailAddress
      // },
      IconContent: formValue.IconContent
    };
    return formRequest;

  }


  CreateStoreRequest(_FormValue: any): void {
    // _FormValue.ContactPerson = {
    //   FirstName: _FormValue.FirstName,
    //   LastName: _FormValue.LastName,
    //   MobileNumber: _FormValue.MobileNumber,
    //   EmailAddress: _FormValue.MEmailAddress
    // }
    // //Location Manager - Start
    // _FormValue.Address = this._Address.Address;
    _FormValue.AddressComponent = this._Address;
    // //Location Manager - End

    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    // _FormValue.EmailAddress = undefined;

    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }


}
