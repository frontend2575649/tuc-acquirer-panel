import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import { DataHelperService, FilterHelperService, HelperService, OList, OSelect } from "../../../../service/service";
declare var $: any;

@Component({
  selector: "tu-stores",
  templateUrl: "./tustores.component.html",
})
export class TUStoresComponent implements OnInit {
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) { }
  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();
    //this.InitColumnConfig();

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (
        this._HelperService.AppConfig.ActiveReferenceKey == null ||
        this._HelperService.AppConfig.ActiveReferenceId == null
      ) {
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
      } else {
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
      }
    });
  }
  public ResetFilterControls: boolean = true;
  //#region ColumnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Merchant Contact",
      Value: true,
    },

    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];
  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Merchant Contact",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  // InitColumnConfig() {
  //   var temp = this._HelperService.GetStorage("BStoreTable");
  //   if (temp != undefined && temp != null) {
  //     this.ColumnConfig = temp.config;
  //     this.TempColumnConfig = JSON.parse(JSON.stringify(temp.config));
  //   }
  // }

  // EditColumnConfig() {
  //   this._HelperService.OpenModal("EditCol");
  // }
  // SaveColumnConfig() {
  //   this.ColumnConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
  //   this._HelperService.SaveStorage("BStoreTable", {
  //     config: this.ColumnConfig,
  //   });
  //   this._HelperService.CloseModal("EditCol");
  // }


  EditColConfig() {
    this._HelperService.OpenModal('EditCol');
  }
  EditTempColConfig() {
    this.ColumnConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
    this._HelperService.SaveStorage('ManagerTable', { config: this.ColumnConfig });
    this._HelperService.CloseModal('EditCol');
  }



  SetOtherFilters(): void {
    this.StoresList_Config.SearchBaseConditions = [];
    this.StoresList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Stores.Owner));
    if (CurrentIndex != -1) {
      this.StoresList_Filter_Owners_Selected = null;
      this.OwnersEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region Stores

  public StoresList_Config: OList;
  StoresList_Setup() {
    var SearchCondition = undefined;
    if (
      this._HelperService.AppConfig.ActiveReferenceId != 0 &&
      this._HelperService.AppConfig.ActiveReferenceKey != undefined &&
      this._HelperService.AppConfig.ActiveReferenceKey != null &&
      this._HelperService.AppConfig.ActiveReferenceKey != ""
    ) {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        SearchCondition,
        "OwnerId",
        this._HelperService.AppConfig.DataType.Number,
        this._HelperService.AppConfig.ActiveReferenceId,
        "="
      );
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        SearchCondition,
        "OwnerKey",
        this._HelperService.AppConfig.DataType.Text,
        this._HelperService.AppConfig.ActiveReferenceKey,
        "="
      );
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(
        SearchCondition,
        "OwnerId",
        this._HelperService.AppConfig.DataType.Number,
        "0",
        ">"
      );
    }
    if (SearchCondition != undefined) {
      this.StoresList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
        ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
        Title: "All Stores",
       // StatusType: "default",
       StatusType:"storedefault",
        Type: this._HelperService.AppConfig.ListType.All,
        DefaultSortExpression: "CreateDate desc",
        // SearchBaseCondition: SearchCondition,
        TableFields: [
          {
            DisplayName: "Store Name",
            SystemName: "DisplayName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Merchant",
            SystemName: "MerchantDisplayName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Contact No",
            SystemName: "ContactNumber",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "Email Address",
            SystemName: "EmailAddress",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "POS",
            SystemName: "Terminals",
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "R. %",
            SystemName: "RewardPercentage",
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: "_600",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Today Tr",
            SystemName: "TodaysTransactionAmount",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "Last Tr",
            SystemName: "LastTransactionDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "Last Login",
            SystemName: "LastLoginDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },

          {
            DisplayName: this._HelperService.AppConfig.CommonResource
              .CreateDate,
            SystemName: "CreateDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: true,
            Search: false,
            Sort: true,
            IsDateSearchField: true,
            ResourceId: null,
          },
        ],
      };
    } else {
      this.StoresList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
        Title: "All Stores",
        StatusType: "default",
        Type: this._HelperService.AppConfig.ListType.All,
        DefaultSortExpression: "CreateDate desc",
        TableFields: [
          {
            DisplayName: "Name",
            SystemName: "DisplayName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Contact No",
            SystemName: "ContactNumber",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "Email Address",
            SystemName: "EmailAddress",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: "Merchant",
            SystemName: "OwnerDisplayName",
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Terminals",
            SystemName: "Terminals",
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: "",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "R. %",
            SystemName: "RewardPercentage",
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: "_600",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Last Tr",
            SystemName: "LastTransactionDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: "Last Login",
            SystemName: "LastLoginDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },

          {
            DisplayName: this._HelperService.AppConfig.CommonResource
              .CreateDate,
            SystemName: "CreateDate",
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: "td-date",
            Show: true,
            Search: false,
            Sort: true,
            IsDateSearchField: true,
            ResourceId: null,
          },
        ],
      };
    }

    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.MerchantSales,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
        const element = this.StoresList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(event, Type, this.StoresList_Config);
    this.StoresList_Config = this._DataHelperService.List_Operations(this.StoresList_Config, event, Type);
    if ((this.StoresList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
      this.StoresList_GetData();
    }
  }
  timeout = null;
  StoresList_ToggleOptionSearch(event: any, Type: any) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
          const element = this.StoresList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }
      this._HelperService.Update_CurrentFilterSnap(event, Type, this.StoresList_Config);
      this.StoresList_Config = this._DataHelperService.List_Operations(this.StoresList_Config, event, Type);
      if ((this.StoresList_Config.RefreshData == true) && this._HelperService.DataReloadEligibility(Type)) {
        this.StoresList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(this.StoresList_Config);
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }



  //#region Owner_Filter

  public StoresList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = 0;
  StoresList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };

    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: "Sort by Owner",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Filter_Owners_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.StoresList_Config,
      this._HelperService.AppConfig.OtherFilters.Stores.Owner
    );

    this.OwnersEventProcessing(event);

  }

  OwnersEventProcessing(event: any): void {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantId",
        this._HelperService.AppConfig.DataType.Number,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.StoresList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = 0;
    } else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "MerchantId",
        this._HelperService.AppConfig.DataType.Number,
        this.StoresList_Filter_Owners_Selected,
        "="
      );
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.StoresList_Config.SearchBaseConditions
      );
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "MerchantId",
          this._HelperService.AppConfig.DataType.Number,
          this.StoresList_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.StoresList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }



  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.StoresList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();


    this.StoresList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        //maxLength: "4",
        minLength: "4",
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Store(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Stores
        );
        this._FilterHelperService.SetStoreConfig(this.StoresList_Config);
        this.StoresList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();

    if (ButtonType == 'Sort') {
      $("#StoresList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#StoresList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    this.StoresList_GetData();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetStoreConfig(this.StoresList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();


    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    this.StoresList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.StoresList_Config);

    //#region setOtherFilters

    this.SetOtherFilters();



    this.StoresList_GetData();
  }



  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.StoresList_Filter_Owners_Load();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }
}
