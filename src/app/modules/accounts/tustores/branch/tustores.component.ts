import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
declare var $: any;

import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon,
  FilterHelperService
} from "../../../../service/service";
import swal from "sweetalert2";

import * as Feather from 'feather-icons';

@Component({
  selector: "tu-stores",
  templateUrl: "./tustores.component.html"
})
export class TUStoresComponent implements OnInit {

  Show: {
    ListShow: string;
    IconShow: string;
  }

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this.Show = {
      ListShow: 'none',
      IconShow: 'block'
    }
  }
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this.StoresList_Filter_Owners_Load();
        this.MerchantsList_Filter_Owners_Load();
        this.StoresList_Setup();
      } else {
        // this._HelperService.Get_UserAccountDetails(true);
        this.StoresList_Filter_Owners_Load();
        this.MerchantsList_Filter_Owners_Load();
        this.StoresList_Setup();
      }
    });
  }

  //#region Stores 

  public StoresList_Config: OList;
  StoresList_Setup() {

    this.StoresList_Config = {
      Id: "StoresList",
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SubReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      SubReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      Title: "All Stores",
      StatusType: "default",
      Type: this._HelperService.AppConfig.ListType.Branch,
      DefaultSortExpression: 'CreateDate desc',
      TableFields: [
        {
          DisplayName: 'Name',
          SystemName: 'DisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'Contact No',
          SystemName: 'ContactNumber',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Email Address',
          SystemName: 'EmailAddress',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: '',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Terminals',
          SystemName: 'Terminals',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '',
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
        },
        {
          DisplayName: 'R. %',
          SystemName: 'RewardPercentage',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: '_600',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Tr',
          SystemName: 'LastTransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: 'Last Login',
          SystemName: 'LastLoginDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: 'CreateDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: 'td-date',
          Show: true,
          Search: false,
          Sort: true,
          IsDateSearchField:true,
          ResourceId: null,
        },
      ]

    };


    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Merchant,
      this.StoresList_Config
    );

    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
        const element = this.StoresList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.StoresList_Config


    );

    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );

    if (
      (this.StoresList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.StoresList_GetData();
    }
  }
  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.StoresList_Config, this.Show
    );
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
 
 
  }

  //#endregion

  //#region Owners_Dropdown 

  public StoresList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = 0;
  StoresList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: 'Sort by Owner',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Filter_Owners_Change(event: any) {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
    }

    this.StoresList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }




  // MerchantsList_Filter_Owners_Change(event: any) {
  //   this._HelperService.Update_CurrentFilterSnap(
  //     event,
  //     this._HelperService.AppConfig.ListToggleOption.Other,
  //     this.StoresList_Config,
  //     this._HelperService.AppConfig.OtherFilters.Merchant.Owner
  //   );

  //   this.OwnerEventProcessing(event);

  // }

  // OwnerEventProcessing(event: any): void {
  //   if (event.value == this.StoresList_Filter_Owners_Selected) {
  //     var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
  //     this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
  //     this.StoresList_Filter_Owners_Selected = 0;
  //   }
  //   else if (event.value != this.StoresList_Filter_Owners_Selected) {
  //     var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
  //     this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
  //     this.StoresList_Filter_Owners_Selected = event.value;
  //     this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
  //   }
  //   this.StoresList_ToggleOption(
  //     null,
  //     this._HelperService.AppConfig.ListToggleOption.ResetOffset
  //   );
  // }

  // Delete_Filter() {
  //   this._HelperService._RefreshUI = false;
  //   this._ChangeDetectorRef.detectChanges();

  //   this._HelperService.Delete_Filter(
  //     this._HelperService.AppConfig.FilterTypeOption.Stores
  //   );
  //   this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
  //   this.StoresList_GetData();

  //   this._HelperService._RefreshUI = true;
  //   this._ChangeDetectorRef.detectChanges();
  // }

  //#endregion

  public ResetFilterControls: boolean = true;
  // ResetFilterUI(): void {
  //   this.ResetFilterControls = false;
  //   this._ChangeDetectorRef.detectChanges();
  //   this.StoresList_Filter_Owners_Load();
  //   this.ResetFilterControls = true;
  //   this._ChangeDetectorRef.detectChanges();

  // }

  
  // ApplyFilters(event: any, Type: any, ButtonType: any): void {
  //   this._HelperService.MakeFilterSnapPermanent();
  //   this.StoresList_Setup();

  //   if (ButtonType == 'Sort') {
  //     $("#StoresList_sdropdown").dropdown('toggle');
  //   } else if (ButtonType == 'Other') {
  //     $("#StoresList_fdropdown").dropdown('toggle');
  //   }

  //   this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  // }


  // ResetFilters(event: any, Type: any): void {
  //   this._HelperService.ResetFilterSnap();
  //   this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
  //   // this.SetOtherFilters();

  //   this.StoresList_Setup();

  //   this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  // }





   //#region OwnerFilter

   public MerchantsList_Filter_Owners_Option: Select2Options;
   public MerchantsList_Filter_Owners_Selected = null;
   MerchantsList_Filter_Owners_Load() {
     var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
     var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
     };
     // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
     //   [
     //     this._HelperService.AppConfig.AccountType.Merchant,
     //     this._HelperService.AppConfig.AccountType.Acquirer,
     //     this._HelperService.AppConfig.AccountType.PGAccount,
     //     this._HelperService.AppConfig.AccountType.PosAccount
     //   ]
     //   , '=');
     var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
     this.MerchantsList_Filter_Owners_Option = {
       placeholder: "Sort by Referrer",
       ajax: _Transport,
       multiple: false,
       allowClear: true,
     };
   }
   MerchantsList_Filter_Owners_Change(event: any) {
     this._HelperService.Update_CurrentFilterSnap(
       event,
       this._HelperService.AppConfig.ListToggleOption.Other,
       this.StoresList_Config,
       this._HelperService.AppConfig.OtherFilters.Merchant.Owner
     );
 
     this.OwnerEventProcessing(event);
 
   }
 
   OwnerEventProcessing(event: any): void {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
    }
 
     this.StoresList_ToggleOption(
       null,
       this._HelperService.AppConfig.ListToggleOption.ResetOffset
     );
   }
 
   //#endregion
 
   SetOtherFilters(): void {
     this.StoresList_Config.SearchBaseConditions = [];
     this.StoresList_Config.SearchBaseCondition = null;
 
     var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
     if (CurrentIndex != -1) {
       this.MerchantsList_Filter_Owners_Selected = null;
       this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
     }
   }
 
 
   //#region filterOperations
 
   Active_FilterValueChanged(event: any) {
     this._HelperService.Active_FilterValueChanged(event);
     this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
 
     //#region setOtherFilters
     this.SetOtherFilters();
     //#endregion
 
     this.StoresList_GetData();
   }
 
   RemoveFilterComponent(Type: string, index?: number): void {
     this._FilterHelperService._RemoveFilter_Merchant(Type, index);
     this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
     this.SetOtherFilters();
     this.StoresList_GetData();
   }
 
  
 
  //  Save_NewFilter() {
  //    swal({
  //      position: "center",
  //      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
  //      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
  //      input: "text",
  //      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
  //      inputAttributes: {
  //        autocapitalize: "off",
  //        autocorrect: "off",
  //        //maxLength: "4",
  //        minLength: "4",
  //      },
  //      animation: false,
  //      customClass: this._HelperService.AppConfig.Alert_Animation,
  //      showCancelButton: true,
  //      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
  //      confirmButtonText: "Save",
  //      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
  //    }).then((result) => {
  //      if (result.value) {
  //        this._HelperService._RefreshUI = false;
  //        this._ChangeDetectorRef.detectChanges();
 
  //        this._FilterHelperService._BuildFilterName_Merchant(result.value);
  //        this._HelperService.Save_NewFilter(
  //          this._HelperService.AppConfig.FilterTypeOption.Merchant
  //        );
 
  //        this._HelperService._RefreshUI = true;
  //        this._ChangeDetectorRef.detectChanges();
  //      }
  //    });
  //  }

  Save_NewFilter() {
    swal({
        position: "center",
        title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
        text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
        // input: "text",
        html:
            '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
            '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
            '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
        focusConfirm: false,
        preConfirm: () => {
            return {
                filter: document.getElementById('swal-input1')['value'],
                private: document.getElementById('swal-input2')['checked']
            }
        },
        // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
        // inputAttributes: {
        //   autocapitalize: "off",
        //   autocorrect: "off",
        //   maxLength: "4",
        //   minLength: "4",
        // },
        animation: false,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        showCancelButton: true,
        confirmButtonColor: this._HelperService.AppConfig.Color_Green,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: "Save",
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
        if (result.value) {

            if (result.value.filter.length < 5) {
                this._HelperService.NotifyError('Enter filter name length greater than 4');
                return;
            }

            this._HelperService._RefreshUI = false;
            this._ChangeDetectorRef.detectChanges();

            this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

            var AccessType: number = result.value.private ? 0 : 1;
            this._HelperService.Save_NewFilter(
                this._HelperService.AppConfig.FilterTypeOption.Stores,
                AccessType
            );

            this._HelperService._RefreshUI = true;
            this._ChangeDetectorRef.detectChanges();
        }
    });
}
 
   Delete_Filter() {
 
     swal({
       position: "center",
       title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
       text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
       animation: false,
       customClass: this._HelperService.AppConfig.Alert_Animation,
       showCancelButton: true,
       confirmButtonColor: this._HelperService.AppConfig.Color_Red,
       cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
       confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
       cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel
 
     }).then((result) => {
       if (result.value) {
         this._HelperService._RefreshUI = false;
         this._ChangeDetectorRef.detectChanges();
 
         this._HelperService.Delete_Filter(
           this._HelperService.AppConfig.FilterTypeOption.Merchant
         );
         this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
         this.StoresList_GetData();
 
         this._HelperService._RefreshUI = true;
         this._ChangeDetectorRef.detectChanges();
       }
     });
 
   }
 
   ApplyFilters(event: any, Type: any, ButtonType: any): void {
     this._HelperService.MakeFilterSnapPermanent();
     this.StoresList_GetData();
 
     if (ButtonType == 'Sort') {
       $("#MerchantsList_sdropdown").dropdown('toggle');
     } else if (ButtonType == 'Other') {
       $("#MerchantsList_fdropdown").dropdown('toggle');
     }
 
     this.ResetFilterUI(); this._HelperService.StopClickPropogation();
   }
 
   ResetFilters(event: any, Type: any): void {
     this._HelperService.ResetFilterSnap();
     this._FilterHelperService.SetMerchantConfig(this.StoresList_Config);
     this.SetOtherFilters();
 
     this.StoresList_GetData();
 
     this.ResetFilterUI(); this._HelperService.StopClickPropogation();
   }
 
   //#endregion
 
   ResetFilterUI(): void {
     this.ResetFilterControls = false;
     this._ChangeDetectorRef.detectChanges();
 
     this.MerchantsList_Filter_Owners_Load();
 
     this.ResetFilterControls = true;
     this._ChangeDetectorRef.detectChanges();
   }


}
