import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { AgmOverlays } from "agm-overlays"

import { TUStoreComponent } from "./tustore.component";
const routes: Routes = [
  {
    path: "",
    component: TUStoreComponent,
    children: [
      {
        path: "/:referencekey",
        data: {
          permission: "getmerchant",
          menuoperations: "ManageMerchant",
          accounttypecode: "merchant"
        },
        loadChildren:
          "../tustore/dashboard/tudashboard.module#TUStoreDashboardModule"
      },
      { path: "dashboard/:referencekey", data: { permission: "getappuser", menuoperations: "ManageAppUser", accounttypecode: "merchant" }, loadChildren: "../tustore/dashboard/tudashboard.module#TUStoreDashboardModule" },

      { path: "terminals/:referencekey", data: { permission: "terminals", PageName: "System.Menu.Terminals" }, loadChildren: "../../accounts/tuterminals/store/tuterminals.module#TUTerminalsModule" },
      { path: 'customers/:referencekey/:referenceid', data: { 'permission': 'rewardhistory', PageName: 'System.Menu.Stores' }, loadChildren: '../../../modules/accounts/tuappusers/store/tuappusers.module#TUAppUsersModule' },

      { path: 'rewardhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardsHistory' }, loadChildren: '../../../modules/transactions/tureward/store/tureward.module#TURewardsModule' },
      { path: 'rewardclaimhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RewardClaimHistory' }, loadChildren: '../../../modules/transactions/turewardclaim/store/turewardclaim.module#TURewardsClaimModule' },
      { path: 'redeemhistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.RedeemHistory' }, loadChildren: '../../../modules/transactions/turedeem/store/turedeem.module#TURedeemModule' },
      { path: 'saleshistory/:referencekey', data: { 'permission': 'merchants', PageName: 'System.Menu.SalesHistory' }, loadChildren: '../../../modules/transactions/tusale/store/tusale.module#TUSaleModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUStoreRoutingModule { }

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    TUStoreRoutingModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    AgmOverlays,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
    })
  ],
  declarations: [TUStoreComponent]
})
export class TUStoreModule { }
