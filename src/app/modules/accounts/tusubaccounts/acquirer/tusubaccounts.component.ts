import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
declare var $: any;
declare var moment: any;
import {
    OSelect,
    DataHelperService,
    HelperService,
    OResponse,
    OStorageContent,
    OCoreParameter,
    OCoreCommon,
    FilterHelperService
} from "../../../../service/service";
import swal from "sweetalert2";

@Component({
    selector: "tu-tusubaccounts",
    templateUrl: "./tusubaccounts.component.html"
})
export class TUSubAccountsComponent implements OnInit {

    public ResetFilterControls: boolean = true;
    public _Roles = [
        {
            'id': 0,
            'text': 'Select Roles',
            'apival': 'Select Roles'
        },
        {
            'id': 6,
            'text': 'Manager',
            'apival': 'manager'
        },
        // {
        //     'id': 7,
        //     'text': 'Administratior/Sub Account',
        //     'apival': 'admin'
        // },
        {
            'id': 8,
            'text': 'RM',
            'apival': 'rm'
        }
    ];
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _FilterHelperService: FilterHelperService
    ) {
        this._HelperService.ShowDateRange = false;
    }
    ngOnInit() {
        Feather.replace();
        this.GetMangers_List();
        this.GetBranches_List();
        this.UserAccounts_Setup();
        this.UserAccounts_Filter_Owners_Load();
        this.Form_AddUser_Load();

        var temp = this._HelperService.GetStorage('ManagerTable');
        if (temp != undefined && temp != null) {
            this.ColoumConfig = temp.config;
            this.TempColumnConfig = JSON.parse(JSON.stringify(temp.config));
        }
    }


    //#region ColumnConfg 

    TempColumnConfig: any = [

        {
            Name: 'Status',
            Value: true
        },
        {
            Name: 'City',
            Value: true
        },
        {
            Name: 'Contact',
            Value: true
        },

        {
            Name: 'Designation',
            Value: true
        },
        {
            Name: 'Reporting To	',
            Value: true
        },
        {
            Name: 'POS',
            Value: true
        },

        {
            Name: 'Stores',
            Value: true
        },
        {
            Name: 'Added',
            Value: true
        },
    ]
    ColoumConfig: any = [
        {
            Name: 'Status',
            Value: true
        },
        {
            Name: 'City',
            Value: true
        },
        {
            Name: 'Contact',
            Value: true
        },

        {
            Name: 'Designation',
            Value: true
        },
        {
            Name: 'Reporting To	',
            Value: true
        },
        {
            Name: 'POS',
            Value: true
        },

        {
            Name: 'Stores',
            Value: true
        },
        {
            Name: 'Added',
            Value: true
        },
    ]

    EditColConfig() {
        this._HelperService.OpenModal('EditColConfig');
    }
    EditTempColConfig() {
        this.ColoumConfig = JSON.parse(JSON.stringify(this.TempColumnConfig));
        this._HelperService.SaveStorage('ManagerTable', { config: this.ColoumConfig });
        this._HelperService.CloseModal('EditColConfig');
    }

    //#endregion

    SetOtherFilters(): void {
        this.UserAccounts_Config.SearchBaseConditions = [];
        this.UserAccounts_Config.SearchBaseCondition = null;

        var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Stores.Owner));
        if (CurrentIndex != -1) {
            this.UserAccounts_Filter_Owners_Selected = null;
            this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    public UserAccounts_Config: OList;
    UserAccounts_Setup() {
        this.UserAccounts_Config = {
            Id: null,
            // Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            // Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Task: this._HelperService.AppConfig.Api.Core.GetManagers,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            Title: "Managers",
            StatusType: "default",
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'Name',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'Branch Name',
                    SystemName: 'BranchName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '_600',
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'MobileNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Email Address',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Stores',
                    SystemName: 'Stores',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Owner',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: 'Designation',
                    SystemName: 'RoleName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: 'Last Tr',
                    SystemName: 'LastTransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Last Login',
                    SystemName: 'LastLoginDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: false,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    IsDateSearchField:true,
                    Sort: true,
                },
            ]
        };

        this.UserAccounts_Config = this._DataHelperService.List_Initialize(this.UserAccounts_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.Manager,
            this.UserAccounts_Config
        );

        this.UserAccounts_GetData();
    }
    UserAccounts_ToggleOption(event: any, Type: any) {
        if (event != null) {
            for (let index = 0; index < this.UserAccounts_Config.Sort.SortOptions.length; index++) {
                const element = this.UserAccounts_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {

                    element.SystemActive = true;

                }
                else {
                    element.SystemActive = false;

                }
            }
        }
        this._HelperService.Update_CurrentFilterSnap(
            event,
            Type,
            this.UserAccounts_Config
        );
        this.UserAccounts_Config = this._DataHelperService.List_Operations(this.UserAccounts_Config, event, Type);
        if (
            (this.UserAccounts_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.UserAccounts_GetData();
        }
    }
    UserAccounts_GetData() {
        // debugger;
        var TConfig = this._DataHelperService.List_GetData(this.UserAccounts_Config);
        this.UserAccounts_Config = TConfig;
        // console.log(this.UserAccounts_Config,"asdds");
    }
    // UserAccounts_ListTypeChange(Type) {
    //     if (Type == 1) {
    //         this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '==');
    //     }
    //     else {
    //         this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 1, '!=');
    //         this.UserAccounts_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '!=');
    //     }
    //     this.UserAccounts_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    // }
    UserAccounts_RowSelected(ReferenceData) {
        var Details =
        {
            ReferenceId: ReferenceData.ReferenceId,
            ReferenceKey: ReferenceData.ReferenceKey,
            DisplayName: ReferenceData.DisplayName,
        };

        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveManager, {
            "ReferenceKey": ReferenceData.ReferenceKey,
            "ReferenceId": ReferenceData.ReferenceId,
            "DisplayName": ReferenceData.DisplayName,
            "AccountTypeCode": this._HelperService.AppConfig.AccountType.Manager
        });

        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveMerchant, Details);
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Manager.Terminals, ReferenceData.ReferenceKey, ReferenceData.ReferenceId, ReferenceData.RoleId]);
    }

    public UserAccounts_Filter_Owners_Option: Select2Options;
    public UserAccounts_Filter_Owners_Selected = 0;
    UserAccounts_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
        //     [
        //         this._HelperService.AppConfig.AccountType.Merchant,
        //         this._HelperService.AppConfig.AccountType.Acquirer,
        //         this._HelperService.AppConfig.AccountType.PGAccount,
        //         this._HelperService.AppConfig.AccountType.PosAccount
        //     ]
        //     , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.UserAccounts_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    UserAccounts_Filter_Owners_Change(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.UserAccounts_Config,
            this._HelperService.AppConfig.OtherFilters.Manager.Owner
        );

        this.OwnerEventProcessing(event);
    }

    OwnerEventProcessing(event: any): void {
        if (event.value == this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.UserAccounts_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.UserAccounts_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.UserAccounts_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '=');
            this.UserAccounts_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.UserAccounts_Config.SearchBaseConditions);
            this.UserAccounts_Filter_Owners_Selected = event.value;
            this.UserAccounts_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'BranchId', this._HelperService.AppConfig.DataType.Number, this.UserAccounts_Filter_Owners_Selected, '='));
        }

    }


    //#region Add_User 

    Form_AddUser: FormGroup;
    Form_AddUser_Show() {
        this._HelperService.OpenModal("Form_AddUser_Content");
    }
    Form_AddUser_Close() {
        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.SubAccounts
        // ]);
      //  this.Form_AddUser_Clear();
        this._HelperService.OpenModal("Form_AddUser_Content");
        this.Form_AddUser_Clear();
        
        //S2GetManagers.value = null;
      
    }
    Form_AddUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_AddUser = this._FormBuilder.group({
            OperationType: "new",
            Task: this._HelperService.AppConfig.Api.Core.SaveManager,
            OwnerKey: ["0", Validators.required],
            OwnerId: ["0", Validators.required],
            // BranchKey: [
            //     "wwdwddwdw",
            //     Validators.compose([
            //         Validators.required,
            //         Validators.minLength(2),
            //         Validators.maxLength(128)
            //     ])
            // ],
            RoleKey: [null, Validators.required],
            RoleId: [null, Validators.required],
            BranchKey: [null, Validators.required],
            BranchId: [null, Validators.required],
            S2GetManagers:[null], 
            S2GetRoles:[null],
            S2GetBranches:[null],
            Name: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(128)
                ])
            ],
            MobileNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(10),
                    Validators.maxLength(14)
                ])
            ],
            EmailAddress: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.email,
                    Validators.minLength(2)
                ])
            ],
            StatusCode: this._HelperService.AppConfig.Status.Active,

        });
    }
    Form_AddUser_Clear() {
        // this.Form_AddUser = this._FormBuilder.group({
        //     S2GetManagers:null, 
        //     S2GetRoles:null,
        //     S2GetBranches:null,

        // })
      //  document.getElementById("S2GetManagers").innerHTML = ""
        this.Form_AddUser.reset();
        this.Form_AddUser_Load();
    }

    Form_AddUser_Process(_FormValue: any) {
        this.ResetFilterUI();
        // _FormValue.DisplayName = _FormValue.FirstName;
        // _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            _FormValue
        );
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.NotifySuccess("Account created successfully");
                    this.Form_AddUser_Clear();
                    this._HelperService.CloseModal('Form_AddUser_Content');
                    this.UserAccounts_Setup();

                    if (_FormValue.OperationType == "close") {
                        this.Form_AddUser_Close();
                    }
                } else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#endregion

    //#region Dropdowns 

    public GetRoles_Option: Select2Options;
    public GetRoles_Transport: any;

    GetRoles_ListChange(event: any) {

        this.Form_AddUser.controls['OwnerId'].setValue(null);
        this.Form_AddUser.controls['OwnerKey'].setValue(null);

        this.Form_AddUser.patchValue(
            {
                RoleId: event.value,
                RoleKey: event.data[0].apival
            }
        );
        this._ShowRole = false;
        this._ChangeDetectorRef.detectChanges();

        if (event.value == 8) { //RM
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 6; //Manager

            this.GetMangers_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();
        } else if (event.value == 7) { //SubAccount

            this._ShowReportingM = false;

            this.Form_AddUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
            this.Form_AddUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);


            this._MangerTypeId = undefined;
        }
        else if (event.value == 6) { //Manager
            this._ShowReportingM = false;
            this._ChangeDetectorRef.detectChanges();
            this._MangerTypeId = 7; //Subaccount
            this.GetMangers_List();
            this._ShowReportingM = true;
            this._ChangeDetectorRef.detectChanges();

        } else {
            this._MangerTypeId = undefined;
            this._ShowReportingM = true;
            this.GetMangers_List();
        }

        this._ShowRole = true;
        this._ChangeDetectorRef.detectChanges();

        this.GetMangers_List();
    }

    public _MangerTypeId: number;
    public _ShowRole: boolean = true;
    public _ShowReportingM: boolean = true;

    public GetMangers_Option: Select2Options;
    public GetMangers_Transport: any;
    GetMangers_List() {
        var PlaceHolder = "Select Manager";
        var _Select: OSelect

        if (this._MangerTypeId == 7) {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
                ReferenceKey: this._HelperService.UserAccount.AccountKey,
                ReferenceId: this._HelperService.UserAccount.AccountId,
                SortCondition: [],
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }
        } else {
            _Select =
            {
                Task: this._HelperService.AppConfig.Api.Core.GetManagers,
                Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
                ReferenceKey: this._HelperService.UserAccount.AccountKey,
                ReferenceId: this._HelperService.UserAccount.AccountId,
                SortCondition: [],
                Fields: [
                    {
                        SystemName: "ReferenceId",
                        Type: this._HelperService.AppConfig.DataType.Number,
                        Id: true,
                        Text: false,
                    },
                    {
                        SystemName: "Name",
                        Type: this._HelperService.AppConfig.DataType.Text,
                        Id: false,
                        Text: true
                    },
                ]
            }

        }


        if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
            _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
        }

        this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetMangers_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetMangers_Transport,
            multiple: false,
        };
    }
    GetMangers_ListChange(event: any) {
        // alert(event);

        this.Form_AddUser.patchValue(
            {
                OwnerId: event.data[0].ReferenceId,
                OwnerKey: event.data[0].ReferenceKey,
            }


        );

    }

    public GetBranches_Option: Select2Options;
    public GetBranches_Transport: any;
    GetBranches_List() {
        var PlaceHolder = "Select Branch";
        var _Select: OSelect =
        {
            Task: this._HelperService.AppConfig.Api.Core.GetBranches,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
            // SearchCondition: "",
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },

                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                },
                // {
                //     SystemName: 'StatusCode',
                //     Type: this._HelperService.AppConfig.DataType.Text,
                //     SearchCondition: '=',
                //     SearchValue: this._HelperService.AppConfig.Status.Active,
                // }
            ]
        }



        this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.GetBranches_Option = {
            placeholder: PlaceHolder,
            ajax: this.GetBranches_Transport,
            multiple: false,
        };
    }
    GetBranches_ListChange(event: any) {

        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.UserAccounts_Config,
            this._HelperService.AppConfig.OtherFilters.Manager.Branch
        );

        this.BranchEventProcessing(event);
    }

    BranchEventProcessing(event: any): void {
        this.Form_AddUser.patchValue(
            {
                BranchKey: event.data[0].ReferenceKey,
                BranchId: event.data[0].ReferenceId
            }
        );
    }


    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.UserAccounts_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Store(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.Manager
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.Manager
                );
                this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);
                this.UserAccounts_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this._HelperService.MakeFilterSnapPermanent();
        this.UserAccounts_GetData();
        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
        if (ButtonType == 'Sort') {
            $("#UserAccounts_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#UserAccounts_fdropdown").dropdown('toggle');
        }



    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.UserAccounts_GetData();
        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.UserAccounts_Config);

        //#region setOtherFilters

        this.SetOtherFilters();

        //#endregion

        this.UserAccounts_GetData();
    }

    //#endregion

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();

        this.GetMangers_List();
        this.GetBranches_List();
        this.UserAccounts_Filter_Owners_Load();
        this.Form_AddUser_Load();

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

}
export class OList {
    public Id: string;
    public ListType?: number = 0;
    public Task: string;
    public Location: string;
    public Title: string = "List";

    public TableFields: OListField[];
    public VisibleHeaders?: any[];

    public ActivePage?: number = 1;
    public PageRecordLimit?: number = 10;
    public TotalRecords?: number = 0;
    public ShowingStart?: number = 0;
    public ShowingEnd?: number = 0;


    public SearchBaseCondition?: string;
    public SearchBaseConditions?: any[];
    public SearchParameter?: string;
    public SearchCondition?: string;
    public Filters?: OSearchFilter[];

    public Data?: any[];

    public StatusType?: string = "default";
    public Status?: number = 0;
    public StatusOptions?: any[];

    public Sort: OListSort;

    public RefreshData?: boolean = false;
    public IsDownload?: boolean = false;

    public ReferenceId?: number = null;
    public SubReferenceId?: number = null;
    public ReferenceKey?: string = null;
    public Type?: string = null;
    public RefreshCount?: boolean = true;
    public TitleResourceId?: string = null;
    public StartTime?: any = null;
    public EndTime?: any = null;
    public StartDate?: any = null;
    public EndDate?: any = null;
}
export class OSearchFilter {
    public Title: string;
    public FildSystemName: string;
}
export class OListSort {
    public SortDefaultName: string;
    public SortDefaultColumn: string;
    public SortDefaultOrder?: string = "desc";
    public SortName?: string;
    public SortOrder?: string = "desc";
    public SortColumn?: string;
    public SortOptions?: any[];
}
export class OListField {
    public DefaultValue?: string = "--";
    public DisplayName: string;
    public DownloadDisplayName?: string;
    public SystemName: string;
    public Content?: string;
    public DataType: string = "text";
    public Class?: string = "";
    public ResourceId?: string = "";
    public Sort?: boolean = true;
    public Show?: boolean = true;
    public Search?: boolean = true;
    public NavigateLink?: string = "";
    public NavigateField?: string = "";
    public IsDateSearchField?: boolean = false;
}
