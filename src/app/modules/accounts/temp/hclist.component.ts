import { Component, OnInit } from "@angular/core";
import { FormBuilder, NgSelectOption } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';

import {
    OSelect,
    DataHelperService,
    HelperService
} from "../../../service/service";
@Component({
    selector: "hc-list",
    templateUrl: "./hclist.component.html"
})
export class HCListComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService
    ) { }
    ngOnInit() {
        Feather.replace();
        this.MerchantsList_Setup();
        this.MerchantsList_Filter_Owners_Load();
    }

    public MerchantsList_Config: OList;
    MerchantsList_Setup() {
        this.MerchantsList_Config = {
            Id: "MerchantsList",
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.TUCAccCore,
            Title: "All Merchants",
            StatusType: "default",
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 1, '!='),
            Type: this._HelperService.AppConfig.ListType.All,
            Sort:
            {
                SortDefaultName: 'Added on',
                SortDefaultColumn: 'CreateDate',
                SortDefaultOrder: 'desc'
            },
            TableFields: [
                {
                    DisplayName: 'Name',
                    SystemName: 'DisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                },
                {
                    DisplayName: 'R. %',
                    SystemName: 'RewardPercentage',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '_600',
                    Show: true,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Contact No',
                    SystemName: 'ContactNumber',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                },
                {
                    DisplayName: 'Email Address',
                    SystemName: 'EmailAddress',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: false,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: 'Stores',
                    SystemName: 'Stores',
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: '',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Owner',
                    SystemName: 'OwnerDisplayName',
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: '',
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: 'ThankUCash'
                },
                {
                    DisplayName: 'Last Tr',
                    SystemName: 'LastTransactionDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
                {
                    DisplayName: 'Last Login',
                    SystemName: 'LastLoginDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: false,
                    Search: false,
                    Sort: false,
                },
                {
                    DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
                    SystemName: 'CreateDate',
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: 'td-date',
                    Show: true,
                    Search: false,
                    Sort: true,
                },
            ]
        };
        this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '==');
        this.MerchantsList_Config = this._DataHelperService.List_Initialize(this.MerchantsList_Config);
        this.MerchantsList_GetData();
    }
    MerchantsList_ToggleOption(event: any, Type: any) {
        this.MerchantsList_Config = this._DataHelperService.List_Operations(this.MerchantsList_Config, event, Type);
        if (this.MerchantsList_Config.RefreshData == true) {
            this.MerchantsList_GetData();
        }
    }
    MerchantsList_GetData() {
        var TConfig = this._DataHelperService.List_GetData(this.MerchantsList_Config);
        this.MerchantsList_Config = TConfig;
    }
    MerchantsList_ListTypeChange(Type) {
        if (Type == 1) {
            this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '==');
        }
        else {
            this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 1, '!=');
            this.MerchantsList_Config.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '!=');
        }
        this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }
    MerchantsList_RowSelected(ReferenceData) {
        var Details =
        {
            ReferenceId: ReferenceData.ReferenceId,
            ReferenceKey: ReferenceData.ReferenceKey,
            DisplayName: ReferenceData.DisplayName,
        };
        this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.ActiveMerchant, Details);
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Merchant.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    }

    public MerchantsList_Filter_Owners_Option: Select2Options;
    public MerchantsList_Filter_Owners_Selected = 0;
    MerchantsList_Filter_Owners_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
            Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }
            ]
        };
        _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
            [
                this._HelperService.AppConfig.AccountType.Merchant,
                this._HelperService.AppConfig.AccountType.Acquirer,
                this._HelperService.AppConfig.AccountType.PGAccount,
                this._HelperService.AppConfig.AccountType.PosAccount
            ]
            , '=');
        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.MerchantsList_Filter_Owners_Option = {
            placeholder: 'Sort by Referrer',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }
    MerchantsList_Filter_Owners_Change(event: any) {
        if (event.value == this.MerchantsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
            this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
            this.MerchantsList_Filter_Owners_Selected = 0;
        }
        else if (event.value != this.MerchantsList_Filter_Owners_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '=');
            this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
            this.MerchantsList_Filter_Owners_Selected = event.value;
            this.MerchantsList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, this.MerchantsList_Filter_Owners_Selected, '='));
        }
        this.MerchantsList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }


}
export class OList {
    public Id: string;
    public ListType?: number = 0;
    public Task: string;
    public Location: string;
    public Title: string = "List";

    public TableFields: OListField[];
    public VisibleHeaders?: any[];

    public ActivePage?: number = 1;
    public PageRecordLimit?: number = 10;
    public TotalRecords?: number = 0;
    public ShowingStart?: number = 0;
    public ShowingEnd?: number = 0;


    public SearchBaseCondition?: string;
    public SearchBaseConditions?: any[];
    public SearchParameter?: string;
    public SearchCondition?: string;
    public Filters?: OSearchFilter[];

    public Data?: any[];

    public StatusType?: string = "default";
    public Status?: number = 0;
    public StatusOptions?: any[];

    public Sort: OListSort;

    public RefreshData?: boolean = false;
    public IsDownload?: boolean = false;

    public ReferenceId?: number = null;
    public SubReferenceId?: number = null;
    public ReferenceKey?: string = null;
    public Type?: string = null;
    public RefreshCount?: boolean = true;
    public TitleResourceId?: string = null;
    public StartTime?: any = null;
    public EndTime?: any = null;
    public StartDate?: any = null;
    public EndDate?: any = null;
}
export class OSearchFilter {
    public Title: string;
    public FildSystemName: string;
}
export class OListSort {
    public SortDefaultName: string;
    public SortDefaultColumn: string;
    public SortDefaultOrder?: string = "desc";
    public SortName?: string;
    public SortOrder?: string = "desc";
    public SortColumn?: string;
    public SortOptions?: any[];
}
export class OListField {
    public DefaultValue?: string = "--";
    public DisplayName: string;
    public DownloadDisplayName?: string;
    public SystemName: string;
    public Content?: string;
    public DataType: string = "text";
    public Class?: string = "";
    public ResourceId?: string = "";
    public Sort?: boolean = true;
    public Show?: boolean = true;
    public Search?: boolean = true;
    public NavigateLink?: string = "";
    public NavigateField?: string = "";
    public IsDateSearchField?: boolean = false;
}
