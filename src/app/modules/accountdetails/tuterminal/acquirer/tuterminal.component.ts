import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, FilterHelperService,  OResponse } from "../../../../service/service";
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
declare var $:any;

@Component({
  selector: "tu-terminal",
  templateUrl: "./tuterminal.component.html"
})
export class TUTerminalComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService

  ) {

  }

  //#region toogle map
  slideOpen: any = false;

  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
      this._HelperService._MapCorrection();
    }
  }

  HideDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");
  }

  ShowDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }
  //#endregion

  ngOnInit() {

    //#region UIInit 

    Feather.replace();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.FullContainer = false;

    //#endregion

    //#region GetStorage 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveTerminal);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    //#endregion

    //#region MapInit 

    this._HelperService._InitMap();
    this._HelperService._MapCorrection();

    //#endregion

    this._HelperService.ResetDateRange();
    this.GetTerminalsDetails();

  }

  //#region AccountOverview 

  public _AccountOverview: OAccountOverview =
    {
      Stores: 0,
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,

    }

  GetAccountOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getaccountoverviewlite',
      StartTime: this._HelperService.DateRangeStart, // new Date(2017, 0, 1, 0, 0, 0, 0),
      EndTime: this._HelperService.DateRangeEnd, // moment().add(2, 'days'),
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.ThankU, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);

        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        console.log(this._HelperService.HandleException(_Error));
      });
  }

  //#endregion

  //#region Terminals 

  public _TerminalDetails:any =
    {
      ProviderIconUrl: null,
      MerchantIconUrl: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  GetTerminalsDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminal,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._TerminalDetails = _Response.Result;
          console.log(this._TerminalDetails);

          //#region RepositionMarker 

          if (_Response.Result.StoreLatitude != undefined && _Response.Result.StoreLongitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.StoreLatitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.StoreLongitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          this._HelperService._ReLocate();

          //#endregion

          //#region InitResponse 

          this._TerminalDetails.EndDateS = this._HelperService.GetDateS(
            this._TerminalDetails.EndDate
          );
          this._TerminalDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.CreateDate
          );
          this._TerminalDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.ModifyDate
          );
          this._TerminalDetails.StatusI = this._HelperService.GetStatusIcon(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusB = this._HelperService.GetStatusBadge(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusC = this._HelperService.GetStatusColor(
            this._TerminalDetails.StatusCode
          );

          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
          console.log(this._HelperService.NotifyError(_Response.Message));
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
        console.log(this._HelperService.HandleException(_Error));
      }
    );
  }

  //#endregion

  //#region PosBlocking 

  BlockPos() {
    this._HelperService.OpenModal("BlockPos");
  }


  
  UnblockPos() {
    this._HelperService.OpenModal("UnBlockPos");
  }

  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "updateterminalstatus",
      ReferenceId: this._TerminalDetails.ReferenceId,
      ReferenceKey: this._TerminalDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess('Terminal Blocked Successfully');
          this._HelperService.CloseModal("BlockPos");
        //  this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminals]);
          this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Terminals]);
          this.GetTerminalsDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
        console.log(this._HelperService.HandleException(_Error));
      }
    );
  }


  public Unblock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "updateterminalstatus",
      ReferenceId: this._TerminalDetails.ReferenceId,
      ReferenceKey: this._TerminalDetails.ReferenceKey,
      StatusCode: "default.active",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess('Terminal UnBlocked Successfully');
          this._HelperService.CloseModal("UnBlockPos");
        //  this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Terminals]);
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Merchant.Terminals]);
        this.GetTerminalsDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
        console.log(this._HelperService.HandleException(_Error));
      }
    );
  }

  //#endregion
  StoresList_RowSelected() {

var ReferenceData = this._TerminalDetails
ReferenceData.ReferenceKey = this._TerminalDetails.StoreKey ;
ReferenceData.ReferenceId  = this._TerminalDetails.StoreId;

    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey:this._TerminalDetails.StoreKey ,
        ReferenceId: this._TerminalDetails.StoreId,
        DisplayName:  this._TerminalDetails.StoreName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );



    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
  }
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}