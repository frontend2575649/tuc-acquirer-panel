import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse, OUserDetails } from "../../../../service/service";
declare let $: any;
declare var moment: any;
import swal from 'sweetalert2';
import { HCoreXAddress, HCXAddressConfig, locationType } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';


@Component({
  selector: "tu-merchant",
  templateUrl: "./tumerchant.component.html"
})
export class TUMerchantComponent implements OnInit {

  public _isAddressLoaded = false;
  public _Address: HCoreXAddress = {};
  public _AddressStore: HCoreXAddress = {};
  subscription: any;

  AddressChange(Address) {
    this._Address = Address;
  }
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form,
    }

  defaultToDarkFilter = ["grayscale: 100%", "invert: 100%"];
  isLoaded: boolean = true;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ResetDateRange();
  }

  //#region mapCorrection 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
    if (this.ExpandedView) {
      this._HelperService._MapCorrection();
    }
  }

  //#endregion

  //#region DetailShowHIde 

  HideStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresDetails");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }

  //#endregion

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {
     //#region UIInitialize 

     Feather.replace();
     this._HelperService.AppConfig.ShowHeader = true;
     this._HelperService.ContainerHeight = window.innerHeight;
 
     this._HelperService._InitMap();
     this.InitBackDropClickEvent();
 
     this.HideStoreDetail();

    //#region GetStorageData 

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveMerchant);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    this.subscription = this._HelperService.getDetailsChangeEmitter()
    .subscribe(item => {
        this.GetMerchantDetails()
      });

    //#endregion

   

    //#endregion

    //#region InitEditForm 

    this.FormA_EditUser_Load();
    this.FormB_EditUser_Load();
    this.FormC_EditUser_Load();

    //#endregion

    this.GetMerchantDetails();

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  //#region MerchantDetails 

  public _UserAccount: OUserDetails = {
    ContactNumber: null,
    //OwnerName:null,
    SecondaryEmailAddress: null,
    ReferenceId: null,
    BankDisplayName: null,
    BankKey: null,
    SubOwnerAddress: null,
    SubOwnerLatitude: null,
    SubOwnerDisplayName: null,
    SubOwnerKey: null,
    SubOwnerLongitude: null,
    AccessPin: null,
    LastLoginDateS: null,
    AppKey: null,
    AppName: null,
    AppVersionKey: null,
    CreateDate: null,
    CreateDateS: null,
    CreatedByDisplayName: null,
    CreatedByIconUrl: null,
    CreatedByKey: null,
    Description: null,
    IconUrl: null,
    ModifyByDisplayName: null,
    ModifyByIconUrl: null,
    ModifyByKey: null,
    ModifyDate: null,
    ModifyDateS: null,
    PosterUrl: null,
    ReferenceKey: null,
    StatusCode: null,
    StatusI: null,
    StatusId: null,
    StatusName: null,
    AccountCode: null,
    AccountOperationTypeCode: null,
    AccountOperationTypeName: null,
    AccountTypeCode: null,
    AccountTypeName: null,
    Address: null,
    AppVersionName: null,
    ApplicationStatusCode: null,
    ApplicationStatusName: null,
    AverageValue: null,
    CityAreaKey: null,
    CityAreaName: null,
    CityKey: null,
    CityName: null,
    CountValue: null,
    CountryKey: null,
    CountryName: null,
    DateOfBirth: null,
    DisplayName: null,
    EmailAddress: null,
    EmailVerificationStatus: null,
    EmailVerificationStatusDate: null,
    FirstName: null,
    GenderCode: null,
    GenderName: null,
    LastLoginDate: null,
    LastName: null,
    Latitude: null,
    Longitude: null,
    MobileNumber: null,
    Name: null,
    NumberVerificationStatus: null,
    NumberVerificationStatusDate: null,
    OwnerDisplayName: null,
    OwnerKey: null,
    Password: null,
    Reference: null,
    ReferralCode: null,
    ReferralUrl: null,
    RegionAreaKey: null,
    RegionAreaName: null,
    RegionKey: null,
    RegionName: null,
    RegistrationSourceCode: null,
    RegistrationSourceName: null,
    RequestKey: null,
    RoleKey: null,
    RoleName: null,
    SecondaryPassword: null,
    SystemPassword: null,
    UserName: null,
    WebsiteUrl: null
  };

  public _MerchantDetails: any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      Latitude: null,
      Longitude: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,

      Address: null,
      AddressComponent:
      {
        Latitude: 0,
        Longitude: 0,
      },


    }

  


  //#endregion

  //#region EditUser

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      OwnerName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      // RewardPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]
      AccountPercentage: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])]
    });
  }

  FormA_EditUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this.Forms_EditUser_Close();
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(18)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      // LastName: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      SecondaryEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V2.System,
      _FormValue
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }


  //#region EditUserFormC 

  FormC_EditUser: FormGroup;
  FormC_EditUser_Address: string = null;
  FormC_EditUser_Latitude: number = 0;
  FormC_EditUser_Longitude: number = 0;


  @ViewChild('placesStore') placesStore: GooglePlaceDirective;

  FormC_EditUser_PlaceMarkerClick(event) {
    this.FormC_EditUser_Latitude = event.coords.lat;
    this.FormC_EditUser_Longitude = event.coords.lng;

  }
  public FormC_EditUser_AddressChange(address: Address) {
    this.FormC_EditUser_Latitude = address.geometry.location.lat();
    this.FormC_EditUser_Longitude = address.geometry.location.lng();
    this.FormC_EditUser_Address = address.formatted_address;
    this.FormC_EditUser.controls['Address'].setValue(address.formatted_address);
  }

  FormC_EditUser_Show() {
    this._HelperService.OpenModal("FormC_EditUser_Content");
  }

  FormC_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormC_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
      AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
      AccountOperationTypeCode: this._HelperService.AppConfig.AccountOperationType.Offline,
      RegistrationSourceCode: this._HelperService.AppConfig.RegistrationSource.System,
      OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
      Latitude: 0,
      Longitude: 0,
      Address: [null],
      Configuration: [],
      CountryName: this._HelperService.UserCountrycode,


    });
  }

  FormC_EditUser_Process(_FormValue: any) {

    _FormValue.Address = this._Address.Address;
    // _FormValue.AddressComponent = this._Address;
    _FormValue.Location = this._Address;

    if (this._Address.CityId < 1) {
      this._HelperService.NotifyError("Please enter address");
    }
    else {
      _FormValue.Address = this._Address.Address;

      this.toogleIsFormProcessing(true);
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V2.System,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this.toogleIsFormProcessing(false);
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Account updated successfully");
            if (_FormValue.OperationType == "edit") {
              this.Forms_EditUser_Close();
            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this.toogleIsFormProcessing(false);
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }


  //#endregion
  Forms_EditUser_Close() {
    this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region BackdropDismiss 

  @ViewChild("offCanvas") divView: ElementRef;

  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  //#endregion



  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }

  FormA_EditUser_Block() {
    // console.log("inside method");
    // console.log("inside method");
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      "Task": "updateaccountstatus",
      "AccountId": 97009,
      "AccountKey": "c360ef9459ad4dce9155d47ba055549e",
      "StatusCode": "default.blocked",
      "AuthPin": "1234",
      "Comment": "Test",
      "AccountTypeCode": "thankumerchant"
    }
    this._HelperService.IsFormProcessing = false;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountMer, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        // console.log(_Response.Status);
        // if (_Response.Status == this._HelperService.StatusSuccess) {
        //     this._HelperService.NotifySuccess("Configuration Updated");
        //     this._HelperService.CloseModal("M_UpdateRewardPercentage");
        //     this.GetMerchantConfigurations();
        // } else {
        //     this._HelperService.NotifySuccess(_Response.Message);
        // }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );

  }

  BlockAccount() {
    swal({
      title: "Really block the merchant?",
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to block this merchant?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">',
      //   '<input type="password" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('swal-input1')['value']) {
          swal.showValidationMessage(
            'Comment is required'
          )
        }
        //     if (!document.getElementById('swal-input2')['value']) {
        //       swal.showValidationMessage(
        //         'Pin is required'
        //       )
        //   }
        return [
          document.getElementById('swal-input1')['value'],
          //     document.getElementById('swal-input2')['value']
        ]
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._MerchantDetails.ReferenceId,
          AccountKey: this._MerchantDetails.ReferenceKey,
          StatusCode: "default.blocked",
          //AuthPin: "1111",//result.value[1]
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.statusupdate, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Merchant Blocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }

  unBlockAccount() {
    swal({
      title: "Really unblock the merchant?",
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to unblock this merchant?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">',
      //   '<input type="password" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('swal-input1')['value']) {
          swal.showValidationMessage(
            'Comment is required'
          )
        }
        //     if (!document.getElementById('swal-input2')['value']) {
        //       swal.showValidationMessage(
        //         'Pin is required'
        //       )
        //   }
        return [
          document.getElementById('swal-input1')['value'],
          //     document.getElementById('swal-input2')['value']
        ]
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._MerchantDetails.ReferenceId,
          AccountKey: this._MerchantDetails.ReferenceKey,
          StatusCode: "default.active",
          //AuthPin: "1111",//result.value[1]
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.statusupdate, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Merchant UnBlocked Successfully");
              this.GetMerchantDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }
  FormC_EditUser_Block() {

  }

  FormB_EditUser_Block() {

  }

  Form_AddStore_Close() {

    this._HelperService.CloseModal("myTabContent");
  }

  mapaddress: any = {};
  GetMerchantDetails() {
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchant,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      AccountId:this._HelperService.UserAccount.AccountId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.toogleIsFormProcessing(false);
          this.isLoaded = true;
          this._MerchantDetails = _Response.Result;
          this.mapaddress = this._MerchantDetails.AddressComponent;
          console.log(this.mapaddress,"mapaddress")
          this._Address = this._MerchantDetails.AddressComponent;

          setTimeout(() => {
            this._isAddressLoaded = true;
          }, 300);

          // for +234 number
          if (this._MerchantDetails.ContactPerson != undefined && this._MerchantDetails.ContactPerson.MobileNumber != undefined
            && this._MerchantDetails.ContactPerson.MobileNumber != null) {
            if (this._MerchantDetails.ContactPerson.MobileNumber.startsWith("234")) {
              this._MerchantDetails.ContactPerson.MobileNumber = this._MerchantDetails.ContactPerson.MobileNumber.substring(3, this._MerchantDetails.ContactPerson.length);
            }
          }


          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }
          this.FormC_EditUser_Latitude = _Response.Result.Latitude;
          this.FormC_EditUser_Longitude = _Response.Result.Longitude;


          //#endregion


          this._MerchantDetails.StartDateS = this._HelperService.GetDateS(
            this._MerchantDetails.StartDate
          );
          this._MerchantDetails.EndDateS = this._HelperService.GetDateS(
            this._MerchantDetails.EndDate
          );
          this._MerchantDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.CreateDate
          );
          this._MerchantDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._MerchantDetails.ModifyDate
          );
          this._MerchantDetails.StatusI = this._HelperService.GetStatusIcon(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusB = this._HelperService.GetStatusBadge(
            this._MerchantDetails.StatusCode
          );
          this._MerchantDetails.StatusC = this._HelperService.GetStatusColor(
            this._MerchantDetails.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


}
export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}