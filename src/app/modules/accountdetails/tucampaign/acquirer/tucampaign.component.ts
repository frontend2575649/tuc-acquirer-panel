import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import { Observable } from "rxjs";
import { DataHelperService, HelperService, OResponse } from "../../../../service/service";
declare var moment: any;



@Component({
  selector: "tu-campaign",
  templateUrl: "./tucampaign.component.html"
})
export class TuCampaignComponent implements OnInit {
  slideOpen: any = false;
  _DateDiff: any = {};
  isExtendActive: boolean;
  isCloseCamp: boolean;
  isPauseCamp: boolean;
  sdate:any;
  edate:any;
  enddate:any;
  startdate:any;

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) {
    this.isCloseCamp = false;
    this.isPauseCamp = false;
    this.isExtendActive = false;
  }
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if(this.slideOpen){
      this._HelperService._MapCorrection();
    }
  }
  myFunction() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-DisplayDiv");

  }

  DisplayDiv() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-DisplayDiv");
    element.classList.remove("Hm-HideDiv");
  }
  public CampaignKey = null;
  CampaignRewardType_FixedAmount = "fixedamount";
  CampaignRewardType_AmountPercentage = "amountpercentage";
  @ViewChild(DaterangePickerComponent)
  private _DaterangePickerComponent: DaterangePickerComponent;
  public _CampaignDetails:any =
    {
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,

      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  ngOnInit() {
    Feather.replace();
    this.Form_EditUser_Load();

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveCampaign);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    this.GetCampaignDetails();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = this._HelperService.AppConfig.AccountType.PosTerminal;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.FullContainer = false;
  }

  //#region ToogleRegion

  ToogleCloseCamp(): void {

    if (!this.isCloseCamp) {
      this.isCloseCamp = !this.isCloseCamp;
      this.isPauseCamp = false;
      this.isExtendActive = false;
    }

    // if (this.isCloseCamp) {
    //   this.Form_EditUser.controls['StatusCode'].setValue(this._HelperService.AppConfig.Status.Campaign.Archived);
    // } else {
    //   this.Form_EditUser.controls['StatusCode'].setValue(this._CampaignDetails.StatusCode);
    // }

  }

  TooglePauseCamp(): void {


    if (!this.isPauseCamp) {
      this.isPauseCamp = !this.isPauseCamp;
      this.isCloseCamp = false;
      this.isExtendActive = false;
    }

  }

  ToogleExtend(): void {

    if (!this.isExtendActive) {
      this.isExtendActive = !this.isExtendActive;
      this.isCloseCamp = false;
      this.isPauseCamp = false;
    }
  }

  //#endregion

  GetCampaignDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCampaign,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._CampaignDetails = _Response.Result;
          console.log(this._CampaignDetails.EndDate)
          this.sdate=this._CampaignDetails.StartDate;
          this.startdate = this._HelperService.GetDateS(this.sdate);
          console.log(this.startdate)
          this.edate=this._CampaignDetails.EndDate;
          //this.enddate = this._HelperService.GetDateS(this.edate);
          //this.enddate=this.edate.split("T");
          this.enddate = this._HelperService.GetDateS(this.enddate);
          console.log(this.enddate)

          if (this._DaterangePickerComponent != undefined) {
            this._DaterangePickerComponent.datePicker.setStartDate(moment(this._CampaignDetails.StartDate).utc().format("YYYY-MM-DD"));
            this._DaterangePickerComponent.datePicker.setEndDate(moment(this._CampaignDetails.EndDate).utc().format("YYYY-MM-DD"));

          }
          this._DateDiff = this._HelperService._DateRangeDifference(this._CampaignDetails.StartDate, this._CampaignDetails.EndDate);
          
          this._CampaignDetails.StartDateS = this._HelperService.GetDateS(this._CampaignDetails.StartDate);
        

          this._CampaignDetails.EndDateS = this._HelperService.GetDateS(
            this._CampaignDetails.EndDate
          );
          this._CampaignDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._CampaignDetails.CreateDate
          );
          this._CampaignDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._CampaignDetails.ModifyDate
          );
          this._CampaignDetails.StatusI = this._HelperService.GetStatusIcon(
            this._CampaignDetails.StatusCode
          );
          this._CampaignDetails.StatusB = this._HelperService.GetStatusBadge(
            this._CampaignDetails.StatusCode
          );
          this._CampaignDetails.StatusC = this._HelperService.GetStatusColor(
            this._CampaignDetails.StatusCode
          );

          // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(3)])],


          // : [null, Validators.compose([Validators.required])],

          // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(10)])],
          // : 0,

          // : 0,
          // : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(5)])],

          // StartDate: [null, Validators.compose([Validators.required])],
          // EndDate: [null, Validators.compose([Validators.required])],


        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GoTo_Overview(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Dashboard, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId]);
  }

  GoTo_Transaction(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Sales, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId]);
  }


  //Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this.isPauseCamp = false;
    this.isCloseCamp = false;
    this.isExtendActive = false;

    this.Form_EditUser_Load();
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_EditUser_Content");
  }
  // Form_EditUser_Load() {
  //   this._HelperService._FileSelect_Icon_Data.Width = 128;
  //   this._HelperService._FileSelect_Icon_Data.Height = 128;

  //   this._HelperService._FileSelect_Poster_Data.Width = 800;
  //   this._HelperService._FileSelect_Poster_Data.Height = 400;

  //   this.Form_EditUser = this._FormBuilder.group({
  //     OperationType: "new",
  //     Task: this._HelperService.AppConfig.Api.Core.SaveUserAccount,
  //     OwnerKey: this._HelperService.AppConfig.ActiveOwnerKey,
  //     FirstName: [
  //       null,
  //       Validators.compose([
  //         Validators.required,
  //         Validators.minLength(2),
  //         Validators.maxLength(128)
  //       ])
  //     ],


  //     StatusCode: this._HelperService.AppConfig.Status.Inactive,


  //     Owners: [],
  //     Configuration: []
  //   });
  // }
  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  // Form_EditUser_Process(_FormValue: any) {
  //   _FormValue.DisplayName = _FormValue.FirstName;
  //   _FormValue.Name = _FormValue.FirstName + " " + _FormValue.LastName;
  //   this._HelperService.IsFormProcessing = true;
  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(
  //     this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     _FormValue
  //   );
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._HelperService.NotifySuccess("Account created successfully");
  //         this.Form_EditUser_Clear();
  //         if (_FormValue.OperationType == "close") {
  //           this.Form_EditUser_Close();
  //         }
  //       } else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //     }
  //   );
  // }

  Form_EditUser: FormGroup;
  Form_EditUser_Load() {
    this.Form_EditUser = this._FormBuilder.group({
      Task: this._HelperService.AppConfig.Api.ThankUCash.UpdateCampaign,
      ReferenceKey: this._CampaignDetails.ReferenceKey,
      UserAccountKey: this._HelperService.AppConfig.ActiveOwnerKey,

      StartDate: [this._CampaignDetails.StartDate, Validators.compose([Validators.required])],
      EndDate: [this._CampaignDetails.EndDate, Validators.compose([Validators.required])],
      StatusCode: [this._CampaignDetails.StatusCode, Validators.compose([Validators.required])]
    });



    if (this._CampaignDetails.StatusCode == this._HelperService.AppConfig.Status.Campaign.Archived) {
      this.isCloseCamp = false;
      this.ToogleCloseCamp();
    }

    if (this._CampaignDetails.StatusCode == this._HelperService.AppConfig.Status.Campaign.Paused) {
      this.isPauseCamp = false;
      this.TooglePauseCamp();
    }


  }
  Form_EditUser_Process(_FormValue: any) {

    if (this.isCloseCamp) {
      var pData =
      {
        Task: "deletecampaign",
        ReferenceKey: this._CampaignDetails.ReferenceKey,
      };
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, pData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Campaign deleted successfully");
            this._HelperService.CloseModal('Form_EditUser_Content');
            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Campaigns.Campaigns]);
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    } else if (this.isExtendActive || this.isPauseCamp) {

      if (this.isExtendActive) {
        _FormValue.StatusCode = this._CampaignDetails.StatusCode;
      } else {
        _FormValue.StartDate = this._CampaignDetails.StartDate;
        _FormValue.EndDate = this._CampaignDetails.EndDate;
        _FormValue.StatusCode = this._HelperService.AppConfig.Status.Campaign.Paused;
      }

      _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.TUCCampaign, _FormValue);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.GetCampaignDetails();
            this.Form_EditUser_Close();
            this.GetCampaignDetails();
            this._HelperService.NotifySuccess("Campaign updated successfully");
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          // console.log(_Error);
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

    } else {

    }

  }

  Date_Toogle(event: any): void {
    // this.Form_EditUser.controls['StartDate'].setValue(event.start);
    this.Form_EditUser.controls['EndDate'].setValue(event.end);
  }

}