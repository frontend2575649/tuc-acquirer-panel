import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { AgmCoreModule } from '@agm/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';



import { TUManagerAlternateComponent } from "./tumanageralternate.component";
const routes: Routes = [
    {
        path: "",
        component: TUManagerAlternateComponent,
        children: [
            { path: "/:referencekey/:referenceid/:type", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/manager/acquirer/dashboard.module#TUDashboardModule" },

            { path: "dashboard/:referencekey/:referenceid/:type", data: { permission: "getmerchant", menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../dashboards/manager/acquirer/dashboard.module#TUDashboardModule" },

            { path: "terminals/:referencekey/:referenceid/:type", data: { permission: "getmerchant", PageName: 'System.Menu.Manager',menuoperations: "ManageMerchant", accounttypecode: "merchant" }, loadChildren: "../../../../modules/accounts/tuterminals/manager/tuterminals.module#TUTerminalsModule" },

            { path: 'stores/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../modules/accounts/tustores/manager/tustores.module#TUStoresModule' },

            { path: 'targets/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../modules/accountdetails/tutargethistory/tutargethistory.module#TUTargetHistoryModule' },

            { path: 'teams/:referencekey/:referenceid/:type', data: { 'permission': 'salehistory', PageName: 'System.Menu.Manager' }, loadChildren: '../../../../modules/accounts/tuteams/manager/tuteams.module#TUTeamsModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUManagerAlternateRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUManagerAlternateRoutingModule,
        LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [TUManagerAlternateComponent]
})
export class TUManagerAlternateModule { }
