import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from 'feather-icons';
import { Observable } from "rxjs";
import { DataHelperService, FilterHelperService, HelperService, OResponse, OSelect } from "../../../../service/service";
declare var moment: any;

declare var $: any;
declare var d3: any;
import swal from 'sweetalert2';

@Component({
  selector: "tu-manager",
  templateUrl: "./tumanager.component.html"
})
export class TUManagerComponent implements OnInit {
  public _ShowMap = false;

  Type: string
  public _Roles = [
    {
      'id': 0,
      'text': 'Select Roles',
      'apival': 'Select Roles'
    },
    {
      'id': 6,
      'text': 'Manager',
      'apival': 'manager'
    },
    // {
    //   'id': 7,
    //   'text': 'Administratior/Sub Account',
    //   'apival': 'admin'
    // },
    {
      'id': 8,
      'text': 'RM',
      'apival': 'rm'
    }
  ];
  @ViewChild("offCanvas") divView: ElementRef;
  _DateDiff: any = {};
  slideOpen: any = false;
  public _ManagerDetails: any =
    {
      RoleId: null,
      BranchKey: null,
      BranchId: null,
      BranchName: null,
      RoleKey: null,
      RoleName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService



  ) { }
  changeSlide(): void {
    this.slideOpen = !this.slideOpen;
    if (this.slideOpen) {
      this._HelperService._MapCorrection();
    }
  }
  // myFunction() {
  //   var element = document.getElementById("StoresHide");
  //   element.classList.add("Hm-HideDiv");
  //   element.classList.remove("Hm-DisplayDiv");

  // }

  // DisplayDiv() {
  //   var element = document.getElementById("StoresHide");
  //   element.classList.add("Hm-DisplayDiv");
  //   element.classList.remove("Hm-HideDiv");
  // }

  ngOnInit() {
    this._ShowMap = true;
    Feather.replace();
    this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');
    this.Form_EditUser_Load();
    this.GetRMs_List();
    this.Form_AssignTarget_Load();
    //this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');

    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };

    this.Form_EditUser_Load();
    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveManager);

    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
      this._ChangeDetectorRef.detectChanges();
      this.GetManagerDetails();

    }

    //   this._ActivatedRoute.params.subscribe((params: Params) => {
    //     this._HelperService.AppConfig.ActiveReferenceKey = params["ReferenceKey"];
    //     this._HelperService.AppConfig.ActiveReferenceId = params['ReferenceId'];
    //     if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
    //         this.GetRMs_List();
    //     } else {
    //         this.GetRMs_List();
    //     }
    // });



    // this.GetBranches_List();
    // this.GetMangers_List();
    this._HelperService.AppConfig.ShowHeader = true;
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.FullContainer = false;
  }




  initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
    var i = '#' + pickerId;

    if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
      $(i).datepicker(
        {
          setDate: new Date(),
          viewMode: "months",
          minViewMode: "months",
          format: "dd/mm/yyyy",
          startDate: '-0m'

        }
      ).on('changeDate', function (ev) {
        $('#sDate1').text($('#datepicker').data('date'));
        $('#datepicker').datepicker('hide');
      });;
    } else {
      $(i).datepicker();
    }

    $(i).on('changeDate', () => {
      this.ScheduleDateRangeChange({
        start: moment($(i).datepicker("getDate")),
        end: moment($(i).datepicker("getDate"))
      });
      $(i).datepicker('hide');
    });

  }



  Form_EditUser: FormGroup;
  Form_EditUser_Show() {
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.OpenModal("Form_EditUser_Content");
  }
  Form_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;
    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_EditUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.Core.UpdateManager,
      ReferenceKey: this._ManagerDetails.ReferenceKey,
      ReferenceId: this._ManagerDetails.ReferenceId,
      OwnerKey: [this._ManagerDetails['OwnerKey'], Validators.required],
      OwnerId: [this._ManagerDetails['OwnerId'], Validators.required],
      RoleKey: [null, Validators.required],
      RoleId: [null, Validators.required],
      BranchKey: [this._ManagerDetails['BranchKey'], Validators.required],
      BranchId: [this._ManagerDetails['BranchId'], Validators.required],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      StatusCode: this._HelperService.AppConfig.Status.Active,


    });

  }

  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }


  Form_EditUser_Clear() {
    this.Form_EditUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_EditUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, _FormValue);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          this.GetManagerDetails();
          this._HelperService.CloseModal('off-canvas')
          this.Form_EditUser_Clear();
          if (_FormValue.OperationType == "close") {
            this.Form_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }


  public GetRoles_Option: Select2Options;
  public GetRoles_Transport: any;

  GetRoles_ListChange(event: any) {

    this.Form_EditUser.controls['OwnerId'].setValue(null);
    this.Form_EditUser.controls['OwnerKey'].setValue(null);

    this.Form_EditUser.patchValue(
      {
        RoleId: event.value,
        RoleKey: event.data[0].apival
      }
    );
    this._ShowRole = false;
    this._ChangeDetectorRef.detectChanges();

    if (event.value == 8) { //RM
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 6; //Manager

      this.GetMangers_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();
    } else if (event.value == 7) { //SubAccount

      this._ShowReportingM = false;
      this.Form_EditUser.controls['OwnerId'].setValue(this._HelperService.UserAccount.AccountId);
      this.Form_EditUser.controls['OwnerKey'].setValue(this._HelperService.UserAccount.AccountKey);

      this._MangerTypeId = undefined;
    }
    else if (event.value == 6) { //Manager
      this._ShowReportingM = false;
      this._ChangeDetectorRef.detectChanges();
      this._MangerTypeId = 7; //Subaccount
      this.GetMangers_List();
      this._ShowReportingM = true;
      this._ChangeDetectorRef.detectChanges();

    } else {
      this._MangerTypeId = undefined;
      this._ShowReportingM = true;
      this.GetMangers_List();
    }

    this._ShowRole = true;
    this._ChangeDetectorRef.detectChanges();

    this.GetMangers_List();
  }

  public _MangerTypeId: number;
  public _ShowRole: boolean = true;
  public _ShowReportingM: boolean = true;

  public GetMangers_Option: Select2Options;
  public GetRole_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect
    if (this._MangerTypeId == 7) {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }
    } else {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetManagers,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }
    }


    if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
    }

    this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if (this._ManagerDetails.OwnerId > 0) {
      this.GetMangers_Option = {
        placeholder: this._ManagerDetails.OwnerDisplayName,
        ajax: this.GetMangers_Transport,
        multiple: false,
      };

    } else {
      this.GetMangers_Option = {
        placeholder: PlaceHolder,
        ajax: this.GetMangers_Transport,
        multiple: false,
      };
    }
  }
  GetMangers_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        OwnerId: event.data[0].ReferenceId,
        OwnerKey: event.data[0].ReferenceKey,
      }
    );

  }

  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
    var PlaceHolder = "Select Branch";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetBranches,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }



    this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if (this._ManagerDetails.BranchId > 0) {
      this.GetBranches_Option = {
        placeholder: this._ManagerDetails.BranchName,
        ajax: this.GetBranches_Transport,
        multiple: false,
      };
    } else {
      this.GetBranches_Option = {
        placeholder: PlaceHolder,
        ajax: this.GetBranches_Transport,
        multiple: false,
      };
    }
  }
  GetBranches_ListChange(event: any) {
    // alert(event);\
    this.Form_EditUser.patchValue(
      {
        BranchKey: event.data[0].ReferenceKey,
        BranchId: event.data[0].ReferenceId

      }
    );

  }

  GoTo_Overview(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Overview, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Terminals(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Terminals, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Stores(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Stores, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  GoTo_Teams(): void {
    this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.ManagerDetail.Teams, this._HelperService.AppConfig.ActiveReferenceKey, this._HelperService.AppConfig.ActiveReferenceId, this._ManagerDetails['RoleId']]);
  }

  roleone:any;
  managerone:any;
  banchone:any;
  GetManagerDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.GetManger,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Branch, pData);
    _OResponse.subscribe(

      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._HelperService.IsFormProcessing = false;
          this._ManagerDetails = _Response.Result;
          console.log(this._ManagerDetails,"details");
          // this.banchone=this._ManagerDetails.BranchName;
          // this.roleone=this._ManagerDetails.RoleName;
          // this.managerone=this._ManagerDetails.OwnerDisplayName;
          

          if (this._ManagerDetails != undefined && this._ManagerDetails.MobileNumber != undefined && this._ManagerDetails.MobileNumber != null) {
            if (this._ManagerDetails.MobileNumber.startsWith("234")) {
              this._ManagerDetails.MobileNumber = this._ManagerDetails.MobileNumber.substring(3, this._ManagerDetails.length);
            }
          }
          // this.GetBranches_Option.placeholder = this._ManagerDetails.BranchName;
          // this.GetMangers_Option.placeholder = this._ManagerDetails.OwnerDisplayName;

          this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveManagerD,
            {
              ActiveAccountId: this._ManagerDetails.AccountId,

            }
          );

          this.GetBranches_List();
          this.GetMangers_List();
          //  this.GetRole_Option.placeholder = this._ManagerDetails.RoleId;
          this.Form_EditUser_Load();
          this.Form_AssignTarget_Load();
          // this.GetMangers_Option.placeholder = this._ManagerDetails.Name;
          // this.GetRoles_Option.placeholder = this._ManagerDetails.Name;
          this._HelperService.ManagerBranchKey = this._ManagerDetails.BranchKey;
          this._HelperService.ManagerBranchId = this._ManagerDetails.BranchId;

          this._DateDiff = this._HelperService._DateRangeDifference(this._ManagerDetails.StartDate, this._ManagerDetails.EndDate);
          this._ManagerDetails.StartDateS = this._HelperService.GetDateS(
            this._ManagerDetails.StartDate
          );
          this._ManagerDetails.EndDateS = this._HelperService.GetDateS(
            this._ManagerDetails.EndDate
          );
          this._ManagerDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._ManagerDetails.CreateDate
          );
          this._ManagerDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._ManagerDetails.ModifyDate
          );
          this._ManagerDetails.StatusI = this._HelperService.GetStatusIcon(
            this._ManagerDetails.StatusCode
          );
          this._ManagerDetails.StatusB = this._HelperService.GetStatusBadge(
            this._ManagerDetails.StatusCode
          );
          this._ManagerDetails.StatusC = this._HelperService.GetStatusColor(
            this._ManagerDetails.StatusCode
          );
          this.RoleName();
          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  clicked() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  RoleName() {
    if (this._ManagerDetails.RoleId == 6) {
      this._ManagerDetails['RoleName'] = "Manager";
      this._Roles[0].text = "Manager"

    }
    else if (this._ManagerDetails.RoleId == 7) {
      this._ManagerDetails['RoleName'] = "Sub-Admin";
      this._Roles[0].text = "Sub-Admin"


    }
    else {
      this._ManagerDetails['RoleName'] = "RM";
      this._Roles[0].text = "RM"
      // this.GetRoles_Option.placeholder = "RM";


    }


  }

  Form_EditUser_Block() {

  }

  BlockAccount() {
    swal({
      title: "Really block the Manager?",
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to block this merchant?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">',
      //   '<input type="password" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('swal-input1')['value']) {
          swal.showValidationMessage(
            'Comment is required'
          )
        }
        //     if (!document.getElementById('swal-input2')['value']) {
        //       swal.showValidationMessage(
        //         'Pin is required'
        //       )
        //   }
        return [
          document.getElementById('swal-input1')['value'],
          //     document.getElementById('swal-input2')['value']
        ]
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",

          AccountId: this._ManagerDetails.ReferenceId,
          AccountKey: this._ManagerDetails.ReferenceKey,

          //  ReferenceKey: this._HelperService.UserAccount.AccountKey,
          //  ReferenceId: this._HelperService.UserAccount.AccountId,
          StatusCode: "default.blocked",
          //AuthPin: "1111",//result.value[1]
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchants

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.statusupdate, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Manager Blocked Successfully");
              this.GetManagerDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }

  unBlockAccount() {
    swal({
      title: "Really unblock the merchant?",
      text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
      position: this._HelperService.AppConfig.Alert_Position,
      animation: this._HelperService.AppConfig.Alert_AllowAnimation,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      allowOutsideClick: this._HelperService.AppConfig.Alert_AllowOutsideClick,
      allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
      showCancelButton: true,
      html:
        ' <label> Do you really want to unblock this merchant?  </label>' +
        '<input type="text"  placeholder="Enter Comment"  id="swal-input1" class="swal2-input">',
      //   '<input type="password" placeholder="Enter Pin"  id="swal-input2" class="swal2-input">',


      focusConfirm: false,
      preConfirm: () => {
        if (!document.getElementById('swal-input1')['value']) {
          swal.showValidationMessage(
            'Comment is required'
          )
        }
        //     if (!document.getElementById('swal-input2')['value']) {
        //       swal.showValidationMessage(
        //         'Pin is required'
        //       )
        //   }
        return [
          document.getElementById('swal-input1')['value'],
          //     document.getElementById('swal-input2')['value']
        ]
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
    }).then((result) => {
      if (result.value) {

        this._HelperService.IsFormProcessing = true;
        var PostData = {
          Task: "updateaccountstatus",
          AccountId: this._ManagerDetails.ReferenceId,
          AccountKey: this._ManagerDetails.ReferenceKey,
          // AccountId: this._ManagerDetails.ReferenceId,
          //  AccountKey: this._ManagerDetails.ReferenceKey,
          StatusCode: "default.active",
          //AuthPin: "1111",//result.value[1]
          Comment: result.value[0],
          AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchants

        };

        let _OResponse: Observable<OResponse>;

        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.statusupdate, PostData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Manager UnBlocked Successfully");
              this.GetManagerDetails();
            } else {
              this._HelperService.NotifySuccess(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
    });


  }

  hideMonthlyPicker: boolean = true;
  ShowHideCalendar() {
    this.hideMonthlyPicker = !this.hideMonthlyPicker;
  }

  // initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
  //   var i = '#' + pickerId;

  //   if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
  //     $(i).datepicker(
  //       {
  //         setDate: new Date(),
  //         viewMode: "months",
  //         minViewMode: "months"
  //       }
  //     );
  //   } else {
  //     $(i).datepicker();
  //   }

  //   $(i).on('changeDate', () => {
  //     this.ScheduleDateRangeChange({
  //       start: moment($(i).datepicker("getDate")),
  //       end: moment($(i).datepicker("getDate"))
  //     });
  //     $(i).datepicker('hide');
  //   });

  // }

  //   ScheduleDateRangeChange(value): void {

  //     this.Form_AssignTarget.patchValue(
  //         {
  //             TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
  //         }
  //     );

  //     this.Form_AssignTarget.patchValue(
  //         {
  //             StartDate: value.start,
  //             EndDate: value.end,
  //         }
  //     );
  // }
  //#region Target Assignments 

  ShowAssignTargetModal() {
    this._HelperService.OpenModal('AssignTarget');
  }
  Form_AssignTarget: FormGroup;
  Form_AssignTarget_Show() {
    this._HelperService.OpenModal("AssignTarget");
  }
  Form_AssignTarget_Close() {
    this._HelperService.CloseModal("AssignTarget");
  }
  Form_AssignTarget_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AssignTarget = this._FormBuilder.group({

      ManagerKey: [this._ManagerDetails['OwnerKey'], Validators.required],
      ManagerId: [this._ManagerDetails['OwnerId'], Validators.required],

      // RoleKey: ['rm', Validators.required],
      // RoleId: [8, Validators.required],

      BranchKey: [this._ManagerDetails['BranchKey'], Validators.required],
      BranchId: [this._ManagerDetails['BranchId'], Validators.required],

      RmKey: [this._ManagerDetails['AccountKey'], Validators.required],
      RmId: [this._ManagerDetails['AccountId'], Validators.required],

      StartDate: [null, Validators.compose([Validators.required])],
      EndDate: [null, Validators.compose([Validators.required])],
      datefield: [null],


      TStartDate: [null],

      TotalTarget: [null, Validators.compose([
        Validators.required,
        Validators.min(1)
      ])
      ],

      // datefield: [null]


    });
  }

  datefield: any;
  Form_AssignTarget_Clear() {

    this.GetRM_ResetSelector();
    this.Form_AssignTarget.reset();
    this.Form_AssignTarget_Load();
    setTimeout(() => {
      this.initializeDatePicker('monthlydate', this._HelperService.AppConfig.DatePickerTypes.month, 'MonthSalesTrend_dropdown');
    }, 200);
  }
  Form_AssignTarget_Process(_FormValue: any) {
    // _FormValue.RmId = this.RMs[0].RmId;
    // _FormValue.RmKey = this.RMs[0].RmKey;
    _FormValue.TStartDate = undefined;

    var pData = {
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveRmTarget,
      Targets: []
    };

    pData.Targets.push(_FormValue);

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Branch,
      pData
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Target Assigned successfully to selected users");
          this.datefield = null;
          this.Form_AssignTarget_Clear();
          this.Form_AssignTarget_Close();
          // this.Form_AssignTarget_Load();
          this._HelperService.ReloadEventEmit.emit(0);

          if (_FormValue.OperationType == "close") {
            this.Form_AssignTarget_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  ScheduleDateRangeChange(value): void {

    this.Form_AssignTarget.patchValue(
      {
        TStartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
      }
    );

    this.Form_AssignTarget.patchValue(
      {
        StartDate: moment(value.start).startOf('month').startOf('day'),
        EndDate: moment(value.end).endOf('month').endOf('day'),
      }
    );
  }


  //#region RM selection 

  _ShowRMSelector: boolean = true;
  RMs = [];
  public GetRMs_Option: Select2Options;
  public GetRMs_Transport: any;
  GetRMs_List() {
    var PlaceHolder = "Select Manager";
    var _Select: OSelect

    if (this._MangerTypeId == 7) {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetSubAccounts,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.SubAccounts,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }
    } else {
      _Select =
      {
        Task: this._HelperService.AppConfig.Api.Core.GetManagers,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
        ReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.UserAccount.AccountId,
        SortCondition: [],
        Fields: [
          {
            SystemName: "ReferenceId",
            Type: this._HelperService.AppConfig.DataType.Number,
            Id: true,
            Text: false,
          },
          {
            SystemName: "Name",
            Type: this._HelperService.AppConfig.DataType.Text,
            Id: false,
            Text: true
          },
        ]
      }

    }


    if (this._MangerTypeId != undefined && this._MangerTypeId != 7) {
      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'RoleId', this._HelperService.AppConfig.DataType.Number, this._MangerTypeId, '=')
    }

    this.GetRMs_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetRMs_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetRMs_Transport,
      multiple: false,
    };
  }
  GetRM_ListChange(event: any) {
    if (event.data[0]) {
      this.RMs[0] = {
        RmId: event.data[0].ReferenceId,
        RmKey: event.data[0].ReferenceKey
      };
    } else {
      this.RMs[0] = undefined;
    }
  }
  GetRM_ResetSelector(): void {
    this._ShowRMSelector = false;
    this._ChangeDetectorRef.detectChanges();
    setTimeout(() => {
      this._ShowRMSelector = true;
      this._ChangeDetectorRef.detectChanges();
    }, 100);
  }

  //#endregion

  //#endregion
  public ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
}
